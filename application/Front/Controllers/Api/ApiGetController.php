<?php namespace Front\Api;

use Models\Game;

class ApiGetController extends \BaseController {

    public function getUserState()
    {
        $token = \Input::get('user_token');

        $state = new UserState($token);

        return \Response::json($state);
    }

    public function getGameState()
    {
        $aid = \Input::get('api_id');

        $state = new GameState($aid);

        return \Response::json($state);

    }



}