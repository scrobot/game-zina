<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 29.07.2015
 * Time: 14:52
 */

namespace Front\Api;

use Models\Game;

class GameState {

    public $game_state;

    public function __construct($aid)
    {
        $this->game_state = $this->game($aid);
    }

    private function game($aid)
    {
        $type = $this->getGameType($aid);
        $start = $this->getGameStart($aid);

        return [
            'type' => $type,
            'start'=> $start
        ];
    }

    private function getGameType($aid)
    {
        $game = Game::where('api_id', $aid)->first();

        return $game->type;
    }

    private function getGameStart($aid)
    {
        $game = Game::where('api_id', $aid)->first();

        $start = $game->start_date > date("Y-m-d H:i:s") ? 0 : 1;

        return $start;
    }

}