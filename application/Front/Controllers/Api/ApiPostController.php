<?php namespace Front\Api;

use Models\Game;

use GuzzleHttp;

class ApiPostController extends \BaseController {

    /**
     * Запрос разрешения запуска приложение, проверка на совпадение API-ключей и токенов.
     * @return int
     */

    public function postPermission()
    {
        $hash = \Input::get('hash');
        $aid = \Input::get('api_id');

        $game = Game::where('api_id', $aid)->first();

        if($hash == $game->api_hash) {
            return 1;
        }

        return 0;
    }

    public function postSpeed()
    {

        $token = \Input::get('user_token');

        $client = new GuzzleHttp\Client();
        $u = $client->get("http://gamezina.ru//api/gamezina/v1/request/get/user-state", ['form_params' => ['token' => $token]]);
        $user_state = $u->getBody();
        return \Response::json(json_decode($user_state));

    }

    public function postSuper()
    {
        $token = \Input::get('user_token');
        $api_id = \Input::get('api_id');
        $result = [];

        $client_user = new GuzzleHttp\Client();
        $u = $client_user->get("http://gamezina.ru/api/gamezina/v1/request/get/user-state?user_token={$token}");
        $user_state = $u->getBody();
        $result[] = json_decode($user_state);

        $client_game = new GuzzleHttp\Client();
        $g = $client_game->get("http://gamezina.ru/api/gamezina/v1/request/get/game-state?api_id={$api_id}");
        $game_state = $g->getBody();
        $result[] = json_decode($game_state);

        return \Response::json($result);
    }

    public function postScore()
    {

    }



}