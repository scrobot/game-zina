<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 29.07.2015
 * Time: 14:52
 */

namespace Front\Api;

use Models\User;

class UserState {

    public $user_state;

    public function __construct($token)
    {
        $this->user_state = $this->user($token);
    }

    private function user($token)
    {
        $user = User::where('token_secret', $token)->first();
        if(is_null($user)) {
            return 0;
        } elseif($user->subscribed == 0) {
            return 1;
        } elseif($user->subscribed == 1) {
            return 2;
        }
    }

}