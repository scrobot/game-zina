<?php

namespace Front;

use Mix\BlockHelper;
use Models\Faq;

class FaqController extends BaseController {

    public function __construct(BlockHelper $block) {
        $this->block = $block;

        parent::__construct();
    }

    public function getHandbook($alias)
    {
        $faq = Faq::where('alias', $alias)->first();
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = $faq->keywords;
        $settings->description = $faq->description;
        $settings->page_title = $faq->title;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();
        $handbookMenu = $this->block->handbookMenu();

        return \View::make('Front::pages_layouts.handbook', compact('settings','menu','ourSocials','handbookMenu','faq'));
    }

}