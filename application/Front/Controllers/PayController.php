<?php

namespace Front;

use Mix\PayHelper;
use Models\Pay;
use Models\User;
use Mix\BlockHelper;
use Mix\UserScore;

class PayController extends BaseController {

    /**
     * Dependency Injection класса BlockHelper
     * @param BlockHelper $block
     */
    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();
    }

    public function postIndex()
    {

        $pay = new Pay(\Input::all());
        $pay->sum = 200;
        $pay->save();
        $merchant_id = PayHelper::$merchant_id; // Идентификатор магазина в Pay2Pay
        $secret_key = PayHelper::$secret_key; // Секретный ключ
        $order_id = $pay->id; // Номер заказа
        $amount = $pay->sum; // Сумма заказа
        $currency = PayHelper::$currency; // Валюта заказа
        $desc = PayHelper::$desc; // Описание заказа
        $test_mode = PayHelper::$test_mode; // Тестовый режим
        // Формируем xml
        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                        <request>
                            <version>1.2</version>
                            <merchant_id>$merchant_id</merchant_id>
                            <language>ru</language>
                            <order_id>$order_id</order_id>
                            <amount>$amount</amount>
                            <currency>$currency</currency>
                            <description>$desc</description>
                            <test_mode>$test_mode</test_mode>
                        </request>";
        // Вычисляем подпись
        $sign = md5($secret_key.$xml.$secret_key);
        // Кодируем данные в BASE64
        $xml_encode = base64_encode($xml);
        $sign_encode = base64_encode($sign);
        $pay->update([
            'xml_encode' => $xml_encode,
            'sign_encode' => $sign_encode,
        ]);

        return \Redirect::to("https://merchant.pay2pay.com/?page=init&xml={$pay->xml_encode}&sign={$pay->sign_encode}");
    }

    public function postSuccess()
    {
        if (isset($_REQUEST['xml']) and isset($_REQUEST['sign'])) {
        // Инициализация переменной для хранения сообщения об ошибке
            // Декодируем входные параметры
            $xml = base64_decode(str_replace(' ', '+', $_REQUEST['xml']));
            $sign = base64_decode(str_replace(' ', '+', $_REQUEST['sign']));
            // преобразуем входной xml в удобный для использования формат
            $vars = simplexml_load_string($xml);

            $order = Pay::find($vars->order_id);
            $order->status_id = 2;
            $order->save();

            $settings   = $this->block->settings();
            $settings->isHome = false;
            $settings->keywords = "Успешная оплата";
            $settings->description = "Оплата проведена успешно";
            $settings->page_title = "Успешно!";
            $menu       = $this->block->menu();
            $ourSocials = $this->block->ourSocials();

            $user = User::find(\Auth::user()->id);
            $games = $this->block->liveGames();

            foreach($games as $game) {
                if(!$game->users->contains($user->id)) {
                    $game->users()->attach($user->id);
                    if($game->type == 'super') {
                        $game->superGameActivation($user->id);
                    }
                    $json = file_get_contents(__FRONTASSETS__."/".$game->alias.".json");
                    $temp = json_decode($json);
                    $newScore = new UserScore($user->nickname, 0);
                    $temp->results[] = $newScore;

                    UserScore::writeResult($game->alias, $temp);

                    unset($temp);
                }
            }

            $user->subscribed = 1;
            $user->save();

            return \View::make('Front::pages_layouts.pay_success', compact('settings','banners','menu','ourSocials'));


        } else {
            return 'error';
        }
    }

    public function postFail()
    {
        if (isset($_REQUEST['xml']) and isset($_REQUEST['sign'])) {
            // Инициализация переменной для хранения сообщения об ошибке
            // Декодируем входные параметры
            $xml = base64_decode(str_replace(' ', '+', $_REQUEST['xml']));
            $sign = base64_decode(str_replace(' ', '+', $_REQUEST['sign']));
            // преобразуем входной xml в удобный для использования формат
            $vars = simplexml_load_string($xml);

            $order = Pay::find($vars->order_id);
            $order->status_id = 3;
            $order->save();

            $settings   = $this->block->settings();
            $settings->isHome = false;
            $settings->keywords = "Успешная оплата";
            $settings->description = "Оплата проведена успешно";
            $settings->page_title = "Успешно!";
            $menu       = $this->block->menu();
            $ourSocials = $this->block->ourSocials();

            return \View::make('Front::pages_layouts.pay_fail', compact('settings','banners','menu','ourSocials'));


        } else {
            return 'error';
        }
    }

    public function postResult()
    {
        if (isset($_REQUEST['xml']) and isset($_REQUEST['sign']))
        // Если не получили параметры xml и sign, то нечего проверять
        {
            // Инициализация переменной для хранения сообщения об ошибке
            $error = '';
            // Декодируем входные параметры
            $xml = base64_decode(str_replace(' ', '+', $_REQUEST['xml']));
            $sign = base64_decode(str_replace(' ', '+', $_REQUEST['sign']));
            // преобразуем входной xml в удобный для использования формат
            $vars = simplexml_load_string($xml);
            if ($vars->order_id)
                // Если поле order_id не заполнено, продолжать нет смысла.
                // Т.к. информацию о заказе не получили и не сможем проверить
                // корректность остальных параметров
                // А также если тип сообщения не result
            {
                // Получаем информацию о заказе из БД
                $order = new Order($vars->order_id);
                if ($order)
                    // Если не нашли заказ с указанным номером, то возвращаем ошибку
                {
                    // Загружаем настройки для работы с Pay2Pay
                    $pmconfigs = GetConfigs_Pay2Pay();
                    if ($order->order_status == $pmconfigs['trans_status_pending'])
                 // Если по заказу ожидается оплата, то продолжаем проверку

                    {
                        $s = md5($pmconfigs['hidden_key'] . $xml . $pmconfigs['hidden_key']);
                        if ($sign == $s)
                            // Если подпись не совпадает, возвращаем ошибку
                        {
                            $currency = $order->currency_code_iso;
                            // Код валюты заказа преобразуем к формату принятом в Pay2Pay
                            if ($currency == 'RUR')
                                $currency = 'RUB';
                            if (strtoupper($currency) == $vars->currency)
                                // Нельзя принимать платеж, если валюта платежа и заказа не совпали
                            {
                                if ($order->order_total <= $vars->amount)
                                    // Сумма платежа может превышать сумму заказа.
                                    // Например, в случае оплаты через системы денежных переводов
                                {
                                    if (($vars->status == 'success') or ($vars->status == 'fail'))
                                        // Если статус платежа окончательный, то нужно обновить заказ
                                    {
                                        // Выбираем какой статус установить для заказа
                                        $status = $pmconfigs['trans_status_failed'];
                                        if ($vars->status == 'success')
                                            $status = $pmconfigs['trans_status_success'];
                                        // Устанавливаем статус заказа
                                        $order->status = $status;
                                        $order->save();
                                    }
                                }
                                else
                                    $error = 'Amount check failed';
                            }
                            else
                                $error = 'Currency check failed';
                        }
                        else
                            $error = 'Security check failed';
                    }
                }
                else
                    $error = 'Unknown order_id';
            }
            else
                $error = 'Incorrect order_id';
            // Отвечаем серверу Pay2Pay
            if ($error == '')
                $ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                         <result>
                         <status>yes</status>
                         <err_msg></err_msg>
                         </result>";
                                    else
                                        $ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                         <result>
                         <status>no</status>
                         <err_msg>$error</err_msg>
                         </result>";
            die($ret);
        }
        /*---------------------------------------------------------------------------*/
    }

}