<?php

namespace Front;

use Mix\BlockHelper;
use Mix\LayerSender;

class CheckoutController extends BaseController {

    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();
    }

    public function getIndex(){

        $lots = $this->block->checkoutLots(\Auth::id());
        $prizes = $this->block->checkoutPrizes(\Auth::id());

        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = 'чекаут';
        $settings->description = 'чекаут';
        $settings->page_title = 'Забрать выигрыш';
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.checkout', compact('settings','menu','ourSocials','lots', 'prizes'));

    }

    public function postSessionCheckout()
    {
        $input = \Input::all();
        if(is_null(\Session::get('steps_form'))) {
            \Session::put('steps_form', '');
            \Session::put('steps_form.step1', $input);
        } else {
            $session_name = 'steps_form.step'.$input['step'];
            \Session::put($session_name, $input);
        }

        $next_step = $input['step'] + 1;
        if($next_step == 4) {
            \Session::put('steps_form.navs', true);
        }
        $template = 'Front::blocks.checkout.step'.$next_step;
        try
        {
            \View::getFinder()->find($template);

            return \View::make($template);
        }
        catch (\InvalidArgumentException $error)
        {
            return 0;
        }
    }

    public function postGetPartedForm()
    {
        $form = \Session::pull('steps_form');

        $sender = new LayerSender('41$fdkA!0021', $form);
        $sender->result = $sender->send();

        if($sender->result < 500) {

            $settings   = $this->block->settings();
            $settings->isHome = false;
            $settings->keywords = '';
            $settings->description = '';
            $settings->page_title = 'Выигрыш оформлен';
            $menu       = $this->block->menu();
            $ourSocials = $this->block->ourSocials();

            return \View::make('Front::pages_layouts.checkout-success', compact('settings','menu','ourSocials'));

        }

        return \Redirect::to("/checkout");

    }

    public function getActualAddressForm()
    {
        return \View::make('Front::blocks.checkout.actual_address');
    }

    public function getStep($step) {
        $get_step = $step;
        $template = 'Front::blocks.checkout.step'.$get_step;
        return \View::make($template);
    }
}