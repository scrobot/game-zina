<?php namespace Front;

use Illuminate\Support\Facades\Validator;
use Models\User;
use Models\Game;
use Mix\BlockHelper;
use Mix\UserScore;

/**
 * Class UserController
 * @package Front
 */

class UserController extends BaseController {

    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();
    }

    public function postValidationField()
    {
        $input = \Input::all();

        $field = [$input['name'] => $input['val']];

        $validator = \Validator::make($field, User::$validation, User::$messages);

        if(empty($input['val']) || $validator->fails()) {
            return \Response::json([
                'status' => 0,
                'message' => empty($input['val']) ? "Значение не может быть пустым" :$validator->errors()->first($input['name']),
            ]);
        } else {
            return \Response::json([
                'status' => 1,
                'message' => '',
            ]);
        }
    }

	public function postUlogin()
    {
        if(\Input::has('token')) {
            $token = \Input::get('token');
        } elseif(\Input::has('code')) {
            $token = \Input::get('code');
        }

        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $token . '&host=' . $_SERVER['HTTP_HOST']);
        $userRequest = json_decode($s, true);
        $user = User::where('uid', $userRequest['uid'])->first();
        if($user) {
            $user->token_secret = $user->generateCode(32);
            $user->access_token = isset($userRequest['access_token']) ? $userRequest['access_token'] : null;
            $user->save();
            \Auth::login($user);
        } else {

            \Session::put('userTemp', $userRequest);

            return \Redirect::to(action('\Front\UserController@getFinalRegistration'));

        }
        return \Redirect::intended('/');

    }

    public function getFinalRegistration()
    {
            if(\Auth::check()) {
                return \Redirect::to('/');
            } else {
                $settings = $this->block->settings();
                $settings->isHome = false;
                $settings->keywords = '';
                $settings->description = '';
                $settings->page_title = "Закончите процесс регистрации";
                $menu = $this->block->menu();
                $ourSocials = $this->block->ourSocials();

                return \View::make('Front::pages_layouts.user.reg-final', compact('settings', 'menu', 'ourSocials'));
            }


    }


    public function postSocialRegister()
    {
        $input = \Input::all();

        $validator = \Validator::make($input, User::$rules, User::$messages);

        if($validator->fails()) {
            return \Redirect::to('/user/final-registration')->withErrors($validator)->withInput();
        }

        $userRequest = \Session::pull('userTemp');
        $userRequest['nickname'] = $input['nickname'];
        $userRequest['email'] = $input['email'];
        $userRequest['password'] = \Hash::make($input['password']);
        $user = User::create($userRequest);
        $user->client_id = $user->generateClientID();
        $user->activation_code = $user->generateCode();
        $user->save();
        $user->sendRegistrationMail($input['password']);

        \Auth::login($user);
        if($user->verified_email) {
            return \Redirect::to('/');
        } else {
            $user->sendActivationMail();
            return \Redirect::to('/user/registration-success');
        }


    }

    public function getYandexHandler()
    {
        \Session::forget('userTemp');
        $client_id = 'fc05173a56954126b919a508bf18b1bb'; // Id приложения
        $client_secret = '6b6c6898e1f84c7daf80c03b1ce2db43'; // Пароль приложения

        if (isset($_GET['code'])) {
            $result = false;

            $params = array(
                'grant_type'    => 'authorization_code',
                'code'          => $_GET['code'],
                'client_id'     => $client_id,
                'client_secret' => $client_secret
            );

            $url = 'https://oauth.yandex.ru/token';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);
            curl_close($curl);

            $tokenInfo = json_decode($result, true);

            if (isset($tokenInfo['access_token'])) {
                $params = array(
                    'format'       => 'json',
                    'oauth_token'  => $tokenInfo['access_token']
                );
            }

            $userInfo = json_decode(file_get_contents('https://login.yandex.ru/info' . '?' . urldecode(http_build_query($params))), true);

            $user = User::where('uid', $userInfo['id'])->first();
            if($user) {
                $user->token_secret = $user->generateCode(32);
                $user->save();
                \Auth::login($user);
            } else {
                \Session::put('userTemp.email', $userInfo['default_email']);
                \Session::put('userTemp.nickname', $userInfo['login']);
                \Session::put('userTemp.network', 'yandex');
                \Session::put('userTemp.uid', $userInfo['id']);
            }
            return \View::make('Front::blocks.user.afterYandexOAuth');

        }

    }

	public function postRegister()
	{
        $input = \Input::all();

        $validator = \Validator::make($input, User::$rules, User::$messages);

        if($validator->fails()) {
            return \Response::json([
                'status' => 500,
                'errors' => $validator->errors()
            ]);
        } else {

            $password = $input['password'];

            $input['password'] = \Hash::make($input['password']);

            $user = User::create($input);
            $user->client_id = $user->generateClientID();
            $user->save();

            \Auth::login($user);

            $user->register($password);

            return \Response::json([
                'status' => 200,
                'link' => '/user/registration-success'
            ]);
        }

	}

    public function getRegistrationSuccess()
    {
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = '';
        $settings->description = '';
        $settings->page_title = "Уведомление об успешной регистрации";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        if(\Auth::check() && \Auth::user()->verified_email == 0) {
            return \View::make('Front::pages_layouts.user.reg-success', compact('settings','menu','ourSocials'));
        } else {
            return \Redirect::to('/');
        }
    }

	/**
	 * Метод активации пользователей по E-mail
	 * @param $userId
	 * @param $activation_code
	 * @return mixed
	 */

	public function getActivate($userId, $activation_code)
	{
		// Получаем указанного пользователя
		$user = User::find($userId);
		if (!$user) {
			return $this->getMessage("Неверная ссылка на активацию аккаунта.");
		}

		// Пытаемся его активировать с указанным кодом
		if ($user->activate($activation_code)) {
			// В случае успеха авторизовываем его
			\Auth::login($user);
			// И выводим сообщение об успехе
			return $this->getMessage("Аккаунт активирован", "/");
		}

		// В противном случае сообщаем об ошибке
		return $this->getMessage("Неверная ссылка на активацию аккаунта, либо учетная запись уже активирована.");
	}

    /**
     * Метод вывода сообщения и запуска редиректа после активации аккаунта
     * @param $message
     * @param bool $redirect
     * @return mixed
     */

    protected function getMessage($message, $redirect = false)
    {
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = '';
        $settings->description = '';
        $settings->page_title = "Уведомление об успешной активации";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.message', compact('message','redirect','settings','menu','ourSocials'));
    }

	/**
	 * Метод деаутентификации пользователя
	 * @return mixed
	 */

	public function getLogout()
	{
		\Auth::logout();
	    return \Redirect::to('/');
	}

	/**
	 * POST-метод авторизации пользователя
	 * @return int
	 */

	public function postLogin()
    {
        // Формируем базовый набор данных для авторизации
        $login_info = [];
        $login_info['password'] = \Input::get('password');

        // В зависимости от того, что пользователь указал в поле username,
        // дополняем авторизационные данные
        $username = \Input::get('login');
        if (strpos($username, '@')) {
            $login_info['email'] = $username;
            $user = User::where('email', $login_info['email'])->first();
        } else {
            $login_info['nickname'] = $username;
            $user = User::where('nickname', $login_info['nickname'])->first();
        }

        // Пытаемся авторизовать пользователя
        if (\Auth::attempt($login_info, \Input::has('remember'))) {
            $user->token_secret = $user->generateCode(32);
            $user->save();
            return \Response::json([
                'status' => 200,
                'link' => \Input::get('currentUrl'),
                'error' => null
            ]);
        } else {
            $alert = "Неверная комбинация имени (email) и пароля";

            // Возвращаем пользователя назад на форму входа с временной сессионной
            // переменной alert (withAlert)
            return \Response::json([
                'status' => 500,
                'error' => $alert,
                'link' => \Input::get('currentUrl'),
            ]);
        }
	}

	/**
	 * Альтернативный метод регистрации и активации пользовательского аккаунта
	 * @return mixed
	 */

	public function postEmailActivation()
	{

		$user = User::find(\Input::get('uid'));
		$user->email = \Input::get('email');
		$user->save();
		$id = $user->register();

		// Вывод информационного сообщения об успешности регистрации
		return $this->getMessage("Регистрация почти завершена. Вам необходимо подтвердить e-mail, указанный при регистрации, перейдя по ссылке в письме.");

	}

    public function postEmailChange()
    {
        $input = \Input::all();
        $user = User::find(\Auth::id());
        $validator = Validator::make($input, User::$update_email_rule, User::$messages);

        if($validator->fails()) {
            return \Response::json([
                'status' => 500,
                'errors' => $validator->errors()
            ]);
        } else {

            $user->email = $input['email'];
            $user->save();

            $user->sendActivationMail();

            return \Response::json([
                'status' => 200,
                'link' => '/user/email-change'
            ]);
        }
    }

    public function getEmailChange()
    {
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = '';
        $settings->description = '';
        $settings->page_title = "Уведомление о смене e-mail";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();
        $message = "Вы успешно изменили свой email. Проверьте новую указанную почту и активируйте свой аккаунт";
        $redirect = false;
        return \View::make('Front::pages_layouts.message', compact('message','redirect','settings','menu','ourSocials'));
    }

    public function getShow()
    {
        $user = User::find(\Auth::id());
        $user->lots = $user->lotSlice();
        $user->lots_shown = count($user->lots) >= 3 ? true : false;
        $user->pays = $user->paySlice();
        $user->pays_shown = count($user->pays) == 3 ? true : false;
        $user->subscribe_expiration = new \DateTime($user->subscribe_expiration);
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = '';
        $settings->description = '';
        $settings->page_title = "Личный кабинет";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.user.show', compact('settings','menu','ourSocials','user'));

    }

    public function getShowLots()
    {
        $start = \Input::get('start');
        $user = User::find(\Auth::id());
        $user->lots = $user->lotSlice($start, true);
        return \View::make('Front::blocks.user.lots', compact('user'));

    }

    public function getShowPays()
    {
        $start = \Input::get('start');
        $user = User::find(\Auth::id());
        $user->pays = $user->paySlice($start, true);
        return \View::make('Front::blocks.user.pays', compact('user'));
    }


    public function getEdit()
    {
        $user = User::find(\Auth::id());
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = '';
        $settings->description = '';
        $settings->page_title = "Настройка профиля";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.user.edit', compact('settings','menu','ourSocials','user'));
    }

	/**
	 * Обвновление пользователя
	 * @return mixed
	 */

	public function postUpdate()
	{
        $input = \Input::except('password', 'password_confirmation');
        $userArrayUpdate = [];
        $password['password'] = \Input::get('password');
        $password['password_confirmation'] = \Input::get('password_confirmation');

        if(!empty($password['password'])) {
            $validator = \Validator::make($password, User::$update_rules, User::$messages);
            if(!$validator->fails()) {
                $userArrayUpdate['password'] = \Hash::make($password['password']);
            } else {
                return \Redirect::to('/user/edit')->withErrors($validator);
            }
        }

        $user = User::find(\Auth::id());

        $userArrayUpdate['first_name'] = $input['first_name'];
        $userArrayUpdate['last_name'] = $input['last_name'];
        $userArrayUpdate['bdate'] = $input['bdate'];

        if(\Input::file('avatar') && \Input::file('avatar')->isValid()) {

            $fileName = \Input::file('avatar')->getClientOriginalName();
            \Input::file('avatar')->move(public_path().'/uploads/avatars', $fileName);
            $userArrayUpdate['photo_big'] = '/uploads/avatars/'.$fileName;
            $userArrayUpdate['photo'] = '/uploads/avatars/'.$fileName;

        }

        $user->update($userArrayUpdate);

        return \Redirect::back();

	}

}
