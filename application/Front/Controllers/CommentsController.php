<?php

namespace Front;

use Models\Comment;

class CommentsController extends BaseController {

    public function postComment()
    {
        $input = \Input::all();

        $comment = new Comment;
        $comment->comment = $input['comment'];
        $comment->post_type = $input['post_type'];
        switch($input['post_type']) {
            case 1:  $comment->game_id = $input['post_type_id'];
                break;
            case 2:  $comment->post_id = $input['post_type_id'];
                break;
            case 3:  $comment->news_id = $input['post_type_id'];
        }
        $comment->user_id = $input['uid'];
        $comment->reply = $input['reply'];
        $comment->reply_comment_id = !empty($input['reply_comment_id']) ? $input['reply_comment_id'] : null;
        $comment->replier = !empty($input['replier']) ? $input['replier'] : null;
        $comment->save();

        $data = [
            'view' => \View::make('Front::blocks.newComment', compact('comment'))->render()
        ];

        return \Response::json($data, 200);

    }

}