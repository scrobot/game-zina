<?php

namespace Front;

use Models\User;
use Models\FreeGame;
use Mix\BlockHelper;
use Mix\UserScore;

class FreeGamesController extends BaseController {

    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();
    }

    public function getIndex()
    {

        $games = $this->block->freeGames();
        $settings = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = "Все бесплатные игры";
        $settings->description = "бесплатные игры";
        $settings->page_title = "Бесплатные игры";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();
        $superGame  = $this->block->superGame();
        $speedGames = $this->block->speedGames();

        return \View::make('Front::pages_layouts.free-games', compact('games','settings','menu','ourSocials', 'superGame', 'speedGames'));
    }

    public function getGame($alias)
    {
        if(\Auth::check()){
            $user = User::find(\Auth::user()->id);
        } else {
            $user = null;
        }

        $game = $this->block->getFreeGame($alias, $user);
        $gameOther = $this->block->otherFreeGames($alias);
        $settings = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = "Все бесплатные игры";
        $settings->description = "бесплатные игры";
        $settings->page_title = "Бесплатные игры";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();
        $superGame  = $this->block->superGame();
        $speedGames = $this->block->speedGames();
        $game_path = "/public/games/{$game->game_file}";

        return \View::make('Front::pages_layouts.free-game', compact('game','gameOther','game_path','settings','menu','ourSocials', 'superGame', 'speedGames'));
    }

    public function postScore()
    {
        $temp = \Input::all();
        $object = $temp[0];

        unset($temp);

        if(!isset($object['bonus']))
            $object['bonus'] = 0;

        $user = User::where('token_secret',$object['user_token'])->first();

        if($user && !$user->banned) {

            $hash = sha1(md5($object['user_token'].$object['gid'].$object['score']));
            $score = (int) str_replace(" ", '', $object['score'])*1;

            if($hash === $object['hash']){

                $game = FreeGame::find($object['gid']);

                $bon = UserScore::setBonuses($user, $game->coefficient, $object['multiplier'], $score, $object['bonus']);
                \Log::info('Результат игры', [
                    'Пользователь' => $user->nickname,
                    'игра' => $game->title,
                    'счет' => $score,
                    'начислено баллов' => $bon,
                    'бонусные баллы' => $object['bonus']
                ]);
                $user->balance += $bon;
                $user->save();

                return \Response::json(['balance' => $user->balance]);

            } else {

                $user->banned = true;
                $user->balance = 0;
                $user->save();

                return \Redirect::back();

            }

        } else {

            unset($user);

            return \Response::json('fail');

        }
    }

    public function getBinder()
    {
        $userBalance = \Auth::check() ? \Auth::user()->balance : null;
        return \Response::json(['balance' => $userBalance]);
    }
}