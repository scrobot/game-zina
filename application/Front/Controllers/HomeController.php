<?php namespace Front;

use \Mix\BlockHelper;

class HomeController extends BaseController {

    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{

        $settings   = $this->block->settings();
        $settings->isHome = true;
        $banners    = $this->block->banners();
        $menu       = $this->block->menu();
        $superGame  = $this->block->superGame();
        $speedGames = $this->block->speedGames();
        $soonGames  = $this->block->soonGames();
        $news       = $this->block->news();
        $blog       = $this->block->blog();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.home', compact('settings','banners','menu','superGame','speedGames','soonGames','news','blog','ourSocials'));

	}

    public function getPhpInfo()
    {
        return $_SERVER['SERVER_ADDR'];
    }

    public function getBanhammer()
    {
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = "Комната банхамера";
        $settings->description = "Комната банхамера";
        $settings->page_title = "Комната банхамера";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.banhammer', compact('settings','menu','ourSocials'));
    }

}
