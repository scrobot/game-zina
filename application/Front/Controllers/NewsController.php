<?php

namespace Front;

use Mix\BlockHelper;
use Mix\DateHelper;
use Models\News;

class NewsController extends BaseController {

    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();
    }

    public function getIndex()
    {
        $news = News::paginate(5);
        foreach($news as $subject) {
            $subject->created = new DateHelper($subject->created_at);
        }
        $settings   = $this->block->settings();
        $settings->isNews = true;
        $settings->page_title = $settings->news_title;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.news-archive', compact('settings','menu','ourSocials','news'));
    }

    public function getNews($alias)
    {
        $news = News::where('alias', $alias)->first();
        $settings   = $this->block->settings();
        $settings->post_type = 3;
        $settings->post_type_id = $news->id;
        $settings->keywords = $news->keywords;
        $settings->description = $news->description;
        $settings->page_title = $news->title;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();
        $comments = $this->block->objectComments($news);

        return \View::make('Front::pages_layouts.single-news', compact('settings','menu','ourSocials','news', 'comments'));
    }

    public function getCategoryNews($category)
    {
        $news = $this->block->newsTypeRequest($category);
        $settings   = $this->block->settings();
        $settings->isNews = true;
        $settings->page_title = $category == "video" ? "Видео новости" : "Текстовые новости";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.news-archive', compact('settings','menu','ourSocials','news'));
    }

}