<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 22.04.2015
 * Time: 15:08
 */

namespace Front;

use Models\Post;
use Mix\BlockHelper;

class BlogController extends BaseController {

    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();
    }

    public function getIndex()
    {
        $blog = Post::paginate(6);
        $settings   = $this->block->settings();
        $settings->isBlog = true;
        $settings->page_title = $settings->blog_title;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.blog-archive', compact('settings','banners','menu','ourSocials','blog'));

    }

    public function getPost($alias)
    {
        $post = Post::where('alias', $alias)->first();
        $settings   = $this->block->settings();
        $settings->post_type = 2;
        $settings->post_type_id = $post->id;
        $settings->keywords = $post->keywords;
        $settings->description = $post->description;
        $settings->page_title = $post->title;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();
        $comments = $this->block->objectComments($post);

        return \View::make('Front::pages_layouts.single-blog', compact('settings','banners','menu','ourSocials','post', 'comments'));
    }

}