<?php namespace Front;

use \Mix\BlockHelper;
use \Mix\DateHelper;
use Mix\JsonHandler;
use Mix\UserScore;
use Models\Game;
use Models\User;

class GamesController extends BaseController {

    /**
     * Конструктор с Dependency Injection.
     * @param BlockHelper $block
     */

    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();
    }

    /**
     * Метод вывода игры.
     * @param $alias
     * @return mixed
     */

	public function getGame($alias)
	{

        $game = $this->block->getGame($alias);
        $gameOther = $this->block->otherGame($alias);
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->isGame = true;
        // Настройки комментов
        $settings->post_type = 1;
        $settings->post_type_id = $game->id;
        //---//
        $settings->keywords = $game->keywords;
        $settings->description = $game->description;
        $settings->page_title = $game->title;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();
        $comments = $this->block->objectComments($game);
        $game_path = "/games/{$game->game_file}";

        $template = 'Front::pages_layouts.game';

        return \View::make($template, compact('game_path', 'settings','menu','ourSocials', 'game', 'gameOther', 'user', 'comments'));
	}

    /**
     * Метод вывода всех активных турниров турниров.
     * @return mixed
     */

    public function getAllTournaments()
    {
        $settings = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = "Все турниры";
        $settings->description = "Все турниры";
        $settings->page_title = "Турниры";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();
        $superGame  = $this->block->superGame();
        $speedGames = $this->block->speedGames();

        return \View::make('Front::pages_layouts.all_tournaments', compact('settings','menu','ourSocials', 'superGame', 'speedGames'));

    }

    /**
     * Метод записи счета в JSON-файл, с проверкой на существования пользователя и на его состояние бана.
     * @return mixed
     */

    public function postScore()
    {


    }

    /**
     * Метод биндинга
     * Получаем json совпадающий с именем алиса. Создаем экземпляр класса JsonHandler, запрашиваем модель Game. ->
     * Перебираем полученный объект, сортируя его по счету, и записываем во временный массив temp его место, ник и счет. ->
     * Сверяем сессионный ник пользователя с ником из массива, если совпадает, записываем в инстанс массив с его данными. ->
     * Выполняем сортировку Методом которого multiSort экземляра JsonHandler и возврашаем осортированный массив.
     * @param $alias
     * @return mixed
     */

    public function getBinder($alias)
    {

    }

    /**
     * Получаем призы к текущей игре
     * @param $id
     * @return mixed
     */

    public function getPrizes($id)
    {
        $game = Game::find($id);
        return \View::make('Front::blocks.prizes', compact('game'));
    }

    /**
     * Шаблон страницы подписки
     * @return mixed
     */

    public function getSubscriptions()
    {

        $games = $this->block->liveGames();
        foreach($games as $game){
            $week_end = DateHelper::dateReverseConverter($game->end_date);
        }
        $settings   = $this->block->settings();
        $settings->isSubscriptions = true;
        $settings->page_title = $settings->subscriptions_title;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.subscriptions', compact('settings','menu','ourSocials','games','week_end'));
    }

    /**
     * ServerSide-метод, отвечающий за проверку репостоспособности пользователя и начисления ему бонусных баллов за репост.
     * @param string $reposted
     * @return int
     */

    public function postReposted($reposted = 'repost')
    {
        $input = \Input::all();
        $user_token = $input[0]['user_token'];

        $user = User::where('token_secret', $user_token)->first();

        if(!is_null($user) && $user->reposted == 0) {
            $user->balance += 5000;
            $user->reposted = 1;
            $user->save();

            return 1;

        } else {
            return 0;
        }

    }


}
