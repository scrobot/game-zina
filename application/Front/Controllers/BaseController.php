<?php namespace Front;


class BaseController extends \BaseController
{
    public function __construct()
    {
        \View::addNamespace('Front', __DIR__.'/../Views');
        if(!isset($_COOKIE['first_time'])) {
            setcookie('first_time', 1, time()+(60*60*24*30*365));
        } else {
            setcookie('first_time', 0, time()+(60*60*24*30*365));
        }
    }



}