<?php namespace Front;

use Models\Page;
use Models\User;
use Mix\BlockHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;

\App::error(function (ModelNotFoundException $e) {
    return \Response::make('Not Found', 404);
});

class PageController extends BaseController {


    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();
    }

    /**
     * TODO: Отрефакторить в один шаблон
     * @param $alias
     * @return mixed
     */

    public function getPage($alias)
    {
        $page = Page::where('alias', $alias)->firstOrFail();

        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = $page->keywords;
        $settings->description = $page->description;
        $settings->page_title = $page->title;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.page', compact('settings','menu','ourSocials','page'));
    }

    public function getStatistic()
    {
        $user = User::find(\Auth::user()->id);
        $superGames = $this->block->superGamesStatistics($user);
        $speedGames = $this->block->speedGamesStatistics($user);

        $settings   = $this->block->settings();
        $settings->isStatistics = true;
        $settings->page_title = $settings->statistics_title;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.statistics', compact('settings','menu','ourSocials','user', 'superGames', 'speedGames'));
    }

    public function getHallOfGlory()
    {
        $games = $this->block->previousGames();
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = "Зал славы";
        $settings->description = "Зал славы";
        $settings->page_title = "Зал славы";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.glory-hall', compact('settings','menu','ourSocials','user', 'games'));
    }

}