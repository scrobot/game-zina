<?php

namespace Front;

use Mix\BlockHelper;
use Models\Lot;
use Models\User;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Mix\DateHelper;

class AuctionController extends BaseController {

    public function __construct(BlockHelper $block)
    {
        $this->block = $block;

        parent::__construct();

    }

    public function getIndex()
    {
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = "Акуцион";
        $settings->description = "Аукцион";
        $settings->page_title = "Аукцион";
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.auction', compact('settings','menu','ourSocials'));

    }

    public function getLot($alias)
    {
        $lot = $this->block->singleLot($alias);
        $settings   = $this->block->settings();
        $settings->isHome = false;
        $settings->keywords = $lot->keywords;
        $settings->description = $lot->description;
        $settings->page_title = $lot->title;
        $settings->page_alias = $lot->alias;
        $settings->lot_description = $lot->lot_description;
        $settings->lot_characteristics = $lot->lot_characteristics;
        $settings->lot_navigation = $lot->navigation;
        $menu       = $this->block->menu();
        $ourSocials = $this->block->ourSocials();

        return \View::make('Front::pages_layouts.auction-lot', compact('settings','menu','ourSocials'));
    }

    public function postBet()
    {
        $input = \Input::all();

        $user = User::where('token_secret', $input['token_secret'])->first();
        $lot = Lot::where('SKU', $input['sku'])->first();
        $maxBet = $lot->getBestBet();

        if($user->balance >= $maxBet) {

            $lot->user()->attach($user->id, [
                'bet' => $input['bet'],
                'bet_time' => date('Y.m.d H:i:s', time())
            ]);

            $user->balance -= $input['bet'];
            $user->save();

            $lots = $this->block->lots();

            $data = [
                'lots' => $lots,
                'expired_lots' => $this->block->expiredLots(),
                'userBalance' => $user->balance,
                'status' => 1
            ];

            return \Response::json($data);

        } else {
            $data = [
                'lots' => null,
                'expired_lots' => null,
                'status' => 0,
                'userBalance' => $user->balance,
                'error' => 'Недостаточно баллов для ставки'
            ];

            return \Response::json($data);
        }


    }

    public function postSingleBet($alias)
    {
        $input = \Input::all();

        $user = User::where('token_secret', $input['token_secret'])->first();
        $lot = Lot::where('SKU', $input['sku'])->first();
        $maxBet = $lot->getBestBet();

        if($user->balance >= $maxBet) {

            $lot->user()->attach($user->id, [
                'bet' => $input['bet'],
                'bet_time' => date('Y.m.d H:i:s', time())
            ]);

            $user->balance -= $input['bet'];
            $user->save();

            $lot = $this->block->singleLot($alias);

            $data = [
                'lot' => $lot,
                'userBalance' => $user->balance,
                'status' => 1
            ];

            return \Response::json($data);

        } else {
            $data = [
                'lot' => null,
                'status' => 0,
                'userBalance' => $user->balance,
                'error' => 'Недостаточно баллов для ставки'
            ];

            return \Response::json($data);
        }


    }

    public function getLotsBinder()
    {
        $d = [];
        $d['lots'] = $this->block->lots();
        $d['expired_lots'] = $this->block->expiredLots();
        return \Response::json($d);
    }

    public function getSingleLotBinder($alias)
    {
        $lot = $this->block->singleLot($alias);
        return \Response::json($lot);
    }

    public function getEventListener()
    {
            $response = new StreamedResponse(function() {
            $d = [];

            while (true) {
                $d['lots'] = $this->block->lots();
                $d['expired_lots'] = $this->block->expiredLots();

                if (count($d)) {
                    echo 'data: ' . json_encode($d) . "\n\n";
                    flush();
                    ob_flush();
                }
                sleep(3);
            }
        });

        $response->headers->set('Content-Type', 'text/event-stream');
        return $response;
    }

    public function getSingleEventListener($alias)
    {

        $response = new StreamedResponse(function() use ($alias) {


            while (true) {
                $lot = $this->block->singleLot($alias);

                if (!empty($lot)) {
                    echo 'data: ' . json_encode($lot) . "\n\n";
                    flush();
                    ob_flush();
                }
                sleep(3);
            }
        });

        $response->headers->set('Content-Type', 'text/event-stream');
        return $response;
    }

}