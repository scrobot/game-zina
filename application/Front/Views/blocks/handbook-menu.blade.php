<ul>
    @foreach($handbookMenu as $menu_item)
        <li class="{{\Request::segment(2) == $menu_item->alias ? 'active-page' : ''}}">
            @if(\Request::segment(2) == $menu_item->alias)
                <a href="javascript:void(0)">{{$menu_item->title}}</a>
                @if(count($menu_item->children))
                <ul>
                    @foreach($menu_item->children as $menu_item_child)
                        <li><a href="#{{$menu_item_child->alias}}">{{$menu_item_child->title}}</a></li>
                    @endforeach
                </ul>
                @endif
            @else
            <a href="/handbook/{{$menu_item->alias}}">{{$menu_item->title}}</a>
            @endif
        </li>
    @endforeach
</ul>