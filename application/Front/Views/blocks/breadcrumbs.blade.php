<ul class="breadcrumb">
    <li><a href="/">Главная</a> <i class="fa fa-angle-double-right"></i> </li>
    @if(count($settings->breadcrumbs) == 1 || $settings->breadcrumbs[0] == 'page')
        <li class="active">{{$settings->page_title}}</li>
    @elseif(count($settings->breadcrumbs) == 2)
        @if($settings->breadcrumbs[0] == 'free-games')
            <li><a href="/free-games">Бесплатные игры</a> <i class="fa fa-angle-double-right"></i> </li>
        @elseif($settings->breadcrumbs[0] == 'tournament')
            <li><a href="/tournament">Турниры</a> <i class="fa fa-angle-double-right"></i> </li>
        @elseif($settings->breadcrumbs[0] == 'blog')
            <li><a href="/blog">Блог</a> <i class="fa fa-angle-double-right"></i> </li>
        @elseif($settings->breadcrumbs[0] == 'news')
            <li><a href="/news">Новости</a> <i class="fa fa-angle-double-right"></i> </li>
        @endif
        <li class="active">{{$settings->page_title}}</li>
    @elseif(count($settings->breadcrumbs) == 3)
        @if($settings->breadcrumbs[0] == 'auctions')
            <li><a href="/auctions">Аукционы</a> <i class="fa fa-angle-double-right"></i> </li>
            <li class="active">{{$settings->page_title}}</li>
        @elseif($settings->breadcrumbs[0] == 'news')
            <li><a href="/news">Новости</a> <i class="fa fa-angle-double-right"></i> </li>
            <li class="active">{{$settings->page_title}}</li>
        @endif
    @endif
</ul>