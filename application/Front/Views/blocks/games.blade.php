<div class="mobile-super col-xs-12 hidden-md hidden-sm hidden-lg">
    @if($settings->isHome)
        <h2 class="title">Игровые турниры</h2>
    @endif
    @if(isset($superGame))
        <div class="game-wrapper clearfix">
            <div class="game-title">{{$superGame->title}}</div>
            <a href="/page/pravila">Подробнее</a>
            <p class="start-info"><strong>{{$superGame->isStarted ? "начало турнира" : "окончание турнира"}}<br/> {{$superGame->isStarted ? $superGame->start->date : $superGame->end->date}}</strong><br/> в <strong>20:00</strong> по мСК</p>
            <a href="/tournament/{{$superGame->alias}}">начать {{$superGame->isStarted ? "Тренировку" : "Игру"}}</a>
            <a href="/tournament/{{$superGame->alias}}#prizes" class="all-prizes">посмотреть все призовые места</a>
        </div>
    @endif
</div>
<div class="super hidden-xs col-md-12">
    @if($settings->isHome)
        <h2 class="title">Игровые турниры</h2>
    @endif
    @if(isset($superGame))
        <div class="game-wrapper clearfix" style="background-image: url('/uploads/poster/super/{{$superGame->thumbnail}}')">
            <div class="scl left-column-info">
                <div class="game-title"><a href="/tournament/{{$superGame->alias}}">{{$superGame->title}}</a></div>
                <a href="/tournament/{{$superGame->alias}}">начать {{$superGame->isStarted ? "Тренировку" : "Игру"}}</a>
            </div>
            <div class="scl right-column-info">
                <h3>Супер игра</h3>
                <p class="start-info"><strong> {{$superGame->isStarted ? "начало турнира" : "окончание турнира"}} {{$superGame->isStarted ? $superGame->start->date : $superGame->end->date}}</strong> в <strong>20:00</strong> по мСК</p>
                <a href="/page/pravila">Подробнее</a>
                <div class="timer">
                    <i>{{$superGame->isStarted ? "старт" : "конец"}}<br/> через</i>
                    <div class="timer-wrap">
                        158:12:36
                    </div>
                </div>
            </div>
            <a href="/tournament/{{$superGame->alias}}#prizes" class="all-prizes">посмотреть все призовые места</a>
        </div>
    @endif
</div>
<div class="speed col-xs-12">
    <div id="speed-games">
        @foreach($speedGames as $speedGame)
            <div class="item col-xs-12 col-sm-6 col-md-3">
                <a href="/tournament/{{$speedGame->alias}}"><img src="/uploads/poster/speed/{{$speedGame->thumbnail_360x250}}" alt="{{$speedGame->alias}}"></a>
                <div class="caption">
                    <h3><a href="/tournament/{{$speedGame->alias}}">{{$speedGame->title}}</a></h3>
                    <h4 class="main-prize"><i>приз за первое место:</i><br/> {{$speedGame->firstPrize}} рублей</h4>
                    <a href="/tournament/{{$speedGame->alias}}#prizes" class="all-prizes">призовые места</a>
                    <a href="/tournament/{{$speedGame->alias}}" class="go-to-game">Играть</a>
                </div>
            </div>
        @endforeach
    </div>
</div>