<section class="comments clearfix">

    <div class="comments-content clearfix">
        @if($comments)
        @foreach($comments as $comment)
        <div class="col-xs-12 comment" id="comment-{{$comment->id}}">
            <div class="col-xs-4 col-sm-2 col-md-1">
                <img src="{{$comment->user->photo}}" alt="{{$comment->user->nickname}}" class="avatar">
            </div>
            <div class="col-xs-8 col-sm-10 col-md-11">
                <div class="nickname">{{$comment->user->nickname}}</div>
                @if($comment->reply)
                    <p><a href="#comment-{{$comment->reply_comment_id}}">{{$comment->replier}}</a>, {{$comment->comment}}</p>
                @else
                <p>{{$comment->comment}}</p>
                @endif
                <a href="#comment-form" class="comment-reply" data-commentid="{{$comment->id}}" data-replier="{{$comment->user->nickname}}">Ответить</a>
            </div>
        </div>
        @endforeach
        @else
            <div class="col-xs-12 comment comment-empty" style="padding: 0 25px;">
                <h3 style="color: #ffdd00">Пока нет ни одного комментария.</h3>
                <p>Но скоро они обязательно появятся =)</p>
            </div>
        @endif
    </div>

    @if(\Auth::check())
        {{\Form::open(['action' => '\Front\CommentsController@postComment', 'id' => 'comment-form', 'class' => 'clearfix', 'role' => 'form'])}}
            <h3>Добавить комментарий</h3>
            <p>Разрешено комментировать только на русском языке. Запрещено использовать мат, транслит.</p>
            {{\Form::textarea('comment', null, ['class' => 'form-control', 'rows' => 5, 'placeholder' => 'Введите текст своего комментария', 'required' => 'required'])}}
            {{\Form::hidden('uid', \Auth::user()->id)}}
            {{\Form::hidden('post_type', $settings->post_type)}}
            {{\Form::hidden('post_type_id', $settings->post_type_id)}}
            {{\Form::hidden('reply', 0, ['id' => 'reply_field'])}}
            {{\Form::hidden('reply_comment_id', null, ['id' => 'reply_comment_id_field'])}}
            {{\Form::hidden('replier', null, ['id' => 'replier'])}}
            {{\Form::button('Отправить комментарий', ['class' => 'btn btn-default', 'type' => 'button'])}}
        {{\Form::close()}}
    @else
        <div class="col-xs-12 denined-comments">
            <p>Чтобы оставлять комментарии вам необходимо <a href="#" data-toggle="modal" data-target="#sign-in">войти</a> под своим аккаунтом.<br/> Если вы еще не зарегистрированы, то можете пройти <a href="#" data-toggle="modal" data-target="#sign-up">экспресс-регистрацию</a>, которая займет всего пару минут.</p>
        </div>
    @endif
</section>