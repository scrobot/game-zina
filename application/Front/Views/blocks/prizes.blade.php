<?/**
 * TODO: Возможно нужно удалить этот шаблон. Оставлю пока, на всякий случай.
 */
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="prizesLabel">Призовые места Турнира "{{$game->title}}"</h4>
</div>
<div class="modal-body">
    <table class="table table-striped">
        <thead>
        <th>Место</th>
        <th>приз</th>
        </thead>
        <tbody>
        @foreach($game->prizes as $prize)
            <tr>
                <td>{{$prize->place}}</td>
                <td><? $prize->prize / 1 != 0 ? print $prize->prize." рублей" : print $prize->prize ?></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
