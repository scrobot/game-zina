<div class="form-group clearfix">
    <div class="col-xs-12">
        {{ \Form::text('actual_postcode', \Session::get('steps_form.step2.actual_postcode', null), ['class' => 'postcode', 'placeholder' => 'Индекс*', 'required' => 'required'])}}
    </div>
    <div class="col-xs-12">
        {{ \Form::text('actual_region', \Session::get('steps_form.step2.actual_region', null), ['class' => '', 'placeholder' => 'Регион*', 'required' => 'required'])}}
    </div>
    <div class="col-xs-12">
        {{ \Form::text('actual_district', \Session::get('steps_form.step2.actual_district', null), ['class' => '', 'placeholder' => 'Район или Город*', 'required' => 'required'])}}
    </div>
    <div class="col-xs-12">
        {{ \Form::text('actual_town', \Session::get('steps_form.step2.actual_town', null), ['class' => '', 'placeholder' => 'Населенный пункт*', 'required' => 'required'])}}
    </div>
    <div class="col-xs-12 col-sm-3 first">
        {{ \Form::text('actual_street', \Session::get('steps_form.step2.actual_street', null), ['class' => '', 'placeholder' => 'Улица*', 'required' => 'required'])}}
    </div>
    <div class="col-xs-12 col-sm-3">
        {{ \Form::text('actual_housing', \Session::get('steps_form.step2.actual_housing', null), ['class' => '', 'placeholder' => 'Корпус'])}}
    </div>
    <div class="col-xs-12 col-sm-3">
        {{ \Form::text('actual_building', \Session::get('steps_form.step2.actual_building', null), ['class' => '', 'placeholder' => 'Дом*', 'required' => 'required'])}}
    </div>
    <div class="col-xs-12 col-sm-3 last">
        {{ \Form::text('actual_flat', \Session::get('steps_form.step2.actual_flat', null), ['class' => '', 'placeholder' => 'Квартира'])}}
    </div>
</div>