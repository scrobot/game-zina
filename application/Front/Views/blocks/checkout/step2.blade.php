<div class="checkout-form col-xs-12">
    @if(\Session::get('steps_form.navs'))
        <a href="#" class="steps-nav prev" data-step="1"><i class="fa fa-chevron-left"></i></a>
        <a href="#" class="steps-nav next" data-step="3"><i class="fa fa-chevron-right"></i></a>
    @endif
    <h2>персональные данные. <span>Шаг 2 из 4</span></h2>
    {{\Form::open(['action' => '\Front\CheckoutController@postSessionCheckout', 'role' => 'form', 'files' => true])}}
    <h3>Паспортные данные</h3>
    <div class="form-group clearfix">
        <div class="col-xs-12 col-sm-6 sm-left">
            {{ \Form::text('division_code', \Session::get('steps_form.step2.division_code', null), ['class' => '', 'placeholder' => 'Код подразделения*', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12 col-sm-6">
            {{ \Form::text('date_of_issue', \Session::get('steps_form.step2.date_of_issue', null), ['class' => '', 'placeholder' => 'Дата выдачи* (дд.мм.гггг)', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12">
            {{ \Form::textarea('who_issued', \Session::get('steps_form.step2.who_issued', null), ['class' => '', 'placeholder' => 'Кем выдан*', 'required' => 'required', 'rows' => 2]) }}
        </div>
        <div class="col-xs-12 col-sm-6 sm-left">
            {{ \Form::text('serial_number', \Session::get('steps_form.step2.serial_number', null), ['class' => '', 'placeholder' => 'Серия и номер*', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12 col-sm-6">
        {{ \Form::text('birthday', \Session::get('steps_form.step2.birthday', null), ['class' => '', 'placeholder' => 'Дата рождения* (дд.мм.гггг)', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12">
            {{ \Form::text('birthplace', \Session::get('steps_form.step2.birthplace', null), ['class' => 'col-xs-12', 'placeholder' => 'Место рождения*', 'required' => 'required'])}}
        </div>
    </div>
    <h3>Адрес по месту регистрации</h3>
    <div class="form-group clearfix">
        <div class="col-xs-12">
            {{ \Form::text('registration_postcode', \Session::get('steps_form.step2.registration_postcode', null), ['class' => 'postcode', 'placeholder' => 'Индекс*', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12">
            {{ \Form::text('registration_region', \Session::get('steps_form.step2.registration_region', null), ['class' => '', 'placeholder' => 'Регион*', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12">
            {{ \Form::text('registration_district', \Session::get('steps_form.step2.registration_district', null), ['class' => '', 'placeholder' => 'Район или Город*', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12">
            {{ \Form::text('registration_town', \Session::get('steps_form.step2.registration_town', null), ['class' => '', 'placeholder' => 'Населенный пункт*', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12 col-sm-3 first">
            {{ \Form::text('registration_street', \Session::get('steps_form.step2.registration_street', null), ['class' => '', 'placeholder' => 'Улица*', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12 col-sm-3">
            {{ \Form::text('registration_housing', \Session::get('steps_form.step2.registration_housing', null), ['class' => '', 'placeholder' => 'Корпус'])}}
        </div>
        <div class="col-xs-12 col-sm-3">
            {{ \Form::text('registration_building', \Session::get('steps_form.step2.registration_building', null), ['class' => '', 'placeholder' => 'Дом*', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12 col-sm-3 last">
            {{ \Form::text('registration_flat', \Session::get('steps_form.step2.registration_flat', null), ['class' => '', 'placeholder' => 'Квартира'])}}
        </div>
    </div>
    <h3>Адрес фактического проживания</h3>
    <div class="form-group clearfix">
        <div class="checkbox">
            <label for="matches">
                {{ \Form::checkbox('matches', true, \Session::get('steps_form.step2.matches'), ['id' => 'matches', 'class' => \Session::get("steps_form.step2.matches") ? 'active' : '']) }} Совпадает с адресом постоянной регистрации
            </label>
        </div>
        <div id="actual_address">

        </div>
    </div>
    {{ \Form::hidden('step', 2) }}
    {{ \Form::hidden('utoken', \Auth::user()->token_secret) }}
    {{ \Form::button('Далее', ['type' => 'submit']) }}
    {{\Form::close()}}
</div>
@unless(\Session::get('steps_form.step2.matches'))
<script type="text/javascript">
    $('#actual_address').load('/checkout/actual-address-form', function(){
        $('#actual_address input').each(function(){
            $(this).attr('required', 'required')
            $(this).focusout(function(){
                $(this).valid();
            })

        })
        $('#actual_address input[name="actual_housing"]').removeAttr('required');
        $('#actual_address input[name="actual_flat"]').removeAttr('required');
        $('.checkout-form form input[name="actual_postcode"]').inputmask("mask", {"mask": "9{6}"}); //specifying fn & options
        $('.checkout-form form input[name="actual_housing"]').inputmask("mask", {"mask": "9{1,6}"}); //specifying fn & options
        $('.checkout-form form input[name="actual_building"]').inputmask("mask", {"mask": "9{1,6}"}); //specifying fn & options
        $('.checkout-form form input[name="actual_flat"]').inputmask("mask", {"mask": "9{1,6}"}); //specifying fn & options
    });
</script>
@endunless