<div class="checkout-form col-xs-12">
    @if(\Session::get('steps_form.navs'))
        <a href="#" class="steps-nav next" data-step="2"><i class="fa fa-chevron-right"></i></a>
    @endif
    <h2>Общая информация. <span>Шаг 1 из 4</span></h2>
    {{\Form::open(['action' => '\Front\CheckoutController@postSessionCheckout', 'role' => 'form', 'files' => true, 'class' => 'clearfix'])}}
    <h3>Заполните заявку для получения выигрыша</h3>
    <div class="form-group">
        <div class="col-xs-12">
            {{ \Form::text('fio', \Session::get('steps_form.step1.fio', null), ['class' => 'fio', 'placeholder' => 'Фамилия Имя Отчество*', 'required' => 'required'])}}
            <label class="col-xs-12 fio-error-message">Недостаточно информации. Введите фамилию, имя и отчество через пробел (Например: Иванов Иван Алексеевич)</label>
        </div>
        <div class="col-xs-12 col-sm-6 sm-left">
            {{ \Form::text('phone', \Session::get('steps_form.step1.phone', null), ['class' => 'phone', 'placeholder' => 'Мобильный телефон*', 'required' => 'required'])}}
        </div>
        <div class="col-xs-12 col-sm-6">
            {{ \Form::text('email', \Session::get('steps_form.step1.email', null), ['class' => 'email', 'placeholder' => 'Электронная почта', 'required' => 'required'])}}
        </div>
        {{ \Form::hidden('step', 1) }}
        {{ \Form::hidden('utoken', \Auth::user()->token_secret) }}
        {{ \Form::button('Далее', ['type' => 'submit']) }}
    </div>
    {{\Form::close()}}
</div>
