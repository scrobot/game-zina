<div class="checkout-form col-xs-12">
    @if(\Session::get('steps_form.navs'))
        <a href="#" class="steps-nav prev" data-step="3"><i class="fa fa-chevron-left"></i></a>
    @endif
    <h2>Проверка данных. <span>Шаг 4 из 4</span></h2>
    <div id="infos">
        <h3>Общая информация.</h3>
        <p><strong>Ф.И.О.:</strong> {{{\Session::get('steps_form.step1.fio')}}}</p>
        <p><strong>Телефон:</strong> {{{\Session::get('steps_form.step1.phone')}}}</p>
        <p><strong>E-mail:</strong> {{{\Session::get('steps_form.step1.email')}}}</p>
        <h3>Паспортные данные</h3>
        <p><strong>Код подразделения:</strong> {{{\Session::get('steps_form.step2.division_code')}}}</p>
        <p><strong>Дата выдачи:</strong> {{{\Session::get('steps_form.step2.date_of_issue')}}}</p>
        <p><strong>Кто выдал:</strong> {{{\Session::get('steps_form.step2.who_issued')}}}</p>
        <p><strong>Серия и номер паспорта:</strong> {{{\Session::get('steps_form.step2.serial_number')}}}</p>
        <p><strong>Дата рождения:</strong> {{{\Session::get('steps_form.step2.birthday')}}}</p>
        <p><strong>Место рождения:</strong> {{{\Session::get('steps_form.step2.birthplace')}}}</p>
        <h3>Адрес по месту регистрации</h3>
        <p><strong>Индекс:</strong> {{{\Session::get('steps_form.step2.registration_postcode')}}}</p>
        <p><strong>Регион:</strong> {{{\Session::get('steps_form.step2.registration_region')}}}</p>
        <p><strong>Район или Город:</strong> {{{\Session::get('steps_form.step2.registration_district')}}}</p>
        <p><strong>Населенный пункт:</strong> {{{\Session::get('steps_form.step2.registration_town')}}}</p>
        <p><strong>Улица:</strong> {{{\Session::get('steps_form.step2.registration_street')}}}</p>
        <p><strong>Корпус: </strong> {{{\Session::get('steps_form.step2.registration_housing')}}}</p>
        <p><strong>Дом: </strong> {{{\Session::get('steps_form.step2.registration_building')}}}</p>
        <p><strong>Квартира: </strong> {{{\Session::get('steps_form.step2.registration_flat')}}}</p>
        <h3>Адрес фактического проживания</h3>
        @if(\Session::get('steps_form.step2.matches'))
            <p><strong>Адрес проживания:</strong> Совпадает с адресом регистрации</p>
        @else
            <p><strong>Индекс:</strong> {{{\Session::get('steps_form.step2.actual_postcode')}}}</p>
            <p><strong>Регион:</strong> {{{\Session::get('steps_form.step2.actual_region')}}}</p>
            <p><strong>Район или Город:</strong> {{{\Session::get('steps_form.step2.actual_district')}}}</p>
            <p><strong>Населенный пункт:</strong> {{{\Session::get('steps_form.step2.actual_town')}}}</p>
            <p><strong>Улица:</strong> {{{\Session::get('steps_form.step2.actual_street')}}}'</p>
            <p><strong>Корпус:</strong> {{{\Session::get('steps_form.step2.actual_housing')}}}</p>
            <p><strong>Строение:</strong> {{{\Session::get('steps_form.step2.actual_building')}}}</p>
            <p><strong>Квартира:</strong> {{{\Session::get('steps_form.step2.actual_flat')}}}</p>
        @endif
        <h3>Банковские реквизиты</h3>
        <p><strong>Наименование банка:</strong> {{{\Session::get('steps_form.step3.bank')}}}</p>
        <p><strong>Расчетный счет:</strong> {{{\Session::get('steps_form.step3.schet')}}}</p>
    </div>
    {{\Form::open(['action' => '\Front\CheckoutController@postGetPartedForm', 'role' => 'form', 'files' => true])}}
    {{ \Form::hidden('utoken', \Auth::user()->token_secret) }}
    {{ \Form::button('Отправить', ['type' => 'submit', 'class' => 'fuck']) }}
    {{\Form::close()}}
</div>

