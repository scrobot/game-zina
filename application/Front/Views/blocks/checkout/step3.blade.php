<div class="checkout-form col-xs-12">
    @if(\Session::get('steps_form.navs'))
        <a href="#" class="steps-nav prev" data-step="2"><i class="fa fa-chevron-left"></i></a>
        <a href="#" class="steps-nav next" data-step="4"><i class="fa fa-chevron-right"></i></a>
    @endif
    <h2>Банковские реквизиты. <span>Шаг 3 из 4</span></h2>
    {{\Form::open(['action' => '\Front\CheckoutController@postSessionCheckout', 'role' => 'form', 'files' => true])}}
    <h3>Банковские реквизиты, необходимые для безналичного перечисления выигрышей</h3>
    <div class="form-group">
        {{ \Form::text('bank', \Session::get('steps_form.step3.bank', null), ['class' => 'col-xs-12', 'placeholder' => 'Наименование банка', 'required' => 'required'])}}
        {{ \Form::text('schet', \Session::get('steps_form.step3.schet', null), ['class' => 'col-xs-12', 'placeholder' => 'Корреспондентский счет отделения банка', 'required' => 'required'])}}
    </div>
    {{ \Form::hidden('step', 3) }}
    {{ \Form::hidden('utoken', \Auth::user()->token_secret) }}
    {{ \Form::button('Далее', ['type' => 'submit']) }}
    {{\Form::close()}}
</div>
