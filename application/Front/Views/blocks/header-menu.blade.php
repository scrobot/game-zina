<section class="menu">
    <ul class="clearfix">
        @if($settings->isHome)
        <li class="active-page">
        @else
        <li>
        @endif
            <a href="/" class="home">Главная</a>
        </li>
        @foreach($menu->header as $item)
            <li class="<? "http://gz.scrobot-group.ru".$item->link === \Request::url() ? print 'active-page' : ''?>">
                <a href="{{$item->link}}">{{$item->title}}</a>
            </li>
        @endforeach
    </ul>
</section>