<section class="banners row">
    <div class="col-xs-12">
        <ul class="rslides" id="advertisement-slider">
            @foreach($banners as $banner)
                <li><a href="{{$banner->link}}"><img src="/uploads/banners/{{$banner->image}}"></a></li>
            @endforeach
        </ul>
    </div>
</section>