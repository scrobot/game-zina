<nav class="footer-menu">
    <a href="/">Главная</a>
    @foreach($menu->footer as $item)
        <a href="{{$item->link}}">{{$item->title}}</a>
    @endforeach
</nav>