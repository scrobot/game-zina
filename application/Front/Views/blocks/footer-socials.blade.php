<nav class="our-socials">
    <span class="soc-text">Мы в</br> соцсетях</span>
    <div class="soc-icons">
        @foreach($ourSocials as $class => $link)
            <a href="{{$link}}" class="{{$class}}" target="_blank">{{$link}}</a>
        @endforeach
    </div>
</nav>