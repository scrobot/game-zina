@if(count($user->lots))
<table class="table">
    <thead>
    <tr>
        <th>Лот</th>
        <th>Название</th>
        <th>Дата</th>
        <th>Ставка</th>
    </tr>
    </thead>
    <tbody>
    @foreach($user->lots as $lot)
        <tr>
            <td>#{{$lot['SKU']}}</td>
            <td><span>{{$lot['title']}}</span></td>
            <td>{{date_format(date_create($lot['expiration_date']), 'd.m.Y')}}</td>
            <td>{{$lot['winner_bet']}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    <p>Вы еще не выиграли ни один лот.</p>
@endif