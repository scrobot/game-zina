@if(count($user->pays))
    <table class="table pays">
        <thead>
        <tr>
            <th>Дата</th>
            <th>Сумма</th>
            <th>Статус</th>
        </tr>
        </thead>
        <tbody>
        @foreach($user->pays as $pay)
            <tr>
                <td>{{date_format(date_create($pay['updated_at']), 'd.m.Y')}}</td>
                <td>{{$pay['sum']}} рублей</td>
                <td><span class="{{$pay['status_id'] == 2 ? 'success' : 'other'}}">{{$pay['status_name']}}</span></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p>Вы еще не сделали ни один платеж.</p>
@endif