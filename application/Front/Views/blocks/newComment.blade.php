<div class="col-xs-12 comment" id="comment-{{$comment->id}}">
    <div class="col-xs-4 col-sm-2 col-md-1">
        <img src="{{$comment->user->photo}}" alt="{{$comment->user->nickname}}" class="avatar">
    </div>
    <div class="col-xs-8 col-sm-10 col-md-11">
        <div class="nickname">{{$comment->user->nickname}}</div>
        @if($comment->reply)
            <p><a href="#comment-{{$comment->reply_comment_id}}">{{$comment->replier}}</a>, {{$comment->comment}}</p>
        @else
            <p>{{$comment->comment}}</p>
        @endif
        <a href="#comment-form" class="comment-reply" data-commentid="{{$comment->id}}" data-replier="{{$comment->user->nickname}}">Ответить</a>
    </div>
</div>