<footer class="footer container">
    <section class="row">
        <div class="col-xs-12 col-sm-3">
            <nav class="partners-links">
                <a href="http://pay2pay.com"><img src="/images/pay2pay_88x31_green.png" alt="pay2pay" width="95" height="35" /></a>
                <a href="http://www.megastock.ru"><img src="/images/wm_acc_blue_transp_ru_b0f9ab30a7a5f347469297c440847465.png" alt="webmoney" width="95" height="35" /></a>
            </nav>
            <p class="copyrights">&copy; {{$settings->copyrights}}</p>
        </div>
        <div class="col-xs-12 col-sm-3">
            @include('Front::blocks.footer-socials')
        </div>
        <div class="col-xs-12 col-sm-6">
            @include('Front::blocks.footer-menu')
        </div>
    </section>
</footer>
<script type="text/javascript" src="/Front/js/libs/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/Front/js/libs/bootstrap.min.js"></script>
<script type="text/javascript" src="/Front/js/libs/bootstrap-filestyle.min.js"> </script>
<script type="text/javascript" src='/Front/js/libs/imagelightbox.min.js'></script>
<script type="text/javascript" src="/Front/js/libs/responsiveslides.min.js"></script>
<script type="text/javascript" src='/Front/js/libs/flipclock.min.js'></script>
<script type="text/javascript" src='/Front/js/libs/jquery.flash.js'></script>
<script type="text/javascript" src='/Front/js/libs/rivets.bundled.min.js'></script>
<script type="text/javascript" src="/Front/js/libs/eventsource.js"></script>
<script type="text/javascript" src="/Front/js/scripts/app.js"></script>
<script type="text/javascript" src="/Front/js/scripts/funcs.js"></script>
<script type="text/javascript" src="//ulogin.ru/js/ulogin.js"></script>
<script>
    function preview(token){
        $.getJSON("//ulogin.ru/token.php?host=" +
                encodeURIComponent(location.toString()) + "&token=" + token + "&callback=?",
                function(data){
                    data=$.parseJSON(data.toString());
                    if(!data.error){
                        alert("Привет, "+data.first_name+" "+data.last_name+"!");
                    }
                });
    }
</script>
<script>
    $('body').on('click', 'div[title="Yandex"]', function(){
        window.location.href = 'http://gz.scrobot-group.ru/user/final-registration'
    })
</script>