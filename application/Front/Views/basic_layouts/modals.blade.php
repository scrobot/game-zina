<!-- Modal -->
<div class="modal fade" id="sign-up" tabindex="-1" role="dialog" aria-labelledby="regLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="regLabel">Регистрация</h4>
            </div>
            <div class="modal-body">
                <div class="social-enter">
                    <div class="menu-title">Войти через соц.сети</div>
                    <nav class="social-enter-menu">
                        <script src="//ulogin.ru/js/ulogin.js"></script><div id="uLogin_6daabba3" data-uloginid="6daabba3"></div>
                    </nav>
                </div>
                {{\Form::open(['action' => '\Front\UserController@postRegister', 'role' => 'form', 'id' => 'reg', 'class' => 'reg-form form-validate'])}}
                    <div class="form-group fg-email">
                        {{\Form::label('email', 'E-mail')}}
                        {{\Form::email('email', null, ["class"=>"form-control", "placeholder"=>"Введите email", 'required' => 'required'])}}
                        <p class="error"></p>
                    </div>
                    <div class="form-group fg-nick">
                        {{\Form::label('nickname', 'Логин')}}
                        {{\Form::text('nickname', null, ["class"=>"form-control", "placeholder"=>"Придумайте логин", 'required' => 'required'])}}
                        <p class="error"></p>
                    </div>
                    <div class="form-group fg-pass">
                        {{\Form::label('password', 'Пароль')}}
                        {{\Form::password('password', ["class"=>"form-control", "placeholder"=>"Придумайте пароль", 'required' => 'required'])}}
                        <p class="error"></p>
                    </div>
                    <div class="checkbox">
                        <label>
                            {{\Form::checkbox('agree', 1, true)}} Я согласен с <a href="/page/pravila">правилами</a>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default btn-request">Регистрация</button>
                {{\Form::close()}}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="sign-in" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="loginLabel">Авторизация</h4>
            </div>
            <div class="modal-body">
                <div class="social-enter">
                    <div class="menu-title">Войти через соц.сети</div>
                    <nav class="social-enter-menu">
                        <script src="//ulogin.ru/js/ulogin.js"></script><div id="uLogin_83a75edc" data-uloginid="83a75edc"></div>
                    </nav>
                </div>
                <div class="error-message"><p></p></div>
                {{\Form::open(['action' => '\Front\UserController@postLogin', 'role' => 'form', 'class' => 'reg-form auth-form'])}}
                    <div class="form-group">
                        {{\Form::label('login', 'Е-mail или Логин')}}
                        {{\Form::text('login', null, ["class"=>"form-control", "placeholder"=>"Введите email или логин", 'required' => 'required'])}}
                    </div>
                    <div class="form-group">
                        {{\Form::label('password', 'Пароль')}}
                        {{\Form::password('password', ["class"=>"form-control", "placeholder"=>"Введите пароль", 'required' => 'required'])}}
                    </div>
                    <div class="checkbox">
                        <label>
                            {{\Form::checkbox('remember', false)}} Запомнить меня
                        </label>
                    </div>
                    {{\Form::hidden('currentUrl', $settings->currentUrl)}}
                    <button type="button" class="btn btn-default btn-request">Войти</button>
                {{\Form::close()}}
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="activation" tabindex="-1" role="dialog" aria-labelledby="activationLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="activationLabel">Активация</h4>
            </div>
            <div class="modal-body">
                <p>Перейдите на страницу подписки для активации игры.</p>
                <a href="/subscriptions" class="activate">Подписаться</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="playerMessage" tabindex="-1" role="dialog" aria-labelledby="prizesLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="editLabel"></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="errorDoubleTabs" tabindex="-1" role="dialog" aria-labelledby="prizesLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editLabel">Ошибка!</h4>
            </div>
            <div class="modal-body">
                <p>Игра открыта в другой вкладке. Закройте одну из вкладок и перезагрузите страницу.</p>
            </div>
        </div>
    </div>
</div>

</body>
</html>