<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="@if($settings->isHome == true){{$settings->home_title}}@elseif($settings->isSubscriptions == true){{$settings->subscriptions_keywords}}@elseif($settings->isStatistics == true){{$settings->statistics_keywords}}@elseif($settings->isBlog == true){{$settings->blog_keywords}}@elseif($settings->isNews == true){{$settings->news_keywords}}@else{{$settings->keywords}}@endif">
        <meta name="description" content="@if($settings->isHome == true){{$settings->home_description}}@elseif($settings->isSubscriptions == true){{$settings->subscriptions_description}}@elseif($settings->isStatistics == true){{$settings->statistics_description}}@elseif($settings->isBlog == true){{$settings->blog_description}}@elseif($settings->isNews == true){{$settings->news_description}}@else{{$settings->description}}@endif">
        <meta name="title" content="@if($settings->isHome == true){{$settings->home_title}}@elseif($settings->isSubscriptions == true){{$settings->subscriptions_title}}@elseif($settings->isStatistics == true){{$settings->statistics_title}}@elseif($settings->isBlog == true){{$settings->blog_title}}@elseif($settings->isNews == true){{$settings->news_title}}@else{{$settings->page_title}}@endif {{$settings->title_patterm}}" />
    @if($settings->isGame)
        <link rel="image_src" href="{{$game->ogImage}}" />
    @endif
    <title>@if($settings->isHome == true){{$settings->home_title}}@elseif($settings->isSubscriptions == true){{$settings->subscriptions_title}}@elseif($settings->isStatistics == true){{$settings->statistics_title}}@elseif($settings->isBlog == true){{$settings->blog_title}}@elseif($settings->isNews == true){{$settings->news_title}}@else{{$settings->page_title}}@endif {{$settings->title_patterm}}</title>
    <link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon">
    <link href="/Front/css/bootstrap.min.css" rel="stylesheet">
    <link href="/Front/css/custom.css" rel="stylesheet">
    <link href="/Front/css/responsiveslides.css" rel="stylesheet">
    <link href="/Front/css/flipclock.css" rel="stylesheet">
    <link href="/Front/css/imagelightbox.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="<?!$settings->isHome ? print 'inner' : false ?>">


