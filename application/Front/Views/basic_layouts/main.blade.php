@include('Front::basic_layouts.head')
<div id="page-wrapper">
    <div class="container">
        @include('Front::basic_layouts.header')
        @include('Front::blocks.header-menu')
        @yield('content')
        <img src="/images/loading110.gif" alt="loading" class="loading">
        <div class="bottom-push"></div> <!-- заготовка под подвал -->
    </div>
</div>
@include('Front::basic_layouts.footer')
@yield('scripts')
<script>
    $(document).ready(function(){
        var firstOrNot = getCookie('first_time')
        if(firstOrNot == 1) {
            $('.handbook-adviser').delay(500).animate({
                bottom: 0 + 'px'
            },1000)
        }

        $('.handbook-adviser').mouseenter(function(){
            $('.handbook-adviser').stop(true,true).animate({
                bottom: 0 + 'px'
            },1000)
        }).mouseleave(function(){
            $('.handbook-adviser').stop(true,false).animate({
                bottom: -175 + 'px'
            },1000)
        })
    });

</script>
<div class="handbook-adviser hidden-xs">
    <a href="/handbook/o-chem-vasche-nasch-sajt">
        <div class="reference">Справка</div>
        <img src="/images/robot.gif" alt="robot" />
    </a>
</div>
@include('Front::basic_layouts.modals')