<header id="header">
    <section class="top row">
        <div class="col-xs-12 col-md-4">
            <a href="/" class="logo">
                <img src="/images/logo.png" alt="logo">
                <hr class="vertical-line" />
                <span>Казуальный<br/> киберспорт</span>
            </a>
        </div>
        @if (!\Auth::check())
            <div class="col-xs-12 col-md-2">
                <span class="call-number">{{$settings->phone_of_call_center}} </br> <i style="margin-top: -2px; display:block;">(звонок бесплатный)</i></span>
            </div>
            <div class="col-xs-12 col-md-2">
                <nav class="registration-menu">
                    <a href="#" data-toggle="modal" data-target="#sign-in">Вход</a> /
                    <a href="#" data-toggle="modal" data-target="#sign-up">Регистрация</a>
                </nav>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="menu-title">Вход через:</div>
                <nav class="social-enter-menu">
                    <div id="uLoginff6e3098" data-ulogin="display=panel;verify=1;sort=default;providers=vkontakte,odnoklassniki,steam,facebook,twitter,mailru,googleplus;redirect_uri=http%3A%2F%2Fgamezina.ru%2Fuser%2Fulogin;callback_hidden=review"></div>
                </nav>
            </div>
        @else
            <div id="user-account" class="col-xs-12 col-md-8">
                <div class="col-xs-12 col-md-4">
                    <span class="call-number">{{$settings->phone_of_call_center}} </br> <i style="margin-top: -2px; display:block;">(звонок бесплатный)</i></span>
                </div>
                <div class="col-xs-12 col-md-4 account">
                    <div class="user-info">
                        <p><a href="/user/show"><span>{{\Auth::user()->nickname}}</span></a></p>
                        <p>Баллы: <span class="user-balance" id="user-balance" data-balance="{{\Auth::user()->balance}}"><i rv-text="user.userBalance">{{\Auth::user()->balance}}</i></span></p>
                        <p>@if(\Auth::user()->subscribed == 1) <i class="vip">vip</i> @else<a href="/subscriptions">Подписка</a>@endif</p>
                    </div>
                    <div class="userpic">
                        <a href="/user/show"><img src="{{\Auth::user()->photo}}" width="50" height="50" /></a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4 take-prizes">
                    <p class="clearfix"><a href="/checkout" class="get-money">Забрать выигрыш</a></p>
                    <p class="links"><a href="/user/show">Личный кабинет</a> / <a href="/user/logout">Выход</a></p>
                </div>
            </div>
        @endif
    </section>
    @if($settings->isHome == true)
        @include('Front::blocks.header-banners')
    @endif
</header>