@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->page_title}}</h1>
        <p>Поздравляем! Вы успешно зарегестировались! Осталось только активировать аккаунт. На ваш почтовый ящик отправлено письмо с инструкциями по активации</p>
    </section>
@stop

@section('scripts')@stop