@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <div class="user-profile clearfix">
            <h1 class="title">{{$settings->page_title}}</h1>
            <section class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
                <div class="user-info">
                    <img src="{{empty($user->photo_big) ? $user->photo : $user->photo_big}}" width="110" height="110">
                    <table>
                        <tr>
                            <td class="name">{{$user->first_name}} {{$user->last_name}}</td>
                            <td class="client_id"><span>ID:</span> {{$user->client_id}}</td>
                        </tr>
                        <tr>
                            <td class="bonuses">Баллы: <span class="coin">{{$user->balance}}</span></td>
                            @if(!$user->verified_email)
                                <td class="email">
                                    <a href="javascript:void(0)" style="color:#77e1ff; font-size: 12px" class="change-email-add">изменить</a>
                                    <i class="einfo">{{$user->email}} <span style='color:#ff3a3a'>(аккаунт не подтвержден)</span></i>
                                    <div class="change-form">
                                        {{ \Form::open(['action' => '\Front\UserController@postEmailChange', 'class' => 'form-validate', 'role' => 'form', 'files' => false])}}
                                        <div class="form-group">
                                            {{ \Form::email('email', null, ['class' => '', 'placeholder' => 'введите новый E-mail', 'required' => '', 'data-change' => '1'])}}
                                            <p class="error"></p>
                                        </div>
                                        {{ \Form::button('ОК', ['type' => 'submit']); }}
                                        {{ \Form::close()}}
                                    </div>
                                </td>
                            @else
                                <td class="email">{{$user->email}} <span style='color:#65b7cf'>(подтвержден)</span></td>
                            @endif
                        </tr>
                        <tr>
                            @if($user->subscribed)
                                <td class="vip"><span>VIP</span>-аккаунт <span class="expir">(действует до {{$user->subscribe_expiration->format('d.m.Y');}})</span></td>
                            @endif
                            <td class="level">Уровень: <span>{{$user->level}}</span></td>
                        </tr>
                    </table>
                    <a class="settings-link" href="/user/edit">Мои настройки</a>
                </div>
                <div class="user-part-info">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tournaments" aria-controls="Турниры" role="tab" data-toggle="tab">Турниры</a></li>
                        <li role="presentation"><a href="#auctions" aria-controls="Аукционы" role="tab" data-toggle="tab">Аукционы</a></li>
                        <li role="presentation"><a href="#pays" aria-controls="История платежей" role="tab" data-toggle="tab">История платежей</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="tournaments">
                            <p>Вы еще ни приняли участие ни в одном турнире.</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="auctions">
                            @include('Front::blocks.user.lots')
                            @if($user->lots_shown)
                                <a href="#">Посмотреть все</a>
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="pays">
                            @include('Front::blocks.user.pays')
                            @if($user->pays_shown)
                                <a href="#">Посмотреть все</a>
                            @endif
                        </div>
                    </div>

                </div>
            </section>
        </div>
    </section>
@stop

@section('scripts')@stop