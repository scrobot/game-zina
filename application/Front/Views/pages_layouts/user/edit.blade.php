@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <div class="user-profile user-edit clearfix">
            <h1 class="title">{{$settings->page_title}}</h1>
            <section class="col-xs-12 col-sm-12 col-md-9 col-md-offset-2">
                <div class="errors">
                    @foreach($errors->all(':message') as $error)
                        <p class="error" style="color: #ff0000; font-size: 16px">{{$error}}</p>
                    @endforeach
                </div>
                {{ \Form::open(['action' => '\Front\UserController@postUpdate', 'files' => true, 'class' => 'clearfix']) }}
                    <div class="form-group col-xs-12 col-sm-2 clearfix fileform">
                        <img src="{{is_null($user->photo_big) ? $user->photo : $user->photo_big}}" width="100" height="100" />
                        {{ \Form::file('avatar', ['id' => 'avatar', 'onchange' => 'getName(this.value);']) }}
                        <a href="#" class="selectbutton">Загрузить</a>
                        <input type="text" class="" id="fileInputName" readonly>
                    </div>
                    <div class="form-group col-xs-12 col-sm-4 clearfix">
                        <p>Имя Фамилия</p>
                        {{ \Form::text('first_name', $user->first_name, ['class' => '', 'placeholder' => 'Имя'])}}
                        {{ \Form::text('last_name', $user->last_name, ['class' => '', 'placeholder' => 'Фамилия'])}}
                    </div>
                    <div class="form-group col-xs-12 col-sm-4 clearfix">
                        <p>Изменить пароль</p>
                        {{ \Form::password('password', ['class' => '', 'placeholder' => 'Пароль'])}}
                        {{ \Form::password('password_confirmation', ['class' => '', 'placeholder' => 'Повторите пароль'])}}
                    </div>
                    <div class="form-group col-xs-12 col-sm-4 clearfix">
                        <p>Дата рождения</p>
                        {{ \Form::text('bdate', $user->bdate, ['class' => '', 'placeholder' => 'День.Месяц.Год'])}}
                    </div>
                    <div class="form-group col-xs-12 col-sm-4 clearfix">
                        {{ \Form::submit('Сохранить') }}
                    </div>
                {{ \Form::close() }}
            </section>
        </div>
    </section>
@stop

@section('scripts')
<script type="text/javascript">
    function getName (str){
        if (str.lastIndexOf('\\')){
            var i = str.lastIndexOf('\\')+1;
        }
        else{
            var i = str.lastIndexOf('/')+1;
        }
        var filename = str.slice(i);
        var uploaded = $("#fileInputName").attr('value', filename);
    }
</script>
@stop