@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <div class="user-profile user-edit clearfix">
            <h1 class="title">{{$settings->page_title}}</h1>
            <section class="col-xs-12">
                <div class="errors">
                    @foreach($errors->all(':message') as $error)
                        <p class="error" style="color: #ff0000; font-size: 16px">{{$error}}</p>
                    @endforeach
                </div>
                {{\Form::open(['action' => '\Front\UserController@postSocialRegister', 'role' => 'form', 'files' => false, 'class' => 'clearfix final-registration'])}}
                <div class="form-group">
                    {{ \Form::label('nickame', 'Логин', ['class' => '']); }}
                    {{ \Form::text('nickname', \Session::get('userTemp.nickname', null), ['class' => '', 'placeholder' => 'Придумайте логин', 'required' => 'required'])}}
                    </div>
                <div class="form-group">
                    {{ \Form::label('email', 'Е-mail', ['class' => '']); }}
                    {{ \Form::text('email', \Session::get('userTemp.email', null), ['class' => '', 'placeholder' => 'Электронная почта', 'required' => 'required'])}}
                </div>
                <div class="form-group">
                    {{ \Form::label('password', 'Пароль', ['class' => '']); }}
                    {{ \Form::password('password', ['class' => '', 'placeholder' => 'Введите пароль', 'required' => 'required'])}}
                </div>
                <div class="form-group">
                    {{ \Form::label('password_confirmation', 'Повторите пароль', ['class' => '']); }}
                    {{ \Form::password('password_confirmation', ['class' => '', 'placeholder' => 'Введите пароль повторно', 'required' => 'required'])}}
                </div>

                {{ \Form::button('Зарегестрироваться', ['type' => 'submit']) }}
                {{\Form::close()}}
            </section>
        </div>
    </section>
@stop

@section('scripts')@stop