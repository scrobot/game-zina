@extends('Front::basic_layouts.main')

@section('content')
    <section class="auction row" id="lot">
        @include('Front::blocks.breadcrumbs')
        <h1 id="lot-title" class="title" data-title="{{$settings->page_alias}}">{{$settings->page_title}}</h1>
        <a href="/free-games" class="free-trn-link">Набрать баллы</a>
        <div id="lot-view" class="col-xs-12">
            <div class="lot clearfix">
                <section>
                    <nav class="lots-navigation">
                        @if(isset($settings->lot_navigation['prev']))
                            <a class="prev" href="/auctions/lot/{{$settings->lot_navigation['prev']['alias']}}"><i class="fa fa-arrow-left"></i> Предыдущий лот <i class="ntitle">{{$settings->lot_navigation['prev']['title']}}</i> </a>
                        @endif
                        @if(isset($settings->lot_navigation['next']))
                            <a class="next" href="/auctions/lot/{{$settings->lot_navigation['next']['alias']}}"><i class="ntitle">{{$settings->lot_navigation['next']['title']}}</i> Следующий лот <i class="fa fa-arrow-right"></i></a>
                        @endif
                    </nav>
                    <div class="col-xs-12 col-sm-6 lot-gallery">
                        <div class="central-thumbnail"><a rv-href="data.lot.full_thumbnail" data-imagelightbox="g"><img rv-src="data.lot.crop_thumbnail"></a></div>
                        <ul class="additional-thumbnails clearfix">
                                <li rv-each-gallery="data.lot.gallery"><a rv-href="gallery" data-imagelightbox="g"><img rv-src="gallery"></a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <h2><span>#{data.lot.SKU}</span> {data.lot.title}</h2>
                        <div class="time-remain">Осталось: <span>{data.lot.bidding_end | date}</span> {data.lot.bidding_end | literal}</div>
                        <div class="bet">
                            @if(\Auth::check())
                                <i class="hidden" id="cid" data-clientid="{{\Auth::user()->client_id}}"></i>
                            @endif
                            <p rv-if="data.lot.ownerIsLook"><strong><span><a href="{{action('\Front\UserController@getShow')}}">Вы лидирующий участник аукциона.</a></span></strong></p>
                            <p rv-unless="data.lot.ownerIsLook">Текущий владелец: <strong><span>{data.lot.current_owner}</span></strong></p>
                            <div class="current-bet">Текущая ставка: <span>{data.lot.current_bet}</span> <i>баллов</i></div>
                            <div class="bets-info">Ставок: <span>{data.lot.betsCount}</span></div>
                            <div class="set-bet" rv-unless="data.lot.expiries">
                                {{ \Form::open(['action' => ['\Front\AuctionController@postSingleBet', $settings->page_alias], 'role' => 'form', 'files' => false])}}
                                <input rv-placeholder="data.lot.next_bet" rv-value="data.lot.next_bet" step="1" rv-min="data.lot.next_bet" rv-max="lot.first_bet " name="bet" type="number">
                                <input type="hidden" name="sku" rv-value="data.lot.SKU">
                                <input type="hidden" name="token_secret" value="{{\Auth::check() ? \Auth::user()->token_secret : 0}}">
                                @if(\Auth::check())
                                    <button type="button" rv-on-click="data.setBet" rv-id="data.lot.SKU">Сделать ставку</button>
                                @else
                                    <button type="button" data-toggle="modal" data-target="#sign-in">Сделать ставку</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="col-xs-12 tabs" role="tabpanel">

            <!-- Nav tabs -->
            <ul id="lot-tab" class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Описание</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Технические характеристики</a></li>
                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Информация о доставке</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="home">
                    {{$settings->lot_description}}
                </div>
                <div role="tabpanel" class="tab-pane fade" id="profile">
                    {{$settings->lot_characteristics}}
                </div>
                <div role="tabpanel" class="tab-pane fade" id="messages">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non mi efficitur, ultricies ipsum vitae, euismod leo. Nulla libero ante, vulputate ac nunc nec, condimentum molestie nibh. Integer auctor quam non consectetur aliquet. Integer viverra tincidunt lacus, volutpat ullamcorper est finibus eget. Suspendisse magna mi, aliquet nec ornare vitae, efficitur ac magna. Suspendisse lacinia, nunc sit amet malesuada eleifend, leo tellus blandit neque, in vulputate sapien dolor sit amet nisl. Ut a tellus ante. Aliquam cursus eget dui ac euismod. Praesent mollis tincidunt urna, vitae maximus purus finibus a. Suspendisse accumsan diam enim. Nullam sed ipsum arcu. Duis lobortis ornare luctus. Quisque nibh lacus, vehicula sit amet urna quis, consequat tristique mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                    <p>Vivamus venenatis massa eu neque eleifend consectetur. Suspendisse felis metus, hendrerit at nisi sed, molestie imperdiet lectus. Donec varius felis in nulla condimentum ullamcorper. Fusce vel aliquam purus, accumsan interdum elit. Mauris sagittis dictum dui, nec maximus dui dignissim vitae. Sed eget venenatis dolor. In iaculis ac velit vel molestie. Integer placerat nulla non placerat condimentum. Suspendisse ipsum ante, dapibus id enim eget, maximus elementum tellus.</p>
                    <p>Maecenas eu risus laoreet, cursus sapien eget, volutpat libero. Mauris rutrum, ipsum sit amet tristique mollis, leo dui fringilla leo, ullamcorper interdum diam mi lacinia risus. Praesent laoreet ligula vitae metus blandit pretium. Mauris eu pellentesque ex. Nulla aliquet aliquam nisl, sed sollicitudin erat commodo vel. Cras hendrerit mauris sit amet lorem posuere, non laoreet metus sodales. Fusce nec finibus ipsum. Suspendisse potenti.</p>
                    <p>Praesent condimentum nunc ac pellentesque venenatis. Aliquam consectetur risus in sem pretium tincidunt. Donec nec augue justo. Praesent sit amet leo dui. Nam cursus non tortor ultrices finibus. Morbi at ligula et leo aliquam facilisis quis ut nibh. Praesent lobortis quis arcu at pellentesque. In hac habitasse platea dictumst. Praesent mattis imperdiet neque, quis eleifend est sodales id. Sed eget lorem consequat, finibus lectus non, fringilla nisl. In eget dictum elit. Aenean ac justo luctus nisi consectetur blandit eu vel tellus.</p>
                    <p>Suspendisse rutrum elit ligula, ac condimentum dui tristique vel. In sollicitudin elit eu lorem hendrerit fermentum. Cras quis quam ut sem ornare viverra. Pellentesque non commodo quam, at fermentum ante. Aliquam varius varius purus, et sagittis mauris sollicitudin in. Etiam cursus rhoncus nisl, quis congue turpis eleifend quis. In tincidunt urna a diam eleifend, et posuere nulla vulputate. Sed ante est, suscipit vel vulputate ac, venenatis et velit. Pellentesque tristique est vel risus maximus, a hendrerit tortor efficitur. Aenean tristique condimentum vestibulum. Nam pretium risus sollicitudin augue ornare fermentum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non mi efficitur, ultricies ipsum vitae, euismod leo. Nulla libero ante, vulputate ac nunc nec, condimentum molestie nibh. Integer auctor quam non consectetur aliquet. Integer viverra tincidunt lacus, volutpat ullamcorper est finibus eget. Suspendisse magna mi, aliquet nec ornare vitae, efficitur ac magna. Suspendisse lacinia, nunc sit amet malesuada eleifend, leo tellus blandit neque, in vulputate sapien dolor sit amet nisl. Ut a tellus ante. Aliquam cursus eget dui ac euismod. Praesent mollis tincidunt urna, vitae maximus purus finibus a. Suspendisse accumsan diam enim. Nullam sed ipsum arcu. Duis lobortis ornare luctus. Quisque nibh lacus, vehicula sit amet urna quis, consequat tristique mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                    <p>Vivamus venenatis massa eu neque eleifend consectetur. Suspendisse felis metus, hendrerit at nisi sed, molestie imperdiet lectus. Donec varius felis in nulla condimentum ullamcorper. Fusce vel aliquam purus, accumsan interdum elit. Mauris sagittis dictum dui, nec maximus dui dignissim vitae. Sed eget venenatis dolor. In iaculis ac velit vel molestie. Integer placerat nulla non placerat condimentum. Suspendisse ipsum ante, dapibus id enim eget, maximus elementum tellus.</p>
                    <p>Maecenas eu risus laoreet, cursus sapien eget, volutpat libero. Mauris rutrum, ipsum sit amet tristique mollis, leo dui fringilla leo, ullamcorper interdum diam mi lacinia risus. Praesent laoreet ligula vitae metus blandit pretium. Mauris eu pellentesque ex. Nulla aliquet aliquam nisl, sed sollicitudin erat commodo vel. Cras hendrerit mauris sit amet lorem posuere, non laoreet metus sodales. Fusce nec finibus ipsum. Suspendisse potenti.</p>
                    <p>Praesent condimentum nunc ac pellentesque venenatis. Aliquam consectetur risus in sem pretium tincidunt. Donec nec augue justo. Praesent sit amet leo dui. Nam cursus non tortor ultrices finibus. Morbi at ligula et leo aliquam facilisis quis ut nibh. Praesent lobortis quis arcu at pellentesque. In hac habitasse platea dictumst. Praesent mattis imperdiet neque, quis eleifend est sodales id. Sed eget lorem consequat, finibus lectus non, fringilla nisl. In eget dictum elit. Aenean ac justo luctus nisi consectetur blandit eu vel tellus.</p>
                    <p>Suspendisse rutrum elit ligula, ac condimentum dui tristique vel. In sollicitudin elit eu lorem hendrerit fermentum. Cras quis quam ut sem ornare viverra. Pellentesque non commodo quam, at fermentum ante. Aliquam varius varius purus, et sagittis mauris sollicitudin in. Etiam cursus rhoncus nisl, quis congue turpis eleifend quis. In tincidunt urna a diam eleifend, et posuere nulla vulputate. Sed ante est, suscipit vel vulputate ac, venenatis et velit. Pellentesque tristique est vel risus maximus, a hendrerit tortor efficitur. Aenean tristique condimentum vestibulum. Nam pretium risus sollicitudin augue ornare fermentum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non mi efficitur, ultricies ipsum vitae, euismod leo. Nulla libero ante, vulputate ac nunc nec, condimentum molestie nibh. Integer auctor quam non consectetur aliquet. Integer viverra tincidunt lacus, volutpat ullamcorper est finibus eget. Suspendisse magna mi, aliquet nec ornare vitae, efficitur ac magna. Suspendisse lacinia, nunc sit amet malesuada eleifend, leo tellus blandit neque, in vulputate sapien dolor sit amet nisl. Ut a tellus ante. Aliquam cursus eget dui ac euismod. Praesent mollis tincidunt urna, vitae maximus purus finibus a. Suspendisse accumsan diam enim. Nullam sed ipsum arcu. Duis lobortis ornare luctus. Quisque nibh lacus, vehicula sit amet urna quis, consequat tristique mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
                    <p>Vivamus venenatis massa eu neque eleifend consectetur. Suspendisse felis metus, hendrerit at nisi sed, molestie imperdiet lectus. Donec varius felis in nulla condimentum ullamcorper. Fusce vel aliquam purus, accumsan interdum elit. Mauris sagittis dictum dui, nec maximus dui dignissim vitae. Sed eget venenatis dolor. In iaculis ac velit vel molestie. Integer placerat nulla non placerat condimentum. Suspendisse ipsum ante, dapibus id enim eget, maximus elementum tellus.</p>
                    <p>Maecenas eu risus laoreet, cursus sapien eget, volutpat libero. Mauris rutrum, ipsum sit amet tristique mollis, leo dui fringilla leo, ullamcorper interdum diam mi lacinia risus. Praesent laoreet ligula vitae metus blandit pretium. Mauris eu pellentesque ex. Nulla aliquet aliquam nisl, sed sollicitudin erat commodo vel. Cras hendrerit mauris sit amet lorem posuere, non laoreet metus sodales. Fusce nec finibus ipsum. Suspendisse potenti.</p>
                    <p>Praesent condimentum nunc ac pellentesque venenatis. Aliquam consectetur risus in sem pretium tincidunt. Donec nec augue justo. Praesent sit amet leo dui. Nam cursus non tortor ultrices finibus. Morbi at ligula et leo aliquam facilisis quis ut nibh. Praesent lobortis quis arcu at pellentesque. In hac habitasse platea dictumst. Praesent mattis imperdiet neque, quis eleifend est sodales id. Sed eget lorem consequat, finibus lectus non, fringilla nisl. In eget dictum elit. Aenean ac justo luctus nisi consectetur blandit eu vel tellus.</p>
                    <p>Suspendisse rutrum elit ligula, ac condimentum dui tristique vel. In sollicitudin elit eu lorem hendrerit fermentum. Cras quis quam ut sem ornare viverra. Pellentesque non commodo quam, at fermentum ante. Aliquam varius varius purus, et sagittis mauris sollicitudin in. Etiam cursus rhoncus nisl, quis congue turpis eleifend quis. In tincidunt urna a diam eleifend, et posuere nulla vulputate. Sed ante est, suscipit vel vulputate ac, venenatis et velit. Pellentesque tristique est vel risus maximus, a hendrerit tortor efficitur. Aenean tristique condimentum vestibulum. Nam pretium risus sollicitudin augue ornare fermentum.</p>
                </div>
            </div>

        </div>
    </section>
    <img src="/images/loading110.gif" alt="loading" class="loading">
@stop

@section('scripts')
    <script type="text/javascript" src="/Front/js/scripts/lightboxinit.js"></script>
    <script type="text/javascript" src="/Front/js/scripts/singleLotsBinder.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#lot-tab a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            })

            $('.lots-navigation a i.ntitle').each(function(){
                var reviewLength = 20;
                var html = $(this).text();

                if( html.length > reviewLength )
                {
                    html = html.substring(0, reviewLength) + "...";
                    $(this).text(html);
                }
            })
        });
    </script>
    <script type="text/javascript">

        if(window.EventSource !== undefined){

            var alias = $('#lot-title').data('title');
            var es = new EventSource(alias);
            var listener = function (event) {

                var arr = JSON.parse(event.data);
                data.lot = arr

            };
            es.addEventListener("open", listener);
            es.addEventListener("message", listener);
            es.addEventListener("error", listener);

        } else {
            // EventSource not supported,
            // apply ajax long poll fallback
            (function poll(){
                setTimeout(function(){
                    var alias = $('#lot-title').data('title');
                    $.ajax({ url: "/auctions/single-lot-binder/" + alias, success: function(response){

                        data.lot = response.lot

                        console.log('Ajax Long Polling Enable');

                        poll();

                    }, dataType: "json"});
                }, 30000);
            })();
        }
    </script>
@stop