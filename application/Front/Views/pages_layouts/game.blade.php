@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content game-page clearfix row">
        @include('Front::blocks.breadcrumbs')
        <div class="clearfix">
            <h1 class="title col-xs-12 col-sm-6 col-md-9" data-alias="{{$game->alias}}">{{$game->title}}</h1>
            <div class="socials col-xs-12 col-sm-6 col-md-3">
                <div data-mobile-view="false" data-share-size="30" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1371247" data-mode="share" data-background-color="#ffffff" data-hover-effect="rotate-cw" data-share-shape="rectangle" data-share-counter-size="12" data-icon-color="#000000" data-mobile-sn-ids="fb.vk.tw.wh.ok.gp." data-text-color="#000000" data-buttons-color="#ffffff" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="horizontal" data-following-enable="false" data-sn-ids="vk.ok.mr.tw.gp.fb." data-preview-mobile="false" data-selection-enable="false" data-exclude-show-more="true" data-share-style="0" data-counter-background-alpha="1.0" data-top-button="false" class="uptolike-buttons" ></div>
            </div>
        </div>
        <div class="clearfix game-section">
            <div class="col-xs-12 col-sm-12 col-md-9">
                <div class="replace-flash-banner hidden-sm hidden-md hidden-lg">
                    <p>Извините, ваше устройство не позволяет вам полноценно играть в наши игры. Пожалуйста, зайдите с десктопного устройства(ноутбук, нетбук, ПК).</p>
                </div>
                <div id="flashContent" class="hidden-xs">
                </div>
            </div>
            <aside class="col-xs-12 col-sm-12 col-md-3">
                <h2><strong><span>{{$game->isStarted ? "начало турнира" : "окончание турнира"}}</span> {{$game->isStarted ? $game->start->date : $game->end->date}} </strong> в <strong>20:00</strong> по мСК</h2>
                <div id="top10Results">
                    <h3>Статистика</h3>
                    <table class="table table-striped">
                        <thead>
                        <th>Место</th>
                        <th>Имя</th>
                        <th>Счет</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr id='current-user' class="current-user">
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <a href="#" class="all-statistic" data-toggle="modal" data-target="#tournament-table">Посмотреть всю статистику</a>
                <a href="/page/pravila" class="t-rules">Правила турнира</a>
                <div class="timer">
                    <i>{{$game->isStarted ? "старт" : "конец"}}<br/> через</i>
                    <div class="timer-wrap">
                        158:12:36
                    </div>
                </div>
            </aside>
        </div>

        <section id="prizes" class="prizes clearfix">
            <h4>Призовые места</h4>
            <div class="prizes-wrap clearfix">
            @foreach($game->prizes as $prize)
                <div class="col-xs-12 col-sm-3 col-md-2-4 {{$prize->place > 10 ? 'hidden-place' : ''}}">
                    <div class="place {{$prize->place == 1 ? 'first-place' : ''}} {{$prize->place == 2 ? 'second-place' : ''}} {{$prize->place == 3 ? 'third-place' : ''}}">{{$prize->place}}</div>
                    <div class="prize"><? $prize->prize / 1 != 0 ? print $prize->prize." RUB" : print $prize->prize ?></div>
                </div>
            @endforeach
            </div>
            <a href="#" class="more-prizes">Посмотреть все</a>
        </section>

        <section class="other-games row">

            <div class="col-xs-12">
                @foreach($gameOther as $otherGame)
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <a href="/tournament/{{$otherGame->alias}}"><img src="/uploads/poster/{{$otherGame->type}}/{{$otherGame->thumbnail_360x250}}" alt="{{$otherGame->title}}"></a>
                    <div class="caption">
                        <h4><a href="/tournament/{{$otherGame->alias}}">{{$otherGame->title}}</a></h4>
                        <a href="/tournament/{{$otherGame->alias}}#prizes" class="all-prizes">посмотреть все призовые места</a>
                    </div>
                </div>
                @endforeach
            </div>
        </section>
        @include('Front::blocks.comments')
    </section>
    @if($settings->isGame && \Auth::check() && $game->users->contains(\Auth::user()->id))
        <div class="modal fade" id="tournament-table" tabindex="-1" role="dialog" aria-labelledby="tournamentLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="tournamentLabel">{{$game->title}}</h4>
                    </div>
                    <div class="modal-body">
                        <h5>Статистика турнира "{{$game->title}}"</h5>
                        <div id="top200Results">
                            <table class="table table-striped" >
                                <thead>
                                <th>Место</th>
                                <th>Имя</th>
                                <th>Счет</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop

@section('scripts')
<script type="text/javascript">
    var clock = $('.timer-wrap').FlipClock({{$game->countdawn}}, {
        countdown: true,
        callbacks: {
            start: function() {
                $('#flashContent').flash({
                    src: '{{$game_path}}',
                    width: 748,
                    height: 540,
                    wmode: 'opaque',
                    allowFullScreen: true,
                    allowScriptAccess: "sameDomain",
                    menu: false,
                    flashvars: {
                        laravel_session: '{{\Session::getId()}}',
                        user_token: '{{$game->user_token}}'
                    }
                });
            },
            stop: function() {
                location.reload();
            }
        }
    });


    (function(w,doc) {
        if (!w.__utlWdgt ) {
            w.__utlWdgt = true;
            var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
            s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
            var h=d[g]('body')[0];
            h.appendChild(s);
        }})(window,document);

    var currentScore = 0;

    function errorDoubleTabsCall()
    {
        $('#errorDoubleTabs').modal({
            backdrop: 'static'
        })
    }


</script>
<script type="text/javascript" src="/Front/js/scripts/binder.js"></script>
@stop