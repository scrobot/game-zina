@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        <h1 class="title">Внимание, вы получили БАН!</h1>
        <p>Уважаемый пользователь, вы получили БАН, вероятно вы нарушили одно из <a href="/page/pravila">правил</a> нашего сервиса. Отправить претензию нам вы можете со страницы <a href="/page/kontakty">контакты</a>, заполнив форму обратной связи, указав в теме письма "претензия за получения бана"</p>
    </section>
@stop

@section('scripts')@stop