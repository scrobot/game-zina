@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->page_title}}</h1>
        <form class="clearfix" id="games-filter">
            <div class="form-group col-sm-6 col-sm-offset-3 col-xs-12">
                <label id="tournament-getter-label" for="tournament-getter" class="control-label col-xs-12 col-sm-4">Выберите игру</label>
                <select id="tournament-getter" class="form-control col-xs-12 col-sm-8">
                    <option value="all" selected>Все игры</option>
                    @foreach($games as $gameFilter)
                    <option value="{{$gameFilter->alias}}">{{$gameFilter->title}}</option>
                    @endforeach
                </select>
            </div>
        </form>

        <div class="tournament-hall clearfix">
            @foreach($games as $game)
                <div class="col-xs-12 col-md-12 {{$game->alias}}">
                <h3>{{$game->title}}({{$game->end}})</h3>
                <table class="table table-striped">
                    <thead>
                    <tr><th>Место</th>
                        <th>Ник игрока</th>
                        <th>Счет</th>
                        <th>Выигрыш</th>
                    </tr></thead>
                    <tbody>
                    @if($game->prizes)
                        @foreach($game->prizes as $prizePlace)
                            <tr>
                                <td>{{$prizePlace->place}}</td>
                                <td>{{$prizePlace->nickname}}</td>
                                <td>{{$prizePlace->game_count}}</td>
                                <td>{{$prizePlace->prize}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4">Не определены</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            @endforeach
        </div>

    </section>
@stop

@section('scripts')@stop