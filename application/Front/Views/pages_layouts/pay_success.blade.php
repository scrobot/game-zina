@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        <h1 class="title">Оплата проведена успешно!</h1>
        <h2><a href="/subscriptions">Вернутся к активации?</a></h2>
    </section>
@stop

@section('scripts')@stop