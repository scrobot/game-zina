@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$page->title}}</h1>
        {{$page->content}}
    </section>
@stop

@section('scripts')@stop