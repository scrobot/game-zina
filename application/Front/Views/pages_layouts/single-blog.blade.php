@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <div class="single-blog">
            <img src="/uploads/poster/blog/{{$post->thumbnail_400x300}}" class="post-thumbnail" alt="{{$post->title}}">
            <h1 class="title">{{$post->title}}</h1>
            <div class="content">
                {{$post->content}}
            </div>
        </div>
    </section>
    @include('Front::blocks.comments')

@stop

@section('scripts')@stop