<div class="modal-header">
    <button type="button" class="close fa fa-close" data-dismiss="modal" aria-label="Close"></button>
    <h4 class="modal-title" id="myModalLabel">Пополнение аккаунта на {{$pay->sum}} рублей?</h4>
</div>
<div class="modal-body clearfix">
    {{\Form::open(['url' => 'https://merchant.pay2pay.com/?page=init', 'role' => 'form'])}}
    <input type="hidden" name="xml" value="{{$pay->xml_encode}}"/>
    <input type="hidden" name="sign" value="{{$pay->sign_encode}}"/>
        <button type="submit">Продолжить</button>
    {{\Form::close()}}
</div>

