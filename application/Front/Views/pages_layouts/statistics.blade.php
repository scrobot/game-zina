@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->page_title}}</h1>
        <div class="col-xs-12 col-md-6">
            <h2>Супер Игры</h2>
            <table class="table table-striped">
                <thead>
                <tr><th>Место</th>
                    <th>Дата</th>
                    <th>Игра</th>
                    <th>Счет</th>
                    <th>Приз</th>
                </tr></thead>
                <tbody>
                @foreach($superGames as $superGame)
                   @if($superGame->isActive)
                        <tr>
                            <td>
                                @if($superGame->is_winner)
                                    {{$superGame->is_winner->place}}
                                @else
                                    {{$superGame->currentPlace}}
                                @endif
                            </td>
                            <td>{{date('d.m.Y', $superGame->start_date)}}</td>
                            <td>{{$superGame->title}}</td>
                            <td>{{$superGame->getUserCount($user->id)}}</td>
                            <td>
                                @if($superGame->is_winner)
                                    {{$superGame->is_winner->prize}}
                                @else
                                    {{"Нет приза"}}
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-md-6">
            <h2>Game Speed</h2>
            <table class="table table-striped">
                <thead>
                <tr><th>Место</th>
                    <th>Дата</th>
                    <th>Игра</th>
                    <th>Счет</th>
                    <th>Выигрыш</th>
                </tr></thead>
                <tbody>
                @foreach($speedGames as $speedGame)
                    @if($speedGame->isActive)
                        <tr>
                            <td>
                                @if($speedGame->is_winner)
                                    {{$speedGame->is_winner->place}}
                                @else
                                    {{$speedGame->currentPlace}}
                                @endif
                            </td>
                            <td>{{date('d.m.Y', $speedGame->start_date)}}</td>
                            <td>{{$speedGame->title}}</td>
                            <td>{{$speedGame->getUserCount($user->id)}}</td>
                            <td>
                                @if($speedGame->is_winner)
                                    {{$speedGame->is_winner->prize}}
                                @else
                                    {{"Нет приза"}}
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>

    </section>
@stop

@section('scripts')@stop