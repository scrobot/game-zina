@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content game-page free-game clearfix row">
        @include('Front::blocks.breadcrumbs')
        <div class="clearfix">
            <h1 class="title col-xs-12 col-sm-6" data-alias="{{$game->alias}}">{{$game->title}}</h1>
        </div>
        <div class="clearfix game-section">
            <div class="col-xs-12 col-sm-12 col-md-9">
                <div class="replace-flash-banner hidden-sm hidden-md hidden-lg">
                    <p>Извините, ваше устройство не позволяет вам полноценно играть в наши игры. Пожалуйста, зайдите с десктопного устройства(ноутбук, нетбук, ПК).</p>
                </div>
                <div id="flashContent" class="hidden-xs">
                </div>
            </div>
            <aside class="col-xs-12 col-sm-12 col-md-3">
                @if(count($gameOther))
                    <section class="other-games clearfix">
                        @foreach($gameOther as $otherGame)
                                <div class="col-xs-12 col-sm-4 col-md-12 item">
                                    <img src="/uploads/poster/free/{{$otherGame->thumbnail_360x250}}" alt="race">
                                    <div class="caption">
                                        <a href="/free-games/{{$otherGame->alias}}">
                                            <h4>{{$otherGame->title}}</h4>
                                            <i class="go-to-game">Играть</i>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                    </section>
                @endif
            </aside>
        </div>
        <div class="clearfix repost">
            <div class="col-xs-12 col-sm-12 col-md-9">
                <p>{{$game->repost_title}}</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div data-mobile-view="false" data-share-size="30" data-like-text-enable="false" data-background-alpha="0.0" data-pid="1371247" data-mode="share" data-background-color="#ffffff" data-hover-effect="rotate-cw" data-share-shape="rectangle" data-share-counter-size="12" data-icon-color="#000000" data-mobile-sn-ids="fb.vk.tw.wh.ok.gp." data-text-color="#000000" data-buttons-color="#ffffff" data-counter-background-color="#ffffff" data-share-counter-type="disable" data-orientation="horizontal" data-following-enable="false" data-sn-ids="vk.ok.mr.tw.gp.fb." data-preview-mobile="false" data-selection-enable="false" data-exclude-show-more="true" data-share-style="0" data-counter-background-alpha="1.0" data-top-button="false" class="uptolike-buttons" ></div>
            </div>
        </div>
        <section class="rules row">
            <div class="col-xs-12">
                {{$game->rules}}
                <p>Подробнее читайте в <a href="/page/pravila">правилах</a></p>
            </div>
        </section>
    </section>
@stop

@section('scripts')
<script type="text/javascript">
    $('#flashContent').flash({
        src: '{{$game_path}}',
        width: 748,
        height: 540,
        wmode: 'opaque',
        allowFullScreen: true,
        allowScriptAccess: "sameDomain",
        menu: false,
        flashvars : {
            uid : '{{$game->uid}}',
            gid : '{{$game->id}}',
            isActivated: '{{$game->isActivated}}',
            url: 'http://gz.scrobot-group.ru/',
            laravel_session: '{{\Session::getId()}}',
            user_token: '{{$game->user_token}}',
            coefficient: '{{$game->coefficient}}',
            count_stop_limit: '{{$game->count_stop_limit}}',
            isFree: '{{$game->isFree}}',
            @if($game->type == 'super')
            attempts: '{{$game->attempts}}',
            @endif
        }
    });


    (function(w,doc) {
        if (!w.__utlWdgt ) {
            w.__utlWdgt = true;
            var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
            s.src = ('https:' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
            var h=d[g]('body')[0];
            h.appendChild(s);
        }})(window,document);

    var currentScore = 0;

    function resultOutput()
    {

        var alias = $('.game-page .title').data('alias');
        var results = showGetResultFree()

        data.userBalance = results.balance

    }

    function activationMessage()
    {
        $('#activation').modal();
    }

    function playedMessage()
    {
        $('#playerMessage').find('.modal-header').find('h4').text('Пошел на хуй!')
        $('#playerMessage').modal();
    }

    function startedMessage(){
        $('#playerMessage').find('.modal-header').find('h4').html('Сорян!<br/> Игра пока не началась.<br/> Приходи {{date('d.m.Y', $game->hourX)}} в 20:00 по МСК');
        $('#playerMessage').modal();
    }

    function gameCall()
    {
        $('#sign-in').modal();
    }

    function errorDoubleTabsCall()
    {
        $('#errorDoubleTabs').modal();
    }


</script>
<script type="text/javascript" src="/Front/js/scripts/free-binder.js"></script>
@stop