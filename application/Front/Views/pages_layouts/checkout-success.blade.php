@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->page_title}}</h1>

        <p>Ваш выигрыш оформлен успешно! В скором времени наш оператор свяжется с вами для уточнения всех деталей.</p>

    </section>
@stop

@section('scripts')@stop