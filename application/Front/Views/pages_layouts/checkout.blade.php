@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->page_title}}</h1>

        @if(count($lots) == 0 && count($prizes) == 0)
            <h2 style="padding-top: 50px">Ваша корзина пуста.</h2>
        @endif

        @if(count($lots))
        <h2 class="title">Корзина аукциона</h2>
        <table class="table table-striped">
            <thead>
                <th>Изображение</th>
                <th>Артикул</th>
                <th>Лот</th>
                <th>Дата окончания торгов</th>
            </thead>
            <tbody>
            @foreach($lots as $lot)
                @unless($lot->ordered)
                    <tr>
                       <td><img src="/uploads/auction_images/{{$lot->crop_thumbnail}}" width="50" /></td>
                       <td>#{{$lot->SKU}}</td>
                       <td>{{$lot->title}}</td>
                       <td>{{$lot->expiration_date}}</td>
                    </tr>
                @endunless
            @endforeach
            </tbody>
        </table>
        @endif

        @if(count($prizes))
            <h2 class="title">Выдача призов за турнир</h2>
            <table class="table table-striped">
                <thead>
                <th>Игра</th>
                <th>Дата окончания турнира</th>
                <th>Место</th>
                <th>Приз</th>
                </thead>
                <tbody>
                @foreach($prizes as $prize)
                    @unless($prize->ordered)
                        <tr>
                            <td>{{$prize->game->title}}</td>
                            <td>{{$prize->game->end_date}}</td>
                            <td>{{$prize->place}}</td>
                            <td>{{$prize->prize}}</td>
                        </tr>
                    @endunless
                @endforeach
                </tbody>
            </table>
        @endif
        @unless(count($lots) == 0 && count($prizes) == 0)
        <div id="checkout">
            @include('Front::blocks.checkout.step1')
        </div>
        @endunless



    </section>
@stop

@section('scripts')
    <script type="text/javascript" src="/Front/js/libs/inputmask.js"></script>
    <script type="text/javascript" src="/Front/js/libs/jquery.inputmask.js"></script>
    <script type="text/javascript" src="/Front/js/libs/jquery.validate.min.js"></script>
    <script type="text/javascript">inputMaskInit()</script>
@stop