@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <div class="single-news">
            @unless($news->category == "video")
                <img src="/uploads/poster/news/{{$news->thumbnail_50x50}}" class="post-thumbnail" alt="{{$news->title}}">
            @endunless
            <h1 class="title">{{$news->title}}</h1>
            <div class="content">
                {{$news->content}}
            </div>
        </div>
    </section>
    @include('Front::blocks.comments')

@stop

@section('scripts')@stop