@extends('Front::basic_layouts.main')

@section('content')
<section class="tournaments row">
    @include('Front::blocks.games')
</section>
<section class="other-games soon row">
    <h2 class="title">Скоро</h2>
    <div class="col-xs-12">
        @foreach($soonGames as $soonGame)
        <div class="col-xs-12 col-sm-6 col-md-3">
            <img src="/uploads/poster/{{$soonGame->type}}/{{$soonGame->thumbnail_360x250}}" alt="{{$soonGame->alias}}">
            <div class="caption">
                <h4>{{$soonGame->title}}</h4>
            </div>
        </div>
        @endforeach
    </div>
</section>
<section class="news-and-blog row">
    <div class="col-xs-12 col-sm-5 col-md-3 news">
        <h5><a href="/news">Новости</a></h5>
        <div class="clearfix news-wrap">
            @foreach($news as $newsPost)
            <div class="col-xs-12 news-post" data-type="{{$newsPost->type}}">
                <h6 class="title"><a href="/news/{{$newsPost->alias}}"><img src="/uploads/poster/news/{{$newsPost->thumbnail_50x50}}" alt="{{$newsPost->alias}}"> {{$newsPost->title}}</a></h6>
                <div class="news-date">{{$newsPost->created->date}}</div>
                <div class="anons">{{$newsPost->anons}}</div>
                <a href="/news/{{$newsPost->alias}}">Подробнее</a>
            </div>
            @endforeach
        </div>
        <a href="/news" class="all-news">Архив новостей</a>
    </div>
    <div class="col-xs-12 col-sm-7 col-md-9 blog">
        <h5><a href="/blog">Блог</a></h5>
        <div class="clearfix">
            @foreach($blog as $blogPost)
            <div class="col-xs-12 col-md-4 blog-post">
                <a href="/blog/{{$blogPost->alias}}"><img src="/uploads/poster/blog/{{$blogPost->thumbnail_400x300}}" alt="{{$blogPost->alias}}"></a>
                <div class="caption">
                    <h6 class="title"><a href="/blog/{{$blogPost->alias}}">{{$blogPost->title}}</a></h6>
                    {{$blogPost->anons}}
                    <a href="/blog/{{$blogPost->alias}}">Подробнее</a>
                </div>
            </div>
            @endforeach
        </div>
        <a href="/blog" class="all-blog">Весь блог</a>
    </div>
</section>
@stop

@section('scripts')
    <script type="text/javascript">
        @if(isset($superGame))

            // Билдинг объекта таймера
            var clock = $('.timer-wrap').FlipClock({{$superGame->countdawn}}, {
            countdown: true,
            callbacks: {
                stop: function() {
                    location.reload();
                }
            }
            });

        @endif
    </script>
@stop