@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->news_title}}</h1>
        <nav class="sort">
            <a href="{{action('\Front\NewsController@getCategoryNews', "text")}}" class="{{\Request::is('news/category-news/text*') ? "active" : ""}}"><i class="fa fa-list fa-3"></i></a>
            <a href="{{action('\Front\NewsController@getCategoryNews', "video")}}" class="{{\Request::is('news/category-news/video*') ? "active" : ""}}"><i class="fa fa-video-camera fa-3"></i></a>
        </nav>
        <div class="news-archive news-wrap clearfix">
            @foreach($news as $newsPost)
                <div class="col-xs-12 news-post">
                    <h6 class="title"><a href="/news/{{$newsPost->alias}}"><img src="/uploads/poster/news/{{$newsPost->thumbnail_50x50}}" alt="{{$newsPost->alias}}"> {{$newsPost->title}}</a></h6>
                    <div class="news-date">{{$newsPost->created->date}}</div>
                    <div class="anons">{{$newsPost->anons}} <a href="/news/{{$newsPost->alias}}">Подробнее</a></div>
                </div>
            @endforeach
        </div>
        <div class="clearfix" style="text-align: center">
            {{$news->links()}}
        </div>
    </section>
@stop

@section('scripts')@stop