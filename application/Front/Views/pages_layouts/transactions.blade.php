@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->page_title}}</h1>
        <div class="col-xs-12">
            <table class="table table-striped">
                <thead>
                <th>Дата проведения платежа</th>
                <th>Сумма</th>
                <th>Статус</th>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->created}}</td>
                        <td>{{$order->sum}}</td>
                        <td>
                            {{$order->status}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@stop

@section('scripts')@stop