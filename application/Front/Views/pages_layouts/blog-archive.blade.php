@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->blog_title}}</h1>
        <div class="blog-archive">
            @foreach($blog as $blogPost)
            <div class="col-xs-12 col-md-4 blog-post">
                <a href="/blog/{{$blogPost->alias}}"><img src="/uploads/poster/blog/{{$blogPost->thumbnail_400x300}}" alt="{{$blogPost->alias}}"></a>
                <div class="caption">
                    <h6 class="title"><a href="/blog/{{$blogPost->alias}}">{{$blogPost->title}}</a></h6>
                    {{$blogPost->anons}}
                    <a href="/blog/{{$blogPost->alias}}">Подробнее</a>
                </div>
            </div>
            @endforeach
            {{$blog->links()}}
        </div>
    </section>
@stop

@section('scripts')@stop