@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->page_title}}</h1>
        @if($message)
            <p style="text-align: center; font-size: 16px; color: lightgreen">{{ $message }}</p>
        @else
            <p style="text-align: center; font-size: 16px; color: lightgreen">{{ "Регистрация почти завершена. Социальные сети предоставили нам ваш e-mail. Вам необходимо подтвердить его, перейдя по ссылке в письме." }}</p>
        @endif

        @if ($redirect)
            <script type="application/javascript">
                setTimeout(
                        function() {
                            location.href = '{{ $redirect }}';
                        },
                        3000
                );
                var i = 4;
                setInterval(function(){
                    document.getElementById('time').innerText = "" + i;
                    i -= 1;
                },1000);
            </script>
            <p style="text-align: center; font-size: 18px; font-weight: bolder">Вы автоматически вернетесь на предыдущую страницу через <span id="time"></span></p>
            <p class="like-h" style="text-align: center">Нажмите <a href="{{ $redirect }}">эту ссылку</a>, если ваш браузер не поддерживает автоматический редирект.</p>
        @endif
    </section>
@stop

@section('scripts')@stop


