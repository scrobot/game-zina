@extends('Front::basic_layouts.main')

@section('content')
    <section class="auction row">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title">{{$settings->page_title}}</h1>
        <a href="/free-games" class="free-trn-link">Набрать баллы</a>
        @if(\Auth::check())
            <i class="hidden" id="cid" data-clientid="{{\Auth::user()->client_id}}"></i>
        @endif
        <div id="auction-ajax-view">
            <div class="grid-view clearfix">
                <div class="col-xs-12" id="lots">
                    <div class="lot col-xs-12 col-sm-4 col-md-3" rv-each-lot="data.lots">
                        <h3  rv-title="lot.title"><a rv-href="lot.alias"><span>Лот #{lot.SKU}</span><br/> <span class="td-undr">{lot.title | title}</span></a></h3>
                        <div class="thumbnail"><a rv-href="lot.alias"><img rv-src="lot.crop_thumbnail"></a></div>
                        <div class="until">осталось: <span>{lot.bidding_end | date}</span> {lot.bidding_end | literal}</div>
                        <div class="current-bet" rv-unless="lot.no_bets">
                            <span>{lot.current_bet} <i>баллов</i></span><br/>
                            <a href="{{action('\Front\UserController@getShow')}}" rv-if="lot.ownerIsLook">{lot.current_owner}</a>
                            <span rv-unless="lot.ownerIsLook" class="owner-name">{lot.current_owner}</span>
                        </div>
                        <div class="current-bet" rv-if="lot.no_bets"><span>{lot.first_bet} <i>баллов</i></span><br/>Начальная ставка</div>
                        <div class="set-bet" rv-unless="lot.expiries">
                            {{ \Form::open(['action' => '\Front\AuctionController@postBet', 'role' => 'form', 'files' => false])}}
                            <input class="col-xs-12" rv-placeholder="lot.next_bet" rv-value="lot.next_bet" step="1" rv-min="lot.next_bet" rv-firstBet="lot.first_bet" name="bet" type="number">
                            <input type="hidden" name="sku" rv-value="lot.SKU">
                            <input type="hidden" name="token_secret" value="{{\Auth::check() ? \Auth::user()->token_secret : 0}}">
                            @if(\Auth::check())
                                <button class="col-xs-12" type="button" rv-on-click="data.setBet" rv-id="lot.SKU">Сделать ставку</button>
                            @else
                                <button class="col-xs-12" type="button" data-toggle="modal" data-target="#sign-in">Сделать ставку</button>
                            @endif
                            {{ \Form::close()}}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" id="expiredLots">
                    <div rv-if="data.expiredLots.hasExpired">
                        <h2 style="margin-top: 25px;">Завершённые аукционы</h2>
                        <div class="clearfix">
                            <div class="lot col-xs-12 col-sm-4 col-md-3" rv-each-elot="data.expiredLots.lotsData">
                                <h3  rv-title="elot.title"><a href="javascript:void(0)"><span>Лот #{elot.SKU}</span><br/> <span class="td-undr">{elot.title | title}</span></a></h3>
                                <div class="thumbnail"><a href="javascript:void(0)"><img rv-src="elot.crop_thumbnail"></a></div>
                                <div class="until">{elot.expiration_date | formatDate}</div>
                                <div class="current-bet"><span>{elot.winner}</span><br/>{elot.best_bet}</div>
                            </div>
                        </div>
                        <button rv-if="data.expiredLots.moreExists" class="list-more" rv-on-click="data.loadMore" rv-dataLength="data.expiredLots.arrayLenght">Посмотреть все</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script type="text/javascript" src="/Front/js/libs/jquery.formatDateTime.js"></script>
    <script type="text/javascript" src="/Front/js/scripts/lotsBinder.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.loading').fadeIn();
            $('#auction-ajax-view').delay(1500).fadeIn('fast').closest('body').find('.loading').fadeOut('fast');
        });
    </script>
    <script type="text/javascript">

        if(window.EventSource !== undefined){

            var es = new EventSource("/auctions/event-listener");
            var listener = function (event) {

                var arr = JSON.parse(event.data);
                var length = $('.list-more').data('length')

                data.lots = response.lots
                data.expiredLots.lotsData = response.expired_lots.slice(0,length)

                if(data.expiredLots.lotsData.length > 0) {
                    data.expiredLots.hasExpired = true
                }
                
                console.log(data.expiredLots.hasExpired);

            };
            es.addEventListener("open", listener);
            es.addEventListener("message", listener);
            es.addEventListener("error", listener);

        } else {
            // EventSource not supported,
            // apply ajax long poll fallback
            (function poll(){
                setTimeout(function(){
                    $.ajax({ url: "/auctions/lots-binder", success: function(response){

                        var length = $('.list-more').data('length')

                        data.lots = response.lots
                        data.expiredLots.lotsData = response.expired_lots.slice(0,length)

                        if(response.expired_lots.length > 0) {
                            data.expiredLots.hasExpired = true
                        }

                        console.log('Ajax Long Polling Enable');

                        poll();

                    }, dataType: "json"});
                }, 30000);
            })();
        }
    </script>
@stop