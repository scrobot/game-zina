@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        <h1 class="title">Ошибка!</h1>
        <p>Во время проведения оплаты произошла ошибка.</p>
        <h2><a href="subscriptions">Вернутся к активации?</a></h2>
    </section>
@stop

@section('scripts')@stop