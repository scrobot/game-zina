@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content subscription clearfix row">
        @include('Front::blocks.breadcrumbs')
        <h1 class="title col-xs-12 col-sm-7">{{$settings->page_title}}</h1>
        <div class="col-xs-12 proposal">
            <h2 class="title">подписка на игровую неделю</h2>
            <div class="proposal-wrapper">
                @if(\Auth::check())
                    @if(\Auth::user()->verified_email == 0)
                        <p class="attention">Вы еще не активировали свой аккаунт. Пожалуйста, пройдите процесс активации.</p>
                    @elseif(\Auth::user()->subscribed == 0)
                        <p>оформив подписку на игровую неделю вы получаете возможность принимать участие в игровых турнирах</p>
                        {{\Form::open(['action' => '\Front\PayController@postIndex', 'role' => 'form', 'class' => 'replenishment col-xs-12 col-sm-5'])}}
                        {{\Form::hidden('user_id', \Auth::user()->id)}}
                        {{\Form::hidden('status_id', 1)}}
                        <button type="submit" class="pay-submit col-xs-6">Оформить</button>
                        {{\Form::close()}}
                        <div class="cost">Стоимость подписки: <span>200 RUB</span></div>
                    @else
                        <p class="attention">Вы уже приобрели подписку! Срок действия подписки - {{date('d.m.Y до H:i по МСК', $week_end)}}.</p>
                    @endif
                @else
                    <p class="attention">Вы не авторизованны! Вам необходимо пройти аутентификацию.</p>
                    <button type="submit" class="pay-submit col-xs-6" data-toggle="modal" data-target="#sign-in">Войти</button>
                @endif

            </div>
        </div>
    </section>
@stop

@section('scripts')@stop