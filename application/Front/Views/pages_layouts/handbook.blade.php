@extends('Front::basic_layouts.main')

@section('content')
    <section class="page-content clearfix">
        @include('Front::blocks.breadcrumbs')
        <div class="row">
            <aside class="col-xs-12 col-sm-4 col-md-3 handbook-menu">
                @include('Front::blocks.handbook-menu')
            </aside>
            <div class="col-xs-12 col-sm-8 col-md-9">
                <h1 class="title">{{$faq->title}}</h1>
                {{$faq->content}}
                @if(count($faq->children))
                    @foreach($faq->children as $child)
                        <section id="{{$child->alias}}">
                            <h2 class="title">{{$child->title}}</h2>
                            {{$child->content}}
                        </section>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@stop

@section('scripts')@stop