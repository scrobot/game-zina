<div class="modal-header">
    <button type="button" class="close fa fa-close" data-dismiss="modal" aria-label="Close"></button>
    <h4 class="modal-title" id="myModalLabel">Восстановление</br> пароля</h4>
</div>
<div class="modal-body clearfix">
    <div id="reminder">
        <div class="alert alert-success" style="display: none;"></div>
        <div class="alert alert-danger" style="display: none;"></div>

        {{ \Form::open(array('url' => action('\Front\RemindersController@postRemind'), 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
        <div class="form-group">
            <label for="email">Ваш E-Mail</label>
            {{ \Form::email('email', null, ['required' => 'required', 'class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ \Form::button('Восстановить', ['class' => 'btn-submit btn-reminder']) }}
        </div>
        {{ \Form::close() }}
    </div>
</div>

