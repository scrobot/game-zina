<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Сброс пароля на сайте gamezina</h2>

<div>
    Чтобы сбросить пароль, заполните форму: {{ URL::to('password/reset', array($token)) }}.<br/>
    Эти ссылка действительна {{ \Config::get('\auth.reminder.expire', 60) }} минут.
</div>
</body>
</html>