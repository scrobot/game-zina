<?php

namespace Mix;


trait ObjectTrait {

    public function DateInjection($object)
    {
        $object->start = new DateHelper($object->start_date);
        $object->end = new DateHelper($object->end_date);
        $object->now = date("Y-m-d H:i:s");

        $object->isStarted = $object->start_date > $object->end_date;
        $object->countdawn = $object->start_date > date("Y-m-d H:i:s") ? DateHelper::timestamp($object->start_date, date("Y-m-d H:i:s")) : DateHelper::timestamp($object->end_date, date("Y-m-d H:i:s"));

        return $object;
    }

    public function bindingLot($lot)
    {
        if(is_null($lot->getBestBet())) {
            $lot->no_bets = true;
            $lot->next_bet = $lot->first_bet + 1;
        } else {
            $lot->no_bets = false;
            $lot->next_bet = $lot->getBestBet() + 1;
            $lot->current_bet = $lot->getBestBet();
            $lot->current_owner = $lot->getOwner();
            $lot->ownerClientId = $lot->getOwnerClientId();
            $lot->ownerIsLook = false;
        }
        $lot->bidding_end = DateHelper::timestamp($lot->expiration_date, date('Y-m-d H:i:s'));
        if($lot->bidding_end <= 0) {
            $lot->expiries = true;
        }

        return $lot;
    }

}