<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 22.04.2015
 * Time: 11:24
 */

namespace Mix;

class ImageHelper {

    static public function imageCrop($path, $image, $extension , $width = 360, $height = 250)
    {

        $thumbnail = \Image::make($path."/".$image.".".$extension);
        $new_thumbnail = $image."_".$width."x".$height.".".$extension;
        $thumbnail->fit($width, $height, function ($constraint) {
            $constraint->upsize();
        })->save($path."/".$new_thumbnail);;

        return $new_thumbnail;
    }

}