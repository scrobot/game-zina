<?php namespace Mix;

class DateHelper {

    public $date = '';

    public function __construct($date)
    {
        $this->date = date_format(date_create($date), 'd.m.Y');
    }

    static public function timestamp($time1, $time2)
    {
        return strtotime($time1) - strtotime($time2);
    }

}