<?php

namespace Mix;

use Illuminate\Support\Facades\URL;
use Models\Faq;
use Models\Menu;
use Models\General;
use Models\Banner;
use Models\Game;
use Models\News;
use Models\Post;
use Models\User;
use Models\Prize;
use Models\Lot;
use Models\Comment;
use Mix\DateHelper;
use Mix\ImageHelper;
use Mix\UrlHelper;
use Models\FreeGame;

class BlockHelper {

    use ObjectTrait;

    public function settings()
    {
        $settings = General::find(1);
        $settings->isHome = false;
        $settings->isSubscriptions = false;
        $settings->isStatistics = false;
        $settings->isBlog = false;
        $settings->isNews = false;
        $settings->isGame = false;
        $settings->currentUrl = URL::current();
        $settings->breadcrumbs = UrlHelper::getPageType($settings->currentUrl);
        return $settings;
    }

    public function banners()
    {
        $banners = Banner::orderBy('weight', 'asc')->take(5)->get();
        return $banners;
    }

    public function menu()
    {
        $menu = (object) [];
        $menu->header = Menu::where('header',true)->orderBy('weight', 'asc')->get();
        $menu->footer = Menu::where('footer',true)->orderBy('weight', 'asc')->get();
        return $menu;
    }

    public function liveGames()
    {
        $games = Game::where('isActive', true)->take(5)->get();
        return $games;
    }

    /**
     * @return mixed
     * Метод возвращающий данные для вывода блока "Супер Игра" на главной странице
     */

    public function superGame()
    {
        $super = Game::where('type', 'super')->where('isActive', true)->first();

        $this->DateInjection($super);

        return $super;
    }

    public function speedGames()
    {
        $speed = Game::where('type', 'speed')->where('isActive', true)->take(4)->get();
        foreach($speed as $subject) {
            $subject->firstPrize = $subject->getFirstPrize();
        }

        return $speed;
    }

    public function getGame($alias)
    {
        $game = Game::where('alias', '=', $alias)->firstOrFail();
        $game->ogImage = "http://gamezina.ru/uploads/poster/".$game->type."/".$game->thumbnail;

        $this->DateInjection($game);

        return $game;
    }

    public function soonGames()
    {
        $soon = Game::where('isNext', true)->take(4)->get();
        return $soon;
    }

    public function getGamePrizes($gid)
    {
        $prizes = Prize::gloryHall($gid);
        return $prizes;
    }

    public function otherGame($alias, $uid = null)
    {
        $games = Game::where('alias', "!=", $alias)->where('isActive', true)->take(4)->get();
        foreach($games as $subject) {
            if(\Auth::check())
                $subject->isActived = $subject->users->contains($uid ? $uid :\Auth::user()->id);
            else
                $subject->isActived = 0;
        }
        return $games;
    }

    public function news()
    {
        $news = News::orderBy('created_at', 'desc')->take(3)->get();
        foreach($news as $subject) {
            $subject->created = new DateHelper($subject->created_at);
        }
        return $news;
    }

    public function newsTypeRequest($category)
    {
        $news = News::orderBy('created_at', 'desc')->where('category', $category)->paginate(5);

        foreach($news as $subject) {
            $subject->created = new DateHelper($subject->created_at);
        }
        return $news;
    }



    public function blog()
    {
        $blog = Post::orderBy('created_at', 'desc')->take(3)->get();
        foreach($blog as $subject) {
            $subject->created = new DateHelper($subject->created_at);
        }
        return $blog;
    }

    public function ourSocials()
    {
        $outSocials = [
            "vk" => $this->settings()->vk_adress,
            "ok" => $this->settings()->ok_adress,
            "fb" => $this->settings()->fb_adress,
            "tw" => $this->settings()->tw_adress,
        ];
        return $outSocials;
    }

    public function objectComments($object)
    {
        if($object->comments)
            $comments = $object->comments()->get();
        else
            $comments = false;
        return $comments;
    }

    public function lots()
    {
        $lots = Lot::where('status', 1)->get();

        foreach($lots as $lot) {
            $this->bindingLot($lot);
        }

        return $lots;
    }

    public function expiredLots()
    {
        $lots = Lot::where('status', 0)->get();

        foreach($lots as $lot) {
            $lot->winner = $lot->owner->nickname;
            $lot->best_bet = $lot->getBestBet();
        }

        return $lots;
    }

    public function singleLot($alias)
    {
        $lot = Lot::where('alias', $alias)->first();
        $this->bindingLot($lot);
        $gallery = [];
        $lot->gallery_1 != null ? $gallery[] = $lot->gallery_1 : $lot->gallery_1 = null;
        $lot->gallery_2 != null ? $gallery[] = $lot->gallery_2 : $lot->gallery_2 = null;
        $lot->gallery_3 != null ? $gallery[] = $lot->gallery_3 : $lot->gallery_3 = null;
        $lot->gallery = $gallery;
        $lot->betsCount = $lot->getBetsCount();

        $lot->navigation = $lot->getLotsNavigation();

        return $lot;
    }

    public function freeGames()
    {
        $games = FreeGame::all();
        return $games;
    }

    public function getFreeGame($alias, $user = null)
    {
        $game = FreeGame::where('alias', $alias)->first();
        $game->isFree = true;
        if(\Auth::check() && $user) {
            $game->isActivated = 2;
            $game->uid = $user->id;
            $game->user_token = $user->token_secret;
        } else {
            $game->isActivated = 0;
            $game->uid = 0;
            $game->user_token = null;
        }
        return $game;
    }

    public function otherFreeGames($alias)
    {
        $games = FreeGame::where('alias', '!=', $alias)->take(4)->get();
        return $games;
    }

    public function checkoutLots($user_id)
    {
        $user = User::find($user_id);
        $lots = $user->lotOwner;
        return $lots;
    }

    public function checkoutPrizes($user_id)
    {
        $user = User::find($user_id);
        $prizes = $user->prizes;
        return $prizes;
    }

    public function handbookMenu()
    {
        $faq = Faq::where('parent_id', null)->get();
        return $faq;
    }

}