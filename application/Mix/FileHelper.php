<?php namespace Mix;

class FileHelper {

   public static function fileNameMerge($type, $file, $ext)
   {
       switch($type) {
           case 'img' : $filename = $file.".".$ext; break;
           case 'demo' : $filename = $file."_demo.".$ext; break;
           case 'full' : $filename = $file.".".$ext; break;
       }

       return $filename;
   }

}