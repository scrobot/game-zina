<?php

namespace Mix;


class UserScore
{

    public $user_nickname;
    public $user_score = [];

    public function __construct($user_nickname, $user_score)
    {

        $this->user_nickname = $user_nickname;
        $this->user_score[] = $user_score;

    }

    public function setResult($score)
    {
        $this->user_score[] = $score;
    }

    static public function writeResult($alias, $temp) {
        $f = fopen(__FRONTASSETS__."/".$alias.".json", 'w');
        fputs($f, json_encode($temp));
        fclose($f);
    }

    static public function setBonuses($user, $coefficient, $multiplier, $score, $bonus)
    {

        $a = [
            'balance' => $user->balance,
            'coff' => $coefficient,
            'mult' => $multiplier,
            'score' => $score,
            'newScore' => ((($score*$coefficient)*$multiplier) + $bonus)
        ];

        $a['newScore'] = (int) $a['newScore'];

        return $a['newScore'];

    }

}