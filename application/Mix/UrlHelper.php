<?php

namespace Mix;


class UrlHelper {

    static public function getPageType($url)
    {
        if (starts_with(\Request::root(), 'http://'))
        {
            $domain = substr (\Request::root(), 7); // $domain is now 'www.example.com'
            $url = substr($url, strlen($domain)+8);
        }
        $urlArr = explode('/', $url);

        return $urlArr;
    }

}