<?php namespace Mix;

class MixMethods
{

    static public function sex($sex_in = "Не указано")
    {

        switch($sex_in) {
            case 1: $sex_out = "Женский"; break;
            case 2: $sex_out = "Мужской"; break;
            default: $sex_out = $sex_in;
        }

        return $sex_out;

    }

    static public function gender($gender_in = "Не указано")
    {

        switch($gender_in) {
            case "female": $sex_out = "Женский"; break;
            case "male": $sex_out = "Мужской"; break;
            default: $sex_out = $gender_in;
        }

        return $sex_out;

    }

    static public function generatePassword($number = 8)
    {
        $arr = array('a','b','c','d','e','f',
            'g','h','i','j','k','l',
            'm','n','o','p','r','s',
            't','u','v','x','y','z',
            'A','B','C','D','E','F',
            'G','H','I','J','K','L',
            'M','N','O','P','R','S',
            'T','U','V','X','Y','Z',
            '1','2','3','4','5','6',
            '7','8','9','0','.',',',
            '(',')','[',']','!','?',
            '&','^','%','@','*','$',
            '<','>','/','|','+','-',
            '{','}','`','~');
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }

}