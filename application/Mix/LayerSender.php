<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 03.08.2015
 * Time: 15:38
 */

namespace Mix;

use GuzzleHttp;

class LayerSender {

    protected $api_key;
    protected $form;

    public $result;

    public function __construct($api_key, $form)
    {
        $this->api_key = $api_key;
        $this->form = $form;
    }

    public function send()
    {
        $client = new GuzzleHttp\Client();
        $u = $client->post("http://layer-api.gamezina.ru/api/layer-api/v1/post/layer-catch", ['form_params' => ['api_key' => $this->api_key, "form" => $this->form]]);
        return $u->getStatusCode();
    }

}