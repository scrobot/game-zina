<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 17.04.2015
 * Time: 17:21
 */

namespace Models;


class Banner extends \Eloquent {

    protected $table = 'banners';
    protected $guarded = [];
    public $timestamps = false;

}