<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 17.04.2015
 * Time: 17:21
 */

namespace Models;


class News extends \Eloquent {

    protected $table = 'news';
    protected $guarded = [];

    public function comments()
    {
        return $this->hasMany('\Models\Comment');
    }

}