<?php

namespace Models;

class General extends \Eloquent {

    protected $table = 'general_information';
    protected $guarded = array();
    public $timestamps = false;

}