<?php namespace Models;

class Game extends \Eloquent {

    protected $table = 'games';
    protected $guarded = [];

    public static $rules = array(
        'title'     => 'required|',
        'thumbnail'  => 'required|mimes:jpeg,jpg,png',
        'api_key' => 'required|unique:games',
        'api_secret_token' => 'required|unique:games'
    );

    public static $update_rules = array(
        'title'     => 'required|',
    );

    public static $messages = [
        'required' => 'Поле :attribute должно быть заполнено.',
        'unique:super_games' => 'в поле :attribute Алиас(Псевдоним), формирующий путь, должен быть уникальным. Придумайте другой алиас или измените этот.',
        'alpha_dash' => 'в поле :attribute Допустимы только латинские символы, дефис и знак подчеркивания',
        'thumbnail.mimes:jpeg,jpg,png' => 'Выбранный файл не соответствует доступному расширению. Допустими файлы только с расширением .jpeg, .jpg, .png',
        'game_file.mimes:swf' => 'Выбранный файл не соответствует доступному расширению. Допустими файлы только с расширением .swf',
        'date' => 'Поле :attribute введено не в формате даты'
    ];

    // Relations

    public function prizes()
    {
        return $this->hasMany('\Models\Prize');
    }

    public function users()
    {
        return $this->belongsToMany('\Models\User')->withPivot('game_count');
    }

    public function comments()
    {
        return $this->hasMany('\Models\Comment');
    }

    // Queries

    public function getFirstPrize()
    {
        $prize = \DB::table('prizes')
            ->where('game_id', $this->id)
            ->where('place', 1)
            ->first();

        return $prize->prize;
    }

    public function winners($count = 10)
    {
        switch($this->counter_type)
        {
            case "straight" : $sortType = 'desc';
                break;
            case "reverse" :  $sortType = 'asc';
                break;
            default : $sortType = 'desc';
        }

        return \DB::table('game_user')
                    ->join('users', 'game_user.user_id', '=', 'users.id')
                    ->where('game_id', $this->id)
                    ->orderBy('game_count', $sortType)
                    ->take($count)
                    ->get();
    }

    public function isWinner($uid)
    {
        return \DB::table('prizes')
            ->where('user_id', $uid)
            ->first();
    }

    public function getUserCount($uid)
    {
        return \DB::table('game_user')->where('user_id', $uid)->where('game_id', $this->id)->pluck('game_count');
    }

    public function getAttempts($uid)
    {
        return \DB::table('game_user')->where('user_id', $uid)->where('game_id', $this->id)->pluck('attempts');
    }

    public function superGameActivation($uid)
    {
        return \DB::table('game_user')
            ->where('game_id', $this->id)
            ->where('user_id', $uid)
            ->update(array('attempts' => $this->attempts_count));
    }

    public function setAttempts($uid)
    {
        $update_attempts = $this->getAttempts($uid)-1;

        return \DB::table('game_user')
            ->where('game_id', $this->id)
            ->where('user_id', $uid)
            ->update(array('attempts' => $update_attempts));
    }

    // Custom Helper Methods

    public function api()
    {
        $this->api_id = $this->id*128;
        $this->api_hash = md5($this->api_id.$this->api_key.$this->api_secret_token);
        $this->save();
    }
}