<?php

namespace Models;

class Pay extends \Eloquent {

    protected $table = 'pays';
    protected $guarded = array();

    public function user()
    {
        return $this->belongsTo('\Models\User');
    }

    public function status()
    {
        return $this->belongsTo('\Models\Status');
    }

}