<?php

namespace Models;

class Lot extends \Eloquent {

    protected $table = 'lots';
    protected $guarded = ['gallery', 'expiration'];

    public function user()
    {
        return $this->belongsToMany('Models\User')->withPivot('bet','bet_time');
    }

    public function owner()
    {
        return $this->belongsTo('Models\User', 'user_id');
    }

    static public function galleryValidation($arr)
    {
        $validation = false;

        if($arr != NULL) {
            foreach($arr as $a) {
                if(empty($a)) {
                    $validation = false;
                } else {
                    $validation = true;
                    break;
                }

            }
        }

        return $validation;
    }

    public function getBestBet()
    {
        return \DB::table('lot_user')->where('lot_id', $this->id)->max('bet');
    }

    public function getOwner()
    {
        $row = \DB::table('lot_user')->where('lot_id', $this->id)->where('bet', $this->getBestBet())->first();
        $user = \DB::table('users')->where('id', $row->user_id)->first();
        return $user->nickname;
    }

    public function getOwnerId()
    {
        $row = \DB::table('lot_user')->where('lot_id', $this->id)->where('bet', $this->getBestBet())->first();
        $user = \DB::table('users')->where('id', $row->user_id)->first();
        return $user->id;
    }

    public function getOwnerClientId()
    {
        $row = \DB::table('lot_user')->where('lot_id', $this->id)->where('bet', $this->getBestBet())->first();
        $user = \DB::table('users')->where('id', $row->user_id)->first();
        return $user->client_id;
    }

    public function getBetsCount()
    {
        return \DB::table('lot_user')->where('lot_id', $this->id)->count();
    }

    public function getLotsNavigation()
    {
        $a = [];
        $temp1 = \DB::table('lots')->where('id', '<', $this->id)->where('status', 1)->orderBy('id', 'desc')->first();
        if($temp1) {
            $a['prev']['title'] = $temp1->title;
            $a['prev']['alias'] = $temp1->alias;
        }
        $temp2 = \DB::table('lots')->where('id', '>', $this->id)->where('status', 1)->first();
        if($temp2) {
            $a['next']['title'] = $temp2->title;
            $a['next']['alias'] = $temp2->alias;
        }
        return $a;
    }

}