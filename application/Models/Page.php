<?php namespace Models;


class Page extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';
    protected $guarded = array();
    public $timestamps = false;

    public static $rules = array(
        // Поле email является обязательным, также это должен быть допустимый адрес
        // электронной почты и быть уникальным в таблице users
        'title'     => 'required',

        // Поле username является обязательным, содержать только латинские символы и цифры, и
        // также быть уникальным в таблице users
        'alias'  => 'required|alpha_dash|unique:pages',
    );

    public static $update_rules = array(
        // Поле email является обязательным, также это должен быть допустимый адрес
        // электронной почты и быть уникальным в таблице users
        'title'     => 'required',

        // Поле username является обязательным, содержать только латинские символы и цифры, и
        // также быть уникальным в таблице users
        'alias'  => 'required|alpha_dash',
    );

    public static $messages = [
        'required' => 'Поле :attribute должно быть заполнено.',
        'unique:pages' => 'в поле :attribute путь должен быть уникальным. Попробуйте изменить название или добавить еще одно ключевое слово',
        'alpha_dash' => 'в поле :attribute Допустимы только латинские символы, дефис и знак подчеркивания',
    ];
}
