<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 20.04.2015
 * Time: 14:18
 */

namespace Models;


class Post extends \Eloquent {

    protected $table = 'posts';
    protected $guarded = [];

    public function comments()
    {
        return $this->hasMany('\Models\Comment');
    }

}