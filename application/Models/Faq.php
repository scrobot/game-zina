<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 20.07.2015
 * Time: 16:02
 */

namespace Models;


class Faq extends \Eloquent {

    protected $table = 'faqs';

    protected $guarded = [];

    public $timestamps = false;

    public static $rules = array(
        // Поле email является обязательным, также это должен быть допустимый адрес
        // электронной почты и быть уникальным в таблице users
        'title'     => 'required',

        // Поле username является обязательным, содержать только латинские символы и цифры, и
        // также быть уникальным в таблице users
        'alias'  => 'required|alpha_dash|unique:faqs',
    );

    public static $update_rules = array(
        // Поле email является обязательным, также это должен быть допустимый адрес
        // электронной почты и быть уникальным в таблице users
        'title'     => 'required',

        // Поле username является обязательным, содержать только латинские символы и цифры, и
        // также быть уникальным в таблице users
        'alias'  => 'required|alpha_dash',
    );

    public static $messages = [
        'required' => 'Поле :attribute должно быть заполнено.',
        'unique:faqs' => 'в поле :attribute путь должен быть уникальным. Попробуйте изменить название или добавить еще одно ключевое слово',
        'alpha_dash' => 'в поле :attribute Допустимы только латинские символы, дефис и знак подчеркивания',
    ];

    public function parent()
    {
        return $this->belongsTo('\Models\Faq');
    }

    public function children()
    {
        return $this->HasMany('\Models\Faq', 'parent_id');
    }

    public function getParent($pid)
    {
        if(!is_null($pid))
            return \DB::table('faqs')->where('id', $pid)->first();
        else
            return null;
    }

}