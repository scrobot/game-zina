<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 20.04.2015
 * Time: 17:10
 */

namespace Models;


class Comment extends \Eloquent {

    protected $table = 'comments';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('\Models\User');
    }

    public function game()
    {
        return $this->belongsTo('\Models\Game');
    }

    public function post()
    {
        return $this->belongsTo('\Models\Post');
    }

    public function news()
    {
        return $this->belongsTo('\Models\News');
    }


}