<?php

namespace Models;

class Status extends \Eloquent {

    protected $table = 'statuses';
    protected $guarded = array();
    public $timestamps = false;

    public function pay()
    {
        return $this->hasOne('\Models\Pay');
    }

}