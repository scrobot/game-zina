<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 20.04.2015
 * Time: 15:18
 */

namespace Models;


class Menu extends \Eloquent {

    protected $table = 'menus';
    protected $guarded = ['id'];
    public $timestamps = false;

}