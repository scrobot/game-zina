<?php

namespace Models;


class FreeGame extends \Eloquent {

    protected $table = 'free_games';
    protected $guarded = [];

    public static $rules = array(
        'title'     => 'required|',
        'thumbnail'  => 'required|mimes:jpeg,jpg,png',
    );

    public static $update_rules = array(
        'title'     => 'required|',
    );

    public static $messages = [
        'required' => 'Поле :attribute должно быть заполнено.',
        'unique:super_games' => 'в поле :attribute Алиас(Псевдоним), формирующий путь, должен быть уникальным. Придумайте другой алиас или измените этот.',
        'alpha_dash' => 'в поле :attribute Допустимы только латинские символы, дефис и знак подчеркивания',
        'thumbnail.mimes:jpeg,jpg,png' => 'Выбранный файл не соответствует доступному расширению. Допустими файлы только с расширением .jpeg, .jpg, .png',
        'game_file.mimes:swf' => 'Выбранный файл не соответствует доступному расширению. Допустими файлы только с расширением .swf',
        'date' => 'Поле :attribute введено не в формате даты'
    ];

}