<?php namespace Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends \Eloquent implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $guarded = array('id', 'manual', 'agree', 'oauth_identity', 'password_confirmation', 'refresh_token');

    static public $success = 'Данные о вашем пользователе успешно обновлены';
    static public $somethingBroken = 'Данные не обновлены. Идите нахуй.';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    public static $validation = array(
        // Поле email является обязательным, также это должен быть допустимый адрес
        // электронной почты и быть уникальным в таблице users
        'email' => 'email|unique:users',

        // Поле username является обязательным, содержать только латинские символы и цифры, и
        // также быть уникальным в таблице users
        'nickname' => 'unique:users',

        'password' => 'min:6'

    );

    public static $rules = array(
        // Поле email является обязательным, также это должен быть допустимый адрес
        // электронной почты и быть уникальным в таблице users
        'email' => 'required|email|unique:users',

        // Поле username является обязательным, содержать только латинские символы и цифры, и
        // также быть уникальным в таблице users
        'nickname' => 'required|unique:users',

        // Поле password является обязательным, должно быть длиной не меньше 6 символов, а
        // также должно быть повторено (подтверждено) в поле password_confirmation
        'password' => 'required|min:6',

    );

    public static $update_rules = array(
        'password' => 'confirmed|min:6',
    );

    public static $update_email_rule = array(
        'email' => 'required|email|unique:users',
    );

    public static $messages = [
        'required' => 'Поле :attribute должно быть заполнено.',
        'email' => 'Введен не корректный e-mail адрес',
        'unique' => 'в поле :attribute Имя пользователя должно быть уникальное. Придумайте другое или измените это.',
        'alpha_dash' => 'в поле :attribute Допустимы только латинские символы, дефис и знак подчеркивания',
        'confirmed' => 'Пароли не совпадают',
        'integer' => 'в поле :attribute Требуется целое число',
        'min' => 'в поле :attribute Пароль должен быть не короче 6-ти символов',
    ];

    public function games()
    {
        return $this->belongsToMany('\Models\Game')->withPivot('game_count');
    }

    public function pays()
    {
        return $this->hasMany('\Models\Pay');
    }

    public function comments()
    {
        return $this->hasMany('\Models\Comment');
    }

    public function prizes()
    {
        return $this->hasMany('\Models\Prize');
    }

    public function lots()
    {
        return $this->belongsToMany('\Models\Lot')->withPivot('bet','bet_time');
    }

    public function lotOwner()
    {
        return $this->hasMany('Models\Lot');
    }

    public function getGameCount($gid)
    {
        return \DB::table('game_user')->where('game_id', $gid)->where('user_id', $this->id)->pluck('game_count');
    }

    public function scoreUpdate($gid, $score)
    {
        return \DB::table('game_user')->where('game_id', $gid)->where('user_id', $this->id)->update(array('game_count' => $score));
    }

    public function currentPlace($count, $gid)
    {
        return \DB::table('game_user')->where('game_id', $gid)->where('game_count', ">=", $count)->count();
    }

	public function register($password)
    {

        $this->activation_code = $this->generateCode();
        $this->token_secret = $this->generateCode(32);
        $this->save();

		$this->sendActivationMail();
		$this->sendRegistrationMail($password);

		return $this->id;
	}

	public function generateCode($chars = 16)
    {
		return \Str::random($chars); // По умолчанию длина случайной строки 16 символов
	}

	public function sendActivationMail()
    {
		$activationUrl = action(
			'\Front\UserController@getActivate',
			array(
				'userId' => $this->id,
				'activation_code'    => $this->activation_code,
			)
		);

		$that = $this;
		\Mail::send('Front::emails.activation',
			array('activationUrl' => $activationUrl),
			function ($message) use($that) {
				$message->from('info@game-zina.com', 'Game ZINA');
				$message->to($that->email)->subject('Активация акканута на game zina');
			}
		);
	}

    public function sendRegistrationMail($password)
    {
        $that = $this;
        \Mail::send('Front::emails.registration', [
            'nickname' => $this->nickname,
            'password' => $password,
        ],
            function ($message) use ($that){
                $message->from('info@game-zina.com', 'Game ZINA');
                $message->to($that->email)->subject('Информация о регистрации на game-zina.ru');
            }
        );
    }

	public function activate($activation_code)
    {
		// Если пользователь уже активирован, не будем делать никаких
		// проверок и вернем false
		if ($this->verified_email) {
			return false;
		}

		// Если коды не совпадают, то также ввернем false
		elseif ($activation_code != $this->activation_code) {
			return false;
		}

		// Обнулим код, изменим флаг verified_email и сохраним
		$this->activation_code = '';
		$this->verified_email = true;
		$this->save();

		return true;
	}

    public function lotSlice($start = 0, $ajax = false, $step = 3)
    {
        $lots = $this->lotOwner()->orderBy('created_at', 'desc')->get()->toArray();
        if($ajax) {
            $step = count($lots);
            $sliced_lots = array_slice($lots, $start, $step);
        } else {
            $sliced_lots = array_slice($lots, $start, $step);
        }
        return $sliced_lots;
    }

    public function paySlice($start = 0, $ajax = false, $step = 3)
    {
        $pays = $this->pays()->orderBy('created_at', 'desc')->get();
        foreach($pays as $pay) {
            $pay->status_name = $pay->status->status;
        }
        $pays = $pays->toArray();
        if($ajax) {
            $step = count($pays);
            $sliced_pays = array_slice($pays, $start, $step);
        } else {
            $sliced_pays = array_slice($pays, $start, $step);
        }
        return $sliced_pays;
    }

    public function generateClientID()
    {
        $cid = rand(1000, 100000000);

        $repeat_exists = \DB::table($this->table)->where('client_id', $cid)->first();

        if(is_null($repeat_exists)) {
            return $cid;
        } else {
            $this->generateClientID();
        }
    }


}
