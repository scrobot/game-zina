<?php namespace Models;

class Prize extends \Eloquent {

    protected $table = 'prizes';
    protected $guarded = [];
    public $timestamps = false;

    public function game()
    {
        return $this->belongsTo('\Models\Game');
    }

    public function user()
    {
        return $this->belongsTo('\Models\User');
    }

    static public function gloryHall($gid){
        return \DB::table('prizes')
            ->join('users', 'prizes.user_id', '=', 'users.id')
            ->where('game_id', $gid)
            ->orderBy('place', "asc")
            ->take(10)
            ->get();
    }

}