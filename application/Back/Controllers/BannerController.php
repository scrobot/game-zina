<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 17.04.2015
 * Time: 17:23
 */

namespace Back;

use Models\Banner;
use Mix\FileHelper;


class BannerController extends BaseController {

    public function getIndex()
    {
        $banners = Banner::all();
        return \View::make('Back::banners.list', compact("banners"));
    }

    public function getCreate()
    {
        $banner = Banner::all();
        $action['route'] = '\Back\BannerController@postStore';
        $action['parameter'] = null;
        $info['bread'] = "создать новый";
        $info['head'] = "Создание баннера";
        return \View::make('Back::banners.item', compact('banner', 'action', 'info'));

    }

    public function postStore()
    {
        $input = \Input::all();

        $images_path = public_path()."/uploads/banners";

        if(\Input::file('image')) {
            $thumbnail = \Input::file('image')->getClientOriginalName();
            $input['image'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('image')->move($images_path, $thumbnail);
        }

        Banner::create($input);

        return \Redirect::back()->with("success", "Баннер успешно добавлен!");

    }

    public function getEdit($id)
    {
        $banner = Banner::find($id);
        $action['route'] = '\Back\BannerController@postUpdate';
        $action['parameter'] = $id;
        $info['bread'] = "update";
        $info['head'] = "Обновление баннера";
        return \View::make('Back::banners.item', compact('banner', 'action', 'info'));

    }

    public function postUpdate($id)
    {
        $input = \Input::all();

        $images_path = public_path()."/uploads/banners";

        $banner = Banner::find($id);

        if(\Input::file('image')) {
            $thumbnail = \Input::file('image')->getClientOriginalName();
            $input['image'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('image')->move($images_path, $thumbnail);
        } else {
            $input['image'] = $banner->image;
        }

        $banner->update($input);

        return \Redirect::back()->with("success", "Баннер успешно обновлен!");

    }

    public function getDestroy($id)
    {
        Banner::destroy($id);
        return \Redirect::back();
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        foreach($input['uid'] as $id) {
            Banner::destroy($id);
        }

        return \Redirect::back();
    }

}