<?php namespace Back;

use \Models\Page;

class PageController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $pages = Page::all();
        return \View::make('Back::pages.list', compact("pages"));
    }

    /**
     * @return mixed
     */

    public function getCreate()
    {
        $pages = Page::all();
        $action['route'] = '\Back\PageController@postStore';
        $action['parameter'] = null;
        $info['bread'] = "Добавить новую страниц";
        $info['head'] = "Добавление новой страницы";
        return \View::make('Back::pages.add', compact('pages', 'action', 'info'));
    }

    /**
     * @return mixed
     */

    public function postStore()
    {

        $input = \Input::all();

        $validator = \Validator::make($input, Page::$rules, Page::$messages);

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        Page::create($input);

        return \Redirect::back()->with("success", "Новая страница успешно добавлена!");

    }

    public function getEdit($id)
    {

        $pages = Page::find($id);
        $action['route'] = '\Back\PageController@postUpdate';
        $action['parameter'] = $id;
        $info['bread'] = "update";
        $info['head'] = "Обновление данных страницы ".$pages->title;
        return \View::make('Back::pages.add', compact('pages', 'action', 'info'));

    }

    public function postUpdate($id)
    {
        $input = \Input::all();

        $validator = \Validator::make($input, Page::$update_rules, Page::$messages);

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $new_arr = array_diff($input, array(0, null));

        $page = Page::find($id);
        $page->update($new_arr);

        return \Redirect::back()->with("success", "Страница успешно обновлена!");

    }

    /**
     * @param $id
     * @return mixed
     */

    public function getDestroy($id)
    {
        Page::destroy($id);
        return \Redirect::back();
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        foreach($input['uid'] as $id) {
            Page::destroy($id);
        }

        return \Redirect::back();

    }

}
