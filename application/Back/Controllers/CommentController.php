<?php

namespace Back;

use Models\Comment;

class CommentController extends BaseController {

    public function getIndex()
    {
        $comments = Comment::all();
        return \View::make('Back::comments.list', compact('comments'));
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        foreach($input['uid'] as $id) {
            Comment::destroy($id);
        }

        return \Redirect::back();
    }

    public function getDestroy($id)
    {
        Comment::destroy($id);
        return \Redirect::back();
    }

    public function getDispublic($id)
    {
        $comment = Comment::find($id);
        $comment->status = 0;
        $comment->save();
        return \Redirect::back();
    }

    public function getPublic($id)
    {
        $comment = Comment::find($id);
        $comment->status = 1;
        $comment->save();
        return \Redirect::back();
    }


}