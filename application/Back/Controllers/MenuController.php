<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 20.04.2015
 * Time: 15:29
 */

namespace Back;
use Models\Menu;


class MenuController extends BaseController {

    public function getIndex()
    {
        $menus = Menu::all();
        return \View::make('Back::menu.list', compact('menus'));
    }

    public function postChange()
    {
        $input = \Input::all();

        $editArray = [];

        for($i = 0; $i < count($input['menuEdit']['id']); $i++) {
            $editArray[$i] = [
                'id' => $input['menuEdit']['id'][$i],
                'title' => $input['menuEdit']['title'][$i],
                'link' => $input['menuEdit']['link'][$i],
                'weight' => $input['menuEdit']['weight'][$i],
            ];
        }

        if (!empty($input['menuCreate']['title']) && !empty($input['menuCreate']['link']) && !empty($input['menuCreate']['weight'])) {
            $menu = new Menu;
            $menu->title = $input['menuCreate']['title'];
            $menu->link = $input['menuCreate']['link'];
            $menu->weight = $input['menuCreate']['weight'];
            $menu->header = !empty($input['menuCreate']['header']) ? 1 : 0;
            $menu->footer = !empty($input['menuCreate']['footer']) ? 1 : 0;
            $menu->save();
        }

        if (!empty($input['menuEdit']) && isset($input['menuEdit'])) {
            foreach ($editArray as $item) {
                $menuItem = Menu::find($item['id']);
                $menuItem->title = $item['title'];
                $menuItem->link = $item['link'];
                $menuItem->weight = $item['weight'];
                $menuItem->save();
            }
        }

        if(isset($input['uid']))
        {
            foreach($input['uid'] as $id) {
                Menu::destroy($id);
            }
        }

        foreach($input['menuEdit']['header'] as $h_key => $h_value) {
            $item = Menu::find($h_key);
            $item->header = $h_value;
            $item->save();
        }

        foreach($input['menuEdit']['footer'] as $f_key => $f_value) {
            $itemf = Menu::find($f_key);
            $itemf->footer = $f_value;
            $itemf->save();
        }

        return \Redirect::back();
    }

}