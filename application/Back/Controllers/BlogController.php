<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 20.04.2015
 * Time: 14:18
 */

namespace Back;

use Models\Post;
use Mix\ImageHelper;
use Mix\FileHelper;

class BlogController extends BaseController {

    public function getIndex()
    {
        $posts = Post::all();
        return \View::make('Back::blog.list', compact("posts"));
    }

    public function getCreate()
    {
        $post = Post::all();
        $action['route'] = '\Back\BlogController@postStore';
        $action['parameter'] = null;
        $info['bread'] = "добавить новую запись";
        $info['head'] = "Добавление записи в блог";
        return \View::make('Back::blog.item', compact('post', 'action', 'info'));

    }

    public function postStore()
    {
        $input = \Input::all();

        $images_path = public_path()."/uploads/poster/blog";

        if(\Input::file('thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('thumbnail')->move($images_path, $thumbnail);

            $thumbnail_400x300 = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext, 400, 300);
            $input['thumbnail_400x300'] = $thumbnail_400x300;
        }

        Post::create($input);

        return \Redirect::back()->with("success", "Запись успешно добавлена!");

    }

    public function getEdit($id)
    {
        $post = Post::find($id);
        $action['route'] = '\Back\BlogController@postUpdate';
        $action['parameter'] = $id;
        $info['bread'] = "update";
        $info['head'] = "Редактировать запись";
        return \View::make('Back::blog.item', compact('post', 'action', 'info'));

    }

    public function postUpdate($id)
    {
        $input = \Input::all();

        $images_path = public_path()."/uploads/poster/blog";

        if(\Input::file('thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('thumbnail')->move($images_path, $thumbnail);

            $thumbnail_400x300 = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext, 400, 300);
            $input['thumbnail_400x300'] = $thumbnail_400x300;
        }

        $post = Post::find($id);
        $post->update($input);

        return \Redirect::back()->with("success", "Информация данной записи успешно обновлена!");

    }

    public function getDestroy($id)
    {
        Post::destroy($id);
        return \Redirect::back();
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        foreach($input['uid'] as $id) {
            Post::destroy($id);
        }

        return \Redirect::back();
    }

}