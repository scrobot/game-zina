<?php

namespace Back;

use Models\Lot;
use Mix\FileHelper;
use Mix\ImageHelper;
use Mix\DateHelper;

class AuctionController extends BaseController {

    public function getIndex()
    {
        $lots = Lot::all();
        return \View::make('Back::auction.list', compact("lots"));
    }

    public function getCreate()
    {
        $lot = Lot::all();
        $expiration = [];
        $expiration['date'] = '';
        $expiration['time'] = '';
        $action['route'] = '\Back\AuctionController@postStore';
        $action['parameter'] = null;
        $info['bread'] = "добавить новый лот";
        $info['head'] = "Добавление нового лота на аукцион";
        return \View::make('Back::auction.item', compact('lot', 'expiration', 'action', 'info'));

    }

    public function postStore()
    {
        $input = \Input::all();
        $input['status'] = 1;
        $images_path = public_path()."/uploads/auction_images";
        $gallery_validation = Lot::galleryValidation($input['gallery']);

        if(!empty($input['expiration']['date']) && !empty($input['expiration']['time'])) {
            $input['expiration']['date'] = date('Y-m-d', strtotime($input['expiration']['date']));
            $input['expiration_date'] = $input['expiration']['date']." ".$input['expiration']['time'];
        }


        if(\Input::file('full_thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('full_thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['full_thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('full_thumbnail')->move($images_path, $thumbnail);

            $crop_thumbnail = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext, 270, 200);
            $input['crop_thumbnail'] = $crop_thumbnail;
        }


        if($gallery_validation) {

            foreach ($input['gallery'] as $key => $value) {
                if($value != NULL) {
                    $name = "gallery_".$key;
                    $thumb_name = $input['alias']."(".$name.")";
                    $thumb_ext = \Input::file('gallery')[$key]->getClientOriginalExtension();
                    $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
                    $input[$name] = $thumbnail;
                    $thumbnail_upload_success = \Input::file('gallery')[$key]->move($images_path, $thumbnail);
                }
            }

        }

        $lot = Lot::create($input);
        $lot->SKU = 1000 + $lot->id;
        $lot->save();

        return \Redirect::back()->with("success", "Запись успешно добавлена!");

    }

    public function getEdit($id)
    {
        $lot = Lot::find($id);
        $expiration = [];
        $expiration['date'] = date('m/d/Y', strtotime($lot->expiration_date));
        $expiration['time'] = date('H:i:s', strtotime($lot->expiration_date));
        $action['route'] = '\Back\AuctionController@postUpdate';
        $action['parameter'] = $id;
        $info['bread'] = "update";
        $info['head'] = "Редактировать запись";
        $info['type'] = 1;
        return \View::make('Back::auction.item', compact('lot', 'expiration', 'action', 'info'));

    }

    public function postUpdate($id)
    {
        $input = \Input::all();
        $images_path = public_path()."/uploads/auction_images";
        $gallery_validation = Lot::galleryValidation($input['gallery']);
        $lot = Lot::find($id);

        if(isset($input['status'])) {
            $input['status'] = 1;
        } else {
            $input['status'] = 0;
        }

        if(!empty($input['expiration']['date']) && !empty($input['expiration']['time'])) {
            $input['expiration']['date'] = date('Y-m-d', strtotime($input['expiration']['date']));
            $input['expiration_date'] = $input['expiration']['date']." ".$input['expiration']['time'];
        }

        if(\Input::file('full_thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('full_thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['full_thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('full_thumbnail')->move($images_path, $thumbnail);

            $crop_thumbnail = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext, 270, 200);
            $input['crop_thumbnail'] = $crop_thumbnail;
        } else {
            $input['full_thumbnail'] = $lot->full_thumbnail;
        }


        if($gallery_validation) {

            foreach ($input['gallery'] as $key => $value) {
                if($value != NULL) {
                    $name = "gallery_".$key;
                    $thumb_name = $input['alias']."(".$name.")";
                    $thumb_ext = \Input::file('gallery')[$key]->getClientOriginalExtension();
                    $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
                    $input[$name] = $thumbnail;
                    $thumbnail_upload_success = \Input::file('gallery')[$key]->move($images_path, $thumbnail);
                }
            }

        }

        $lot->update($input);

        return \Redirect::back()->with("success", "Запись успешно добавлена!");

    }

    public function getDestroy($id)
    {
        Lot::destroy($id);
        return \Redirect::back();
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        foreach($input['uid'] as $id) {
            Lot::destroy($id);
        }

        return \Redirect::back();
    }

}