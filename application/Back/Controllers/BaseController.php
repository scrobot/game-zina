<?php namespace Back;


class BaseController extends \BaseController
{
    public function __construct()
    {
        \View::addNamespace('Back', __DIR__.'/../Views');
        define('__ASSETS__',__DIR__."/../../Assets");
    }

}