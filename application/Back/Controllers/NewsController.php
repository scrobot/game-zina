<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 17.04.2015
 * Time: 17:23
 */

namespace Back;

use Models\News;
use Mix\FileHelper;
use Mix\ImageHelper;


class NewsController extends BaseController {

    public function getIndex()
    {
        $news = News::all();
        return \View::make('Back::news.list', compact("news"));
    }

    public function getCreate()
    {
        $news = News::all();
        $action['route'] = '\Back\NewsController@postStore';
        $action['parameter'] = null;
        $info['bread'] = "добавить новую";
        $info['head'] = "Добавление новости";
        return \View::make('Back::news.item', compact('news', 'action', 'info'));

    }

    public function postStore()
    {
        $input = \Input::all();

        $images_path = public_path()."/uploads/poster/news";

        if(\Input::file('thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('thumbnail')->move($images_path, $thumbnail);

            $thumbnail_50x50 = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext, 90, 90);
            $input['thumbnail_50x50'] = $thumbnail_50x50;
        }

        News::create($input);

        return \Redirect::back()->with("success", "Новость успешно добавлен!");

    }

    public function getEdit($id)
    {
        $news = News::find($id);
        $action['route'] = '\Back\NewsController@postUpdate';
        $action['parameter'] = $id;
        $info['bread'] = "update";
        $info['head'] = "Редактировать новость";
        return \View::make('Back::news.item', compact('news', 'action', 'info'));

    }

    public function postUpdate($id)
    {
        $input = \Input::all();

        $images_path = public_path()."/uploads/poster/news";

        $news = News::find($id);

        if(\Input::file('thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('thumbnail')->move($images_path, $thumbnail);

            $thumbnail_50x50 = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext, 90, 90);
            $input['thumbnail_50x50'] = $thumbnail_50x50;
        } else {
            $input['thumbnail_50x50'] = $news->thumbnail_50x50;
            $input['thumbnail'] = $news->thumbnail;
        }


        $news->update($input);

        return \Redirect::back()->with("success", "Информация данной новости успешно обновлена!");

    }

    public function getDestroy($id)
    {
        News::destroy($id);
        return \Redirect::back();
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        foreach($input['uid'] as $id) {
            News::destroy($id);
        }

        return \Redirect::back();
    }

}