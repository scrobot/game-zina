<?php namespace Back;

use \Models\User;

class UserController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$users = User::where('id', '>', 1)->paginate(100);

		return \View::make('Back::user.users', compact("users"));
	}

    /**
     * @return mixed
     */

    public function getCreate()
    {
        $user = User::all();
        $action['route'] = '\Back\UserController@postStore';
        $action['parameter'] = null;
        $info['bread'] = "Добавить нового пользователя";
        $info['head'] = "Добавление нового пользователя";
        return \View::make('Back::user.add', compact('user', 'action', 'info'));
    }

    /**
     * @return mixed
     */

    public function postStore()
    {

        $input = \Input::all();

        if(isset($input['verified_email'])) {
            $input['verified_email'] = 1;
        } else {
            $input['verified_email'] = 0;
        }

        if(isset($input['isAdmin'])) {
            $input['isAdmin'] = 1;
        } else {
            $input['isAdmin'] = 0;
        }

        $validator = \Validator::make($input, User::$rules, User::$messages);

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $user = User::create($input);

        $user->password = \Hash::make($input['password']);
        $user->save();

        return \Redirect::back()->with("success", "Новый пользователь успешно добавлен!");

    }

    public function getEdit($id)
    {

        $user = User::find($id);
        $singleUser = true;
        $action['route'] = '\Back\UserController@postUpdate';
        $action['parameter'] = $id;
        $info['bread'] = "update";
        $info['head'] = "Обновление данных пользователя ".$user->nickname;
        $user->verified_email = $user->verified_email == 1 ? true : false;
        $user->isAdmin = $user->isAdmin == 1 ? true : false;
        return \View::make('Back::user.add', compact('user', 'singleUser' , 'action', 'info'));

    }

    public function postUpdate($id)
    {
        $input = \Input::all();

        $validator = \Validator::make($input, User::$update_rules, User::$messages);

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $input['password'] = \Hash::make($input['password']);
        $new_arr = array_diff($input, array(0, null));

        if(isset($input['verified_email'])) {
            $new_arr['verified_email'] = 1;
        } else {
            $new_arr['verified_email'] = 0;
        }

        if(isset($input['isAdmin'])) {
            $new_arr['isAdmin'] = 1;
        } else {
            $new_arr['isAdmin'] = 0;
        }

        $user = User::find($id);
        $user->update($new_arr);

        return \Redirect::back()->with("success", "Информация о пользователе успешно обновлена!");

    }

    /**
     * @param $id
     * @return mixed
     */

    public function getDestroy($id)
    {
        User::destroy($id);
        return \Redirect::back();
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        if(isset($input['uid'])) {
            foreach($input['uid'] as $id) {
                User::destroy($id);
            }
        }

        if(isset($input['banned'])) {
            foreach ($input['banned'] as $key => $banned) {
                if($banned != 'on') {
                    $user = User::find($key);
                    $user->banned = 1;
                    $user->save();
                }
            }
        }

        return \Redirect::back();

    }

    public function getRemoveBan($id)
    {
        $user = User::find($id);
        $user->banned = 0;
        $user->save();

        return \Redirect::back();
    }

}
