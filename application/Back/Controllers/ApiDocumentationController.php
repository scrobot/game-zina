<?php
/**
 * Created by PhpStorm.
 * User: scrobot91
 * Date: 30.07.2015
 * Time: 12:25
 */

namespace Back;


class ApiDocumentationController extends BaseController {

    public function getIndex()
    {
        return \View::make('Back::api.doc');
    }

}