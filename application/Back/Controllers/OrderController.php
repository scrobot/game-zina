<?php
namespace Back;

use Models\Pay;

class OrderController extends BaseController {

    public function getIndex()
    {
        $orders = Pay::all();
        return \View::make('Back::orders.list', compact('orders'));
    }


}