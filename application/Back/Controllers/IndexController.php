<?php

namespace Back;

use Models\User;

class IndexController extends BaseController {

    public function getIn()
    {
        return \View::make('Back::auth.signIn');

    }

    public function postIn()
    {
        $userInfo = \Input::except(['_token','remember']);
        $user = User::where('nickname', $userInfo['nickname'])->first();
        $userInfo['isAdmin'] = $user->isAdmin;

        if (\Auth::attempt($userInfo, \Input::has('remember'))) {
            return \Redirect::to('/admin/admin-page');
        } else {
            return \Redirect::to('/403');
        }

    }

    public function getOut()
    {
        \Auth::logout();
        return \Redirect::to('/admin/admin-page');
    }

}