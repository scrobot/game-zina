<?php namespace Back;

use Mix\FileHelper;
use Mix\DateHelper;
use Mix\ImageHelper;
use Models\Game;
use Models\Prize;

class GameController extends BaseController
{

    public function getIndex()
    {
        $games = Game::all();
        return \View::make('Back::games.list', compact('games'));
    }

    public function getCreate()
    {
        $action['route'] = '\Back\GameController@postStore';
        $action['parameter'] = null;
        $info['bread'] = "добавить турнира";
        $info['head'] = "Создание турнира";
        return \View::make('Back::games.add', compact('info', 'action'));
    }

    public function postStore()
    {
        // get&&set
        $input = \Input::except('prize');

        $validator = \Validator::make($input, Game::$rules, Game::$messages);

        $validator->sometimes(array('game_file', 'start_date', 'end_date'), 'required', function ($input) {
            return isset($input->isActive) && $input->isActive == 1;
        });

        $validator->sometimes(array('alias'), 'required|alpha_dash|unique:games', function ($input) {
            return isset($input->isActive) && $input->isActive == 1;
        });

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $images_path = public_path()."/uploads/poster/".$input['type'];
        $games_path = public_path()."/games/";

        // Модификации принимаемых значений
        if(!empty($input['start_date']))
            $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_date']));
        if(!empty($input['end_date']))
            $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_date']));

        // Замуты с файлами
        // Превью
        if(\Input::file('thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('thumbnail')->move($images_path, $thumbnail);

            $thumbnail_360x250 = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext);
            $input['thumbnail_360x250'] = $thumbnail_360x250;
        }

        //Фул
        if(\Input::file('game_file')) {
            $full_name = $input['alias'];
            $full_ext = \Input::file('game_file')->getClientOriginalExtension();
            $game_file = FileHelper::fileNameMerge('full', $full_name, $full_ext);
            $input['game_file'] = $game_file;
            $full_upload_success = \Input::file('game_file')->move($games_path, $game_file);
        }

        $superGame = Game::create($input);
        $superGame->api();

        // Формирование призов и вставка в таблицу prizes
        $prizesArr = [];
        $i = 0;
        if(\Input::has('prize')) {
            foreach(\Input::get('prize') as $place => $sum) {
                $prizesArr[$i]['game_id'] = $superGame->id;
                $prizesArr[$i]['place'] = $place;
                $prizesArr[$i]['prize'] = $sum[0];
                $i++;
            }

            foreach($prizesArr as $game_prize) {
                $prize = new Prize();
                $prize->game_id = $game_prize['game_id'];
                $prize->place = $game_prize['place'];
                $prize->prize = $game_prize['prize'];
                $prize->save();
            }
        }

        return \Redirect::back()->with("success", "Новый турнир SUPER GAME успешно добавлен!");
    }

    public function getEdit($id)
    {
        $action['route'] = '\Back\GameController@postUpdate';
        $action['parameter'] = $id;
        $game = Game::find($id);
        $game->start_date = date('m/d/Y', strtotime($game->start_date));
        $game->end_date = date('m/d/Y', strtotime($game->end_date));
        $game->isActive = $game->isActive == 1 ? true : false;
        $game->isNext = $game->isNext == 1 ? true : false;
        $prizes = Prize::where('game_id', $id)->get();
        $info['bread'] = "турнир ".$game->title;
        $info['head'] = "Редактировать турнир ".$game->title;
        return \View::make('Back::games.edit', compact('info', 'game', 'prizes' , 'action'));
    }

    public function postUpdate($id)
    {
        // get&&set
        $input = \Input::except('prize');

        if(isset($input['isActive'])) {
            $input['isActive'] = 1;
        } else {
            $input['isActive'] = 0;
        }

        if(isset($input['isNext'])) {
            $input['isNext'] = 1;
        } else {
            $input['isNext'] = 0;
        }


        $validator = \Validator::make($input, Game::$update_rules, Game::$messages);

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $images_path = public_path()."/uploads/poster/".$input['type'];
        $games_path = public_path()."/games/";

        $game = Game::find($id);

        // Модификации принимаемых значений
        if(!empty($input['start_date']))
            $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_date']));
        if(!empty($input['end_date']))
            $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_date']));

        // Замуты с файлами
        // Превью
        if(\Input::file('thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('thumbnail')->move($images_path, $thumbnail);

            $thumbnail_360x250 = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext);
            $input['thumbnail_360x250'] = $thumbnail_360x250;
        } else {
            $input['thumbnail'] = $game->thumbnail;
        }

        //Фул
        if(\Input::file('game_file')) {
            $full_name = $input['alias'];
            $full_ext = \Input::file('game_file')->getClientOriginalExtension();
            $game_file = FileHelper::fileNameMerge('full', $full_name, $full_ext);
            $input['game_file'] = $game_file;
            $full_upload_success = \Input::file('game_file')->move($games_path, $game_file);
        } else {
            $input['game_file'] = $game->game_file;
        }

        $game->update($input);
        $game->api();

        // Формирование призов и вставка в таблицу prizes
        $prizesArr = [];
        $i = 0;
        if(\Input::has('prize')) {
            foreach(\Input::get('prize') as $place => $sum) {
                $prizesArr[$i]['game_id'] = $game->id;
                $prizesArr[$i]['place'] = $place;
                $prizesArr[$i]['prize'] = $sum[0];
                $i++;
            }

            foreach($prizesArr as $game_prize) {
                $prize = Prize::where('game_id', $id)->where('place', $game_prize['place'])->first();
                $prize->game_id = $game_prize['game_id'];
                $prize->place = $game_prize['place'];
                $prize->prize = $game_prize['prize'];
                $prize->save();
            }

        }

        return \Redirect::back()->with("success", "Новый турнир SUPER GAME успешно добавлен!");
    }

    public function getDestroy($id)
    {
        $game = Game::find($id);
        if(file_exists(__ASSETS__."/".$game->alias.".json"))
            unlink(__ASSETS__."/".$game->alias.".json");
        Game::destroy($id);
        return \Redirect::back();
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        foreach($input['id'] as $id) {
            $game = Game::find($id);
            if(file_exists(__ASSETS__."/".$game->alias.".json"))
                unlink(__ASSETS__."/".$game->alias.".json");
            Game::destroy($id);
        }

        return \Redirect::back();
    }

    public function getPrizes($id)
    {
        $prizes = Prize::where('game_id', $id)->get();

        return \View::make('Back::games.prizes', compact('prizes'));
    }

    public function postGenerateToken()
    {
        $length = \Input::get('length');
        return \Str::random($length);
    }
}