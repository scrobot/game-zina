<?php namespace Back;

use Illuminate\View;
use Models\General;
use Models\Prize;
use Models\User;
use Models\Game;
use Mix\DateHelper;


class AdminController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getAdminPage()
    {
        $activeGames = Game::where('isActive', true)->get();
        $nextGames = Game::where('isNext', true)->get();
        $sampleGame = Game::where('isActive', true)->where('type', 'super')->first();
        $timestamp = $sampleGame->start_date > date("Y-m-d H:i:s") ? DateHelper::timestamp($sampleGame->start_date, date("Y-m-d H:i:s")) : DateHelper::timestamp($sampleGame->end_date, date("Y-m-d H:i:s"));
        return \View::make('Back::dashboard', compact('activeGames', 'nextGames', 'timestamp'));
    }

    public function getSettings()
    {
        $general = General::find(1);
        $action['route'] = '\Back\AdminController@postStore';
        $action['parameter'] = null;
        $info['bread'] = "Редактировать основные настрокий";
        $info['head'] = "Общая информация";
        return \View::make('Back::settings.edit',  compact('general', 'action', 'info'));
    }

    public function postStore(){
        $input = \Input::all();
        $general = General::find(1);

        $general->update($input);

        return \Redirect::back()->with("success", "Основная инфа успешно обновлена!");
    }

    public function getEmailNewsletter()
    {
        $action['route'] = '\Back\AdminController@postEmailNewsletter';
        $action['parameter'] = null;
        $info['bread'] = "E-mail рассылка";
        $info['head'] = "E-mail рассылка всем зарегестрированным пользователям.";
        return \View::make('Back::newsLetter', compact('action', 'info'));

    }

    public function postEmailNewsletter()
    {

        $subject = \Input::get('theme');
        $content = \Input::get('content');
        $users = User::all();

        foreach ($users as $user) {

            $email = $user->email;

            \Mail::send('Back::emails.newsletter', [
                'sex' => $user->sex,
                'user' => $user->first_name,
                'text' => $content,
            ],
                function ($message) use ($email, $subject){
                    $message->from('info@game-zina.com', 'Game ZINA');
                    $message->to($email)->subject($subject);
                }
            );

        }


        return \Redirect::back()->with("success", "Рассылка успешно проведена!");

    }

    /**
     * TODO: рефакторинг - перенести все запросы и расчеты в модель.
     * TODO: ЖЕСТКИЙ ТЕСТ-ТРАХ, проверить БД на нагрузку при огромном количестве запросов.
     */
    public function postStopTournaments()
    {

        \DB::table('users')
            ->where('subscribed', 1)
            ->update(['subscribed' => 0, 'level' => 5]);

        $games = Game::where('isActive', true)->take(5)->get();

        foreach($games as $game) {
            $winners = $game->winners(200);
            $tempWinners = [];

            for($i = 0; $i < count($winners); $i++) {
                $tempWinners[$game->id][] = [
                    "user_id" => $winners[$i]->user_id,
                    'game_count' => $winners[$i]->game_count
                ];
            }

            for($k = 0; $k < count($winners); $k++) {
                $prize = Prize::where('game_id', $game->id)->where('place', $k+1)->first();
                $prize->user_id = $tempWinners[$game->id][$k]['user_id'];
                $prize->game_count = $tempWinners[$game->id][$k]['game_count'];
                $prize->save();
                $user = User::find($prize->user_id);
                $user->save();
            }

        }

        \DB::table('games')
            ->where('isActive', 1)
            ->update(['isActive' => 0]);

        \DB::table('games')
            ->where('isNext', 1)
            ->update(['isActive' => 1, 'isNext' => 0]);

        return \Redirect::back();


    }


}
