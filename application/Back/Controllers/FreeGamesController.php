<?php

namespace Back;

use Models\FreeGame;
use Mix\FileHelper;
use Mix\ImageHelper;

class FreeGamesController extends BaseController {

    public function getIndex()
    {
        $games = FreeGame::all();
        return \View::make('Back::free.list', compact('games'));
    }

    public function getCreate()
    {
        $game = FreeGame::all();
        $action['route'] = '\Back\FreeGamesController@postScore';
        $action['parameter'] = null;
        $info['bread'] = "добавить бесплатную игру";
        $info['head'] = "Создание бесплатной игры";
        return \View::make('Back::free.item', compact('game','action','info'));
    }

    public function postScore()
    {

        $input = \Input::all();

        $validator = \Validator::make($input, FreeGame::$rules, FreeGame::$messages);

        $validator->sometimes(array('game_file'), 'required', function ($input) {
            return isset($input->isActive) && $input->isActive == 1;
        });

        $validator->sometimes(array('alias'), 'required|alpha_dash|unique:free_games', function ($input) {
            return isset($input->isActive) && $input->isActive == 1;
        });

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $images_path = public_path()."/uploads/poster/free";
        $games_path = public_path()."/games/";

        // Замуты с файлами
        // Превью
        if(\Input::file('thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('thumbnail')->move($images_path, $thumbnail);

            $thumbnail_360x250 = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext);
            $input['thumbnail_360x250'] = $thumbnail_360x250;
        }

        //Фул
        if(\Input::file('game_file')) {
            $full_name = $input['alias'];
            $full_ext = \Input::file('game_file')->getClientOriginalExtension();
            $game_file = FileHelper::fileNameMerge('full', $full_name, $full_ext);
            $input['game_file'] = $game_file;
            $full_upload_success = \Input::file('game_file')->move($games_path, $game_file);
        }

        $game = FreeGame::create($input);

        return \Redirect::back()->with("success", "Новая игра успешно добавлена!");
    }

    public function getEdit($id)
    {
        $game = FreeGame::find($id);
        $action['route'] = '\Back\FreeGamesController@postUpdate';
        $action['parameter'] = $id;
        $info['bread'] = "добавить бесплатную игру";
        $info['head'] = "Создание бесплатной игры";
        return \View::make('Back::free.item', compact('game','action','info'));
    }

    public function postUpdate($id)
    {
        // get&&set
        $input = \Input::all();

        if(isset($input['isActive'])) {
            $input['isActive'] = 1;
        } else {
            $input['isActive'] = 0;
        }

        $validator = \Validator::make($input, FreeGame::$update_rules, FreeGame::$messages);

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $images_path = public_path()."/uploads/poster/free";
        $games_path = public_path()."/games/";

        $game = FreeGame::find($id);

        // Замуты с файлами
        // Превью
        if(\Input::file('thumbnail')) {
            $thumb_name = $input['alias'];
            $thumb_ext = \Input::file('thumbnail')->getClientOriginalExtension();
            $thumbnail = FileHelper::fileNameMerge('img', $thumb_name, $thumb_ext);
            $input['thumbnail'] = $thumbnail;
            $thumbnail_upload_success = \Input::file('thumbnail')->move($images_path, $thumbnail);

            $thumbnail_360x250 = ImageHelper::imageCrop($images_path, $thumb_name, $thumb_ext);
            $input['thumbnail_360x250'] = $thumbnail_360x250;
        } else {
            $input['thumbnail'] = $game->thumbnail;
        }

        //Фул
        if(\Input::file('game_file')) {
            $full_name = $input['alias'];
            $full_ext = \Input::file('game_file')->getClientOriginalExtension();
            $game_file = FileHelper::fileNameMerge('full', $full_name, $full_ext);
            $input['game_file'] = $game_file;
            $full_upload_success = \Input::file('game_file')->move($games_path, $game_file);
        } else {
            $input['game_file'] = $game->game_file;
        }

        $game->update($input);

        return \Redirect::back()->with("success", "Новый турнир SUPER GAME успешно добавлен!");
    }

    public function getDestroy($id)
    {
        FreeGame::destroy($id);
        return \Redirect::back();
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        foreach($input['id'] as $id) {
            FreeGame::destroy($id);
        }

        return \Redirect::back();
    }

}