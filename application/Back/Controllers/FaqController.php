<?php

namespace Back;

use Models\Faq;

class FaqController extends BaseController
{

    public function getIndex()
    {
        $faqs = Faq::all();
        return \View::make('Back::faq.list', compact('faqs'));
    }

    public function getCreate()
    {
        $action['route'] = '\Back\FaqController@postStore';
        $action['parameter'] = null;
        $info['bread'] = "Добавить новую справку";
        $info['head'] = "Добавление новой справки";
        $info['isCreate'] = true;
        $faq = Faq::all();
        $faq->parents = [];
        $faq->parents[0] = 'Нет родительской страницы';
        foreach ($faq as $item) {
            if(is_null($item->parent_id)) {
                $faq->parents[$item->id] = $item->title;
            }
        }

        return \View::make('Back::faq.model', compact('faq', 'action', 'info'));
    }

    public function postStore()
    {
        $input = \Input::all();
        $input['parent_id'] = $input['parent_id'] == 0 ? null : $input['parent_id'];

        $validator = \Validator::make($input, Faq::$rules, Faq::$messages);

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        Faq::create($input);

        return \Redirect::back()->with("success", "Новая страница успешно добавлена!");
    }

    public function getEdit($id)
    {
        $faq = Faq::find($id);
        $action['route'] = '\Back\FaqController@postUpdate';
        $action['parameter'] = $id;
        $info['bread'] = "update";
        $info['head'] = "Обновление данных страницы ".$faq->title;
        $info['isCreate'] = false;
        $parents = [];
        $parents[0] = 'Нет родительской страницы';
        $faqs = Faq::all();
        foreach ($faqs as $item) {
            if(is_null($item->parent_id)) {
                $parents[$item->id] = $item->title;
            }
        }

        return \View::make('Back::faq.model', compact('faq', 'action', 'info', 'parents'));
    }

    public function postUpdate($id)
    {
        $input = \Input::all();
        $input['parent_id'] = $input['parent_id'] == 0 ? null : $input['parent_id'];

        $validator = \Validator::make($input, Faq::$update_rules, Faq::$messages);

        if($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        $faq = Faq::find($id);
        $faq->update($input);

        return \Redirect::back()->with("success", "Страница успешно обновлена!");
    }

    public function getDestroy($id)
    {
        Faq::destroy($id);
        return \Redirect::back();
    }

    public function postMultiplyDestroy()
    {
        $input = \Input::all();

        foreach($input['uid'] as $id) {
            Faq::destroy($id);
        }

        return \Redirect::back();

    }
}