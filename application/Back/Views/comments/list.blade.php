@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/comments">Главная</a>
        </li>
        <li class="active">Комментарии</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Комментарии
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
               просмотр, редактирование, удаление
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            {{\Form::open(['action' => '\Back\CommentController@postMultiplyDestroy', 'role' => 'form'])}}
            <div class="button-group">
                <button type="submit" class="btn btn-danger"><i class="ace-icon fa fa-trash align-top bigger-125"></i> Удалить</button>
            </div>

            <table id="dymamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th class="center"></th>
                    <th>Пользователь</th>
                    <th>Игра\Статья\Новость</th>
                    <th>Комментарий</th>
                    <th>Тип поста</th>
                    <th>Статус</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($comments as $comment)
                <tr>
                    <td class="center">
                        <label class="pos-rel">
                            {{\Form::checkbox('uid[]', $comment->id, false, ['class' => 'ace'])}}
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td>{{$comment->user->nickname}}</td>
                    <td>
                        @if($comment->post_type == 1)
                            {{$comment->game->title}}
                        @elseif($comment->post_type == 2)
                            {{$comment->post->title}}
                        @elseif($comment->post_type == 3)
                            {{$comment->news->title}}
                        @endif

                    </td>
                    <td>{{$comment->comment}}</td>
                    <td>
                        @if($comment->post_type == 1)
                            {{'Игры'}}
                        @elseif($comment->post_type == 2)
                            {{'Блог'}}
                        @elseif($comment->post_type == 3)
                            {{'Новости'}}
                        @endif
                    </td>
                    <td>
                        @if($comment->status == 1)
                            {{'Опубликовано'}}
                        @elseif($comment->status == 0)
                            {{'Снято с публикации'}}
                        @endif
                    </td>
                    <td>
                        <div class="btn-group">
                            @if($comment->status == 1)
                                <a href="/admin/comments/dispublic/{{$comment->id}}" class="btn btn-xs btn-info">
                                    <i class="ace-icon fa fa-minus bigger-120"></i>
                                </a>
                            @elseif($comment->status == 0)
                                <a href="/admin/comments/public/{{$comment->id}}" class="btn btn-xs btn-info">
                                    <i class="ace-icon fa fa-plus bigger-120"></i>
                                </a>
                            @endif
                            <a href="/admin/comments/destroy/{{$comment->id}}" class="btn btn-xs btn-danger">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </a>

                        </div>

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop