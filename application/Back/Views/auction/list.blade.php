@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li class="active">Аукцион(лоты)</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Аукцион(лоты)
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Создание, просмотр, редактирование, удаление(CRUD)
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            {{\Form::open(['action' => '\Back\AuctionController@postMultiplyDestroy', 'role' => 'form'])}}
            <div class="button-group">
                <a href="/admin/lots/create" class="btn btn-primary"><i class="ace-icon fa fa-plus align-top bigger-125"></i> Добавить</a>
                <button type="submit" class="btn btn-danger"><i class="ace-icon fa fa-trash align-top bigger-125"></i> Удалить</button>
            </div>

            <table id="dymamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th class="center"></th>
                    <th class="center">Артикул</th>
                    <th>Изображение</th>
                    <th>Заголовок</th>
                    <th>Дата окончания</th>
                    <th>Начальная ставка</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($lots as $lot)
                <tr>
                    <td class="center">
                        <label class="pos-rel">
                            {{\Form::checkbox('uid[]', $lot->id, false, ['class' => 'ace'])}}
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td class="center">{{$lot->SKU}}</td>
                    <td><img src="/uploads/auction_images/{{$lot->crop_thumbnail}}" alt="{{$lot->alias}}" style="max-width: 100%;"/></td>
                    <td><a href="/auctions/{{$lot->alias}}" target="_blank">{{$lot->title}}</a></td>
                    <td>{{$lot->expiration_date}}</td>
                    <td>{{$lot->first_bet}}</td>
                    <td>
                        <div class="btn-group">
                            <a href="/admin/lots/edit/{{$lot->id}}" class="btn btn-xs btn-info">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </a>

                            <a href="/admin/lots/destroy/{{$lot->id}}" class="btn btn-xs btn-danger">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </a>

                        </div>

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop