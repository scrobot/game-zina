@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li>
            <a href="/admin/lots">Аукцион(лоты)</a>
        </li>
        <li class="active">
            {{$info['bread']}}
        </li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Аукцион(лоты)
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{$info['head']}}
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            @if( Session::has('success') )
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <p>
                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Заебись!!!
                        </strong>
                        {{Session::get('success')}}
                    </p>

                    <p>
                        <a href="/admin/lots" class="btn btn-sm btn-success">Просмотреть лоты?</a>
                        <a href="/admin/lots/create" class="btn btn-sm">Добавить еще?</a>
                    </p>
                </div>
            @endif
            @foreach ($errors->all(':message') as $message)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Всему пиздец!
                    </strong>
                    {{$message}}
                    <br>
                </div>
            @endforeach

        </div>
        <div class="col-xs-12">
            {{\Form::model($lot, ['action' => [$action['route'], $action['parameter']], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true])}}
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Изображение<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        @if(isset($lot->crop_thumbnail))
                            <div class='col-xs-10 col-sm-12'>
                                <img src="/uploads/auction_images/{{$lot->crop_thumbnail}}" width="75" style="margin-bottom: 30px" />
                            </div>
                        @endif
                            {{\Form::file('full_thumbnail', ['id' => 'thumb-file', 'class' => 'col-xs-10 col-sm-5'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Галерея </label>

                    <div class="col-sm-9">
                        @if(isset($lot->gallery_1))
                            <div class='col-xs-10 col-sm-12'>
                                <img src="/uploads/auction_images/{{$lot->gallery_1}}" width="75" style="margin-bottom: 30px" />
                            </div>
                        @endif
                        {{\Form::file('gallery[1]', ['id' => 'thumb-file', 'class' => 'col-xs-10 col-sm-5'])}}
                    </div>
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div class="col-sm-9">
                        @if(isset($lot->gallery_2))
                            <div class='col-xs-10 col-sm-12'>
                                <img src="/uploads/auction_images/{{$lot->gallery_2}}" width="75" style="margin-bottom: 30px" />
                            </div>
                        @endif
                        {{\Form::file('gallery[2]', ['id' => 'thumb-file', 'class' => 'col-xs-10 col-sm-5'])}}
                    </div>
                    <label class="col-sm-3 control-label no-padding-right"></label>
                    <div class="col-sm-9">
                        @if(isset($lot->gallery_3))
                            <div class='col-xs-10 col-sm-12'>
                                <img src="/uploads/auction_images/{{$lot->gallery_3}}" width="75" style="margin-bottom: 30px" />
                            </div>
                        @endif
                        {{\Form::file('gallery[3]', ['id' => 'thumb-file', 'class' => 'col-xs-10 col-sm-5'])}}
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Артикул(SKU)<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('SKU', null, ['placeholder' => "Заполняется автоматически при создании", 'class' => "col-xs-10 col-sm-5", 'readonly'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Заголовок<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('title', null, ['placeholder' => "Введите заголовок", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageTitle', 'required' => 'required'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Алиас<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('alias', null, ['placeholder' => "Введите алиас(путь) для вашей страницы", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageAlias', 'required' => 'required'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Дата окончания торгов<small class="red">*</small> </label>

                    <div class="col-sm-1">
                        <div class="input-daterange input-group">
                            {{\Form::text('expiration[date]', $expiration['date'], ['class' => 'input-sm form-control'])}}
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="input-group bootstrap-timepicker">
                            {{\Form::text('expiration[time]', $expiration['time'], ['class' => 'input-sm form-control', 'id' => 'timepicker1'])}}<span class="input-group-addon"><i class="fa fa-clock-o bigger-110"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-1">

                    </div>
                </div>
                @if(isset($info['type']))
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Активен?<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        <div class="col-sm-12">
                            <div class="checkbox">
                                <label>
                                    {{\Form::checkbox('status', $lot->status, $lot->status, ['class' => 'ace ace-switch ace-switch-4'])}}
                                    <span class="lbl"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Начальная ставка<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('first_bet', null, ['placeholder' => "Укажите начальную ставку для начала торгов", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Ключевые слова</label>

                    <div class="col-sm-9">
                        {{\Form::text('keywords', null, ['placeholder' => "Введите ключевые слова", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-8 col-sm-offset-2"> Описание<small class="red">*</small> </label>

                    <div class="col-sm-8 col-sm-offset-2">
                        {{\Form::textarea('lot_description', null, ['placeholder' => "Описание лота", 'class' => "col-xs-10 col-sm-5", 'id' => 'news-anons', 'required' => 'required'])}}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-8 col-sm-offset-2"> Технические характеристики </label>

                    <div class="col-sm-8 col-sm-offset-2">
                        {{\Form::textarea('lot_characteristics', null, ['placeholder' => "Технические характеристики лота", 'class' => "col-xs-10 col-sm-5", 'id' => 'pages-contents'])}}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-8 col-sm-offset-2"> SEO-описание </label>

                    <div class="col-sm-8 col-sm-offset-2">
                        {{\Form::textarea('description', null, ['placeholder' => "SEO-описание для лота", 'class' => "col-xs-10 col-sm-12"])}}
                    </div>
                </div>

                <div class="form-group">

                <div class="col-xs-12">
                    {{\Form::submit('Готово', ['class' => "btn btn-primary", "style" => "float:right;"])}}
                </div>

            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop