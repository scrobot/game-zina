@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li class="active">Документация GamezinaAPI</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            API
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Документация
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            <div id="mains">
                <h2>Основные объекты и методы</h2>

                <p>GamazinaAPI - представляет собой закрытый интерфейс программирования приложений, разработанный для связи данного проекта с ActionScript(flash-приложениями). Система отдает вспомогательные сущности, такие как состояние пользователей и запрашиваемого приложения.&nbsp;</p>

                <p>&nbsp;</p>
            </div>
            <div id="integration">
                <h3>Интеграция приложения</h3>

                <p>Приложение подключается к GamezinaAPI при помощи следущих свойств</p>

                <p>&nbsp;</p>

                <table class="table" border="0" cellpadding="2" cellspacing="2" style="line-height:1.6; width:100%">
                    <thead>
                    <tr>
                        <th scope="col"><strong>Свойство</strong></th>
                        <th scope="col">Тип</th>
                        <th scope="col"><strong>Описание</strong></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="text-align:center"><strong>api_id</strong></td>
                        <td style="text-align:center"><em>int</em></td>
                        <td><strong>Id приложения -&nbsp;</strong>используется в качестве ключа, запрашивающего данные приложения. Генерируется автоматически при создании игры</td>
                    </tr>
                    <tr>
                        <td style="text-align:center"><strong>api_key</strong></td>
                        <td style="text-align:center"><em>string</em></td>
                        <td><strong>Ключ приложения -&nbsp;</strong>сгенерированная 8-символьная строка. Основной ключ приложения.</td>
                    </tr>
                    <tr>
                        <td style="text-align:center"><strong>api_secret_token</strong></td>
                        <td style="text-align:center"><em>string</em></td>
                        <td><strong>Секретный токен приложения -&nbsp;</strong>Сгенерированная 24-символьная строка. Является секретным токеном. Назначение - усложнения генерации <em><strong>api_hash</strong></em>.</td>
                    </tr>
                    <tr>
                        <td style="text-align:center"><strong>api_hash</strong></td>
                        <td style="text-align:center"><em>string(hidden)</em></td>
                        <td>
                            <p><strong>Хэш-сумма приложения -&nbsp;</strong>Зашифрованная хэш-сумма приложения. Создается путем генерации шифровальным алгоритмом md5</p>
                            <pre>md5(<span style="color:#9876aa">api_id</span>.<span style="color:#9876aa">api_key</span>.<span style="color:#9876aa">api_secret_token</span>)</pre>
                            <p>Служит сущностью-обработчиком принимаемого хэша из приложения.</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div id="methods">
                <h3>Методы запросов</h3>

                <table class="table" border="0" cellpadding="5" cellspacing="5" style="width:100%">
                    <thead>
                    <tr>
                        <th scope="col">Метод API</th>
                        <th scope="col">Описание</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="#permission"> POST /api/gamezina/v1/request/post/permission</a></td>
                        <td>Проверяет интегрироцию приложения. &nbsp;</td>
                    </tr>
                    <tr>
                        <td><a href="#speed"> POST&nbsp;/api/gamezina/v1/request/post/speed</a></td>
                        <td>Возвращает состояние пользователя. Используется в турнирах</td>
                    </tr>
                    <tr>
                        <td><a href="#super">POST&nbsp;/api/gamezina/v1/request/post/super</a></td>
                        <td>Возвращает состояние пользователя и игры. Используется в Супер-игре</td>
                    </tr>
                    <tr>
                        <td><a href="#score">POST&nbsp;/api/gamezina/v1/request/post/score</a></td>
                        <td>Запись результата от пользователя.</td>
                    </tr>
                    <tr>
                        <td><a href="#user-state">GET /api/gamezina/v1/request/get/user-state</a></td>
                        <td>Возвращает состояние пользователя</td>
                    </tr>
                    <tr>
                        <td><a href="#game-state">GET /api/gamezina/v1/request/get/game-state</a></td>
                        <td>Возвращает состояние Игры</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>

                <p>&nbsp;</p>

            </div>

            <div id="permission">

                <h2><strong>PERMISSION</strong></h2>

                <p>Производит проверку приложения на интеграцию. Необходим для использования в качестве защитного порога, дабы провести проверку приложения, и в случае провала, остановить работу приложения.&nbsp;</p>

                <h3>&nbsp;</h3>

                <h3>Resource URL</h3>

                <p><strong><span style="color:rgb(57, 57, 57); font-family:open sans,sans-serif">POST /api/gamezina/v1/request/post/permission</span></strong></p>

                <p>&nbsp;</p>

                <h3>Returned Result</h3>

                <p><strong>boolean</strong></p>

                <p>&nbsp;</p>

                <h3>Parameters</h3>

                <table class="table">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">PARAMETER</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">laravel_session</td>
                        <td style="vertical-align:top">string</td>
                        <td style="vertical-align:top">Берется из <em>flashvars</em></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">hash</td>
                        <td style="vertical-align:top">string, md5-generated</td>
                        <td style="vertical-align:top">Хэш-сумма, сгенерированная при помощи md5 в приложении</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">api_id</td>
                        <td style="vertical-align:top">integer</td>
                        <td style="vertical-align:top">ID приложения</td>
                    </tr>
                    </tbody>
                </table>

                <p>&nbsp;</p>

            </div>

            <div id="speed">

                <h2><strong>SPEED</strong></h2>

                <p>Запрос состояния пользователя. проверка на наличие VIP-акканунта.</p>

                <p>&nbsp;</p>

                <h3>Resource URL</h3>

                <p><strong><span style="color:rgb(57, 57, 57); font-family:open sans,sans-serif">POST&nbsp;/api/gamezina/v1/request/post/speed</span></strong></p>

                <p>&nbsp;</p>

                <h3>Returned Result</h3>

                <p><strong>JSON</strong></p>

                <pre>
                {
                    &nbsp; &quot;user_state&quot;: <i>value</i>
                }</pre>

                <p>&nbsp;</p>

                <h3>Parameters</h3>

                <table class="table">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">PARAMETER</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">laravel_session</td>
                        <td style="vertical-align:top">string</td>
                        <td style="vertical-align:top">Секретный токен. Принимается из flashvars</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">user_token</td>
                        <td style="vertical-align:top">string</td>
                        <td style="vertical-align:top">Секретный токен пользователя. Принимается из flashvars</td>
                    </tr>
                    </tbody>
                </table>

                <p>&nbsp;</p>

                <h3>Responsed Fields</h3>

                <p><strong>USER_STATE</strong></p>

                <table class="table">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">Value</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">0</td>
                        <td style="vertical-align:top">integer</td>
                        <td style="vertical-align:top">Пользователь не зарегестрирован</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">1</td>
                        <td style="vertical-align:top"><span style="color:rgb(49, 57, 66); font-family:pt sans,arial,helvetica,sans-serif">integer</span></td>
                        <td style="vertical-align:top">Пользователь зарегестрирован, не имеет VIP-аккаунта</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">2</td>
                        <td style="vertical-align:top"><span style="color:rgb(49, 57, 66); font-family:pt sans,arial,helvetica,sans-serif">integer</span></td>
                        <td style="vertical-align:top"><span style="color:rgb(49, 57, 66); font-family:pt sans,arial,helvetica,sans-serif">Пользователь зарегестрирован, имеет VIP-аккаунта</span></td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <div id="super">
                <h2><strong>SUPER</strong></h2>

                <p>Запрос состояния пользователя и состояния игры. Проверка на наличие VIP-акканунта у пользователя, и проверка на старт супер-игры.</p>

                <p>&nbsp;</p>

                <h3>Resource URL</h3>

                <p><strong><span style="font-family:open sans,sans-serif">POST&nbsp;/api/gamezina/v1/request/post/super</span></strong></p>

                <p>&nbsp;</p>

                <h3>Returned Result</h3>

                <p><strong>JSON</strong></p>

                <pre>
                [
                &nbsp; {
                &nbsp; &nbsp; &quot;user_state&quot;: value
                &nbsp; },
                &nbsp; {
                &nbsp; &nbsp; &quot;game_state&quot;: {
                &nbsp; &nbsp; &nbsp; &quot;type&quot;: &quot;value&quot;,
                &nbsp; &nbsp; &nbsp; &quot;start&quot;: value
                &nbsp; &nbsp; }
                &nbsp; }
                ]</pre>

                <p>&nbsp;</p>

                <h3>Parameters</h3>

                <table class="table" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(57, 57, 57); font-family:open sans,sans-serif; line-height:19.5px; margin-bottom:20px; max-width:100%; width:1107px">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">PARAMETER</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">laravel_session</td>
                        <td style="vertical-align:top">string</td>
                        <td style="vertical-align:top">Секретный токен. Принимается из flashvars</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">api_id</td>
                        <td style="vertical-align:top">integer</td>
                        <td style="vertical-align:top">Id приложения</td>
                    </tr>
                    </tbody>
                </table>

                <p>&nbsp;</p>

                <h3>Responsed Fields</h3>

                <p><strong>USER_STATE</strong></p>

                <table class="table" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(57, 57, 57); font-family:open sans,sans-serif; line-height:19.5px; margin-bottom:20px; max-width:100%; width:1107px">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="font-weight:normal; line-height:20.7999992370605px">Value</span></font></th>
                        <th style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="font-weight:normal; line-height:20.7999992370605px">TYPE</span></font></th>
                        <th style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="font-weight:normal; line-height:20.7999992370605px">DESCRIPTION</span></font></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">0</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">integer</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">Пользователь не зарегестрирован</span></font></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">1</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">integer</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">Пользователь зарегестрирован, не имеет VIP-аккаунта</span></font></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">2</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">integer</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">Пользователь зарегестрирован, имеет VIP-аккаунта</span></font></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>GAME_STATE:TYPE</strong></p>

                <table class="table" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(57, 57, 57); font-family:open sans,sans-serif; line-height:19.5px; margin-bottom:20px; max-width:100%; width:1107px">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">Value</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">super</td>
                        <td style="vertical-align:top">string</td>
                        <td style="vertical-align:top">Тип игры - супер-игра</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">speed</td>
                        <td style="vertical-align:top"><span style="color:rgb(57, 57, 57); font-family:open sans,sans-serif">string</span></td>
                        <td style="vertical-align:top">Тип игры - турнир</td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>GAME_STATE:START</strong></p>

                <table class="table" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(57, 57, 57); font-family:open sans,sans-serif; line-height:19.5px; margin-bottom:20px; max-width:100%; width:1107px">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">Value</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">0</td>
                        <td style="vertical-align:top">integer</td>
                        <td style="vertical-align:top">Игра не началась</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">1</td>
                        <td style="vertical-align:top">integer</td>
                        <td style="vertical-align:top">Игра началась</td>
                    </tr>
                    </tbody>
                </table>


            </div>
            <div id="score">
                <h2><strong>SCORE</strong></h2>
                <p>на данный момент отсутствует реализация данного метода. Появится позже</p>
            </div>
            <div id="user-state">
                <h2><strong>USER-STATE</strong></h2>
                <p>Простой запрос состояния пользователя. Методы POST /api/gamezina/v1/request/post/speed и POST /api/gamezina/v1/request/post/super - использует этот запрос для компонования сложного запроса.</p>

                <h3>Resource URL</h3>

                <p><strong><span style="font-family:open sans,sans-serif">GET&nbsp;/api/gamezina/v1/request/get/user-state</span></strong></p>

                <p>&nbsp;</p>

                <h3>Returned Result</h3>

                <p><strong>JSON</strong></p>

                <pre>
                {
                  "user_state": value
                }
                </pre>

                <p>&nbsp;</p>

                <h3>Parameters</h3>

                <table class="table" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(57, 57, 57); font-family:open sans,sans-serif; line-height:19.5px; margin-bottom:20px; max-width:100%; width:1107px">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">PARAMETER</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">laravel_session</td>
                        <td style="vertical-align:top">string</td>
                        <td style="vertical-align:top">Секретный токен. Принимается из flashvars</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">user_token</td>
                        <td style="vertical-align:top">string</td>
                        <td style="vertical-align:top">Секретный токен пользователя. Принимается из flashvars</td>
                    </tr>
                    </tbody>
                </table>

                <p>&nbsp;</p>

                <h3>Responsed Fields</h3>

                <p><strong>USER_STATE</strong></p>

                <table class="table" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(57, 57, 57); font-family:open sans,sans-serif; line-height:19.5px; margin-bottom:20px; max-width:100%; width:1107px">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="font-weight:normal; line-height:20.7999992370605px">Value</span></font></th>
                        <th style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="font-weight:normal; line-height:20.7999992370605px">TYPE</span></font></th>
                        <th style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="font-weight:normal; line-height:20.7999992370605px">DESCRIPTION</span></font></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">0</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">integer</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">Пользователь не зарегестрирован</span></font></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">1</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">integer</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">Пользователь зарегестрирован, не имеет VIP-аккаунта</span></font></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">2</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">integer</span></font></td>
                        <td style="vertical-align:top"><font color="#333333" face="sans-serif, Arial, Verdana, Trebuchet MS"><span style="line-height:20.7999992370605px">Пользователь зарегестрирован, имеет VIP-аккаунта</span></font></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="game-state">
                <h2><strong>GAME-STATE</strong></h2>
                <p>Простой запрос состояния игры. Методы POST /api/gamezina/v1/request/post/speed и POST /api/gamezina/v1/request/post/super - использует этот запрос для компонования сложного запроса.</p>

                <p>&nbsp;</p>

                <h3>Resource URL</h3>

                <p><strong><span style="font-family:open sans,sans-serif">POST&nbsp;/api/gamezina/v1/request/post/super</span></strong></p>

                <p>&nbsp;</p>

                <h3>Returned Result</h3>

                <p><strong>JSON</strong></p>

                <pre>
                {
                  "game_state": {
                    "type": "value",
                    "start": value
                  }
                }
                </pre>

                <p>&nbsp;</p>

                <h3>Parameters</h3>

                <table class="table" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(57, 57, 57); font-family:open sans,sans-serif; line-height:19.5px; margin-bottom:20px; max-width:100%; width:1107px">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">PARAMETER</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">laravel_session</td>
                        <td style="vertical-align:top">string</td>
                        <td style="vertical-align:top">Секретный токен. Принимается из flashvars</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">api_id</td>
                        <td style="vertical-align:top">integer</td>
                        <td style="vertical-align:top">Id приложения</td>
                    </tr>
                    </tbody>
                </table>

                <p>&nbsp;</p>

                <h3>Responsed Fields</h3>

                <p><strong>GAME_STATE:TYPE</strong></p>

                <table class="table" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(57, 57, 57); font-family:open sans,sans-serif; line-height:19.5px; margin-bottom:20px; max-width:100%; width:1107px">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">Value</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">super</td>
                        <td style="vertical-align:top">string</td>
                        <td style="vertical-align:top">Тип игры - супер-игра</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">speed</td>
                        <td style="vertical-align:top"><span style="color:rgb(57, 57, 57); font-family:open sans,sans-serif">string</span></td>
                        <td style="vertical-align:top">Тип игры - турнир</td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>GAME_STATE:START</strong></p>

                <table class="table" style="border-collapse:collapse; border-spacing:0px; box-sizing:border-box; color:rgb(57, 57, 57); font-family:open sans,sans-serif; line-height:19.5px; margin-bottom:20px; max-width:100%; width:1107px">
                    <thead>
                    <tr>
                        <th style="vertical-align:top; width:200px">Value</th>
                        <th style="vertical-align:top">TYPE</th>
                        <th style="vertical-align:top">DESCRIPTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="vertical-align:top">0</td>
                        <td style="vertical-align:top">integer</td>
                        <td style="vertical-align:top">Игра не началась</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">1</td>
                        <td style="vertical-align:top">integer</td>
                        <td style="vertical-align:top">Игра началась</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div><!-- /.span -->
    </div><!-- /.row -->

@stop