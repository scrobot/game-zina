@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li>
            <a href="/admin/pages">Старницы</a>
        </li>
        <li class="active">
            {{$info['bread']}}
        </li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Старницы
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{$info['head']}}
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            @if( Session::has('success') )
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <p>
                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Заебись!!!
                        </strong>
                        {{Session::get('success')}}
                    </p>

                    <p>
                        <a href="/admin/pages" class="btn btn-sm btn-success">Просмотреть страницы?</a>
                        <a href="/admin/pages/create" class="btn btn-sm">Добавить еще?</a>
                    </p>
                </div>
            @endif
            @foreach ($errors->all(':message') as $message)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Всему пиздец!
                    </strong>
                    {{$message}}
                    <br>
                </div>
            @endforeach

        </div>
        <div class="col-xs-12">
            {{\Form::model($general, ['action' => [$action['route'], $action['parameter']], 'class' => 'form-horizontal', 'role' => 'form'])}}

            <h1>SEO раздел</h1>

            <h2>Паттерн тайтла страниц</h2>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Шаблон заголовка страницы<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('title_patterm', null, ['placeholder' => "например \"| GameZina.ru\"", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>

            <h2>Главная страница</h2>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Заголовок главной страницы<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('home_title', null, ['placeholder' => "Заголовок главной страницы", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Ключевые слова главной страницы<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('home_keywords', null, ['placeholder' => "Ключевые слова главной страницы", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Seo-описание главной страницы<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::textarea('home_description', null, ['placeholder' => "Seo-описание домашней страницы", 'class' => "col-xs-10 col-sm-12"])}}
                </div>
            </div>

            <h2>"Абонимент"</h2>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Заголовок страницы "Абонимент"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('subscriptions_title', null, ['placeholder' => "Заголовок страницы \"Абонимент\"", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Ключевые слова страницы "Абонимент"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('subscriptions_keywords', null, ['placeholder' => "Ключевые слова страницы \"Абонимент\"", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Seo-описание страницы "Абонимент"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::textarea('subscriptions_description', null, ['placeholder' => "Seo-описание страницы \"Абонимент\"", 'class' => "col-xs-10 col-sm-12"])}}
                </div>
            </div>

            <h2>"Статистика"</h2>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Заголовок страницы "Статистика"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('statistics_title', null, ['placeholder' => "Заголовок страницы \"Статистика\"", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Ключевые слова страницы "Статистика"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('statistics_keywords', null, ['placeholder' => "Ключевые слова страницы \"Статистика\"", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Seo-описание страницы "Статистика"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::textarea('statistics_keywords', null, ['placeholder' => "Seo-описание страницы \"Статистика\"", 'class' => "col-xs-10 col-sm-12"])}}
                </div>
            </div>

            <h2>"Блог"</h2>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Заголовок страницы "Блог"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('blog_title', null, ['placeholder' => "Заголовок страницы \"Блог\"", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Ключевые слова страницы "Блог"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('blog_acrhive_keywords', null, ['placeholder' => "Ключевые слова страницы \"Блог\"", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Seo-описание страницы "Блог"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::textarea('blog_acrhive_keywords', null, ['placeholder' => "Seo-описание страницы \"Блог\"", 'class' => "col-xs-10 col-sm-12"])}}
                </div>
            </div>

            <h2>"Новости"</h2>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Заголовок страницы "Новости"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('news_title', null, ['placeholder' => "Заголовок страницы \"Новости\"", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Ключевые слова страницы "Новости"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('news_archive_keywords', null, ['placeholder' => "Ключевые слова страницы \"Новости\"", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>\
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Seo-описание страницы "Новости"<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::textarea('news_archive_keywords', null, ['placeholder' => "Seo-описание страницы \"Новости\"", 'class' => "col-xs-10 col-sm-12"])}}
                </div>
            </div>

            <h1>Другое</h1>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Телефон Кол-центра</label>

                <div class="col-sm-9">
                    {{\Form::text('phone_of_call_center', null, ['placeholder' => "Телефон горячей линии", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> адрес группы ВКонтакте</label>

                <div class="col-sm-9">
                    {{\Form::text('vk_adress', null, ['placeholder' => "Ссылка на адрес группы ВКонтакте", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> адрес группы Одноклассников</label>

                <div class="col-sm-9">
                    {{\Form::text('ok_adress', null, ['placeholder' => "Ссылка на адрес группы Одноклассников", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> адрес группы Facebook</label>

                <div class="col-sm-9">
                    {{\Form::text('fb_adress', null, ['placeholder' => "Ссылка на адрес группы Facebook", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> адрес группы Twitter</label>

                <div class="col-sm-9">
                    {{\Form::text('tw_adress', null, ['placeholder' => "Ссылка на адрес группы Twitter", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Копирайты</label>

                <div class="col-sm-9">
                    {{\Form::text('copyrights', null, ['placeholder' => "Копирайты", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="col-xs-12">
                {{\Form::submit('СОЗДАТЬ', ['class' => "btn btn-primary", "style" => "float:right;"])}}
            </div>

            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop