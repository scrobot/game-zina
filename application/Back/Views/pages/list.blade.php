@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li class="active">Пользователи</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Страницы
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Создание, просмотр, редактирование, удаление(CRUD)
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            {{\Form::open(['action' => '\Back\PageController@postMultiplyDestroy', 'role' => 'form'])}}
            <div class="button-group">
                <a href="/admin/pages/create" class="btn btn-primary"><i class="ace-icon fa fa-plus align-top bigger-125"></i> Добавить</a>
                <button type="submit" class="btn btn-danger"><i class="ace-icon fa fa-trash align-top bigger-125"></i> Удалить</button>
            </div>

            <table id="dymamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th class="center"></th>
                    <th class="center">id</th>
                    <th>Заголовок</th>
                    <th>Алиас</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                <tr>
                    <td class="center">
                        <label class="pos-rel">
                            {{\Form::checkbox('uid[]', $page->id, false, ['class' => 'ace'])}}
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td class="center">{{$page->id}}</td>
                    <td>{{$page->title}}</td>
                    <td>{{$page->alias}}</td>
                    <td>
                        <div class="btn-group">
                            <a href="/admin/pages/edit/{{$page->id}}" class="btn btn-xs btn-info">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </a>

                            <a href="/admin/pages/destroy/{{$page->id}}" class="btn btn-xs btn-danger">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </a>

                        </div>

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop