@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li class="active">Консоль</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Консоль
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                обзор и статистика
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->

            <div class="row">
                <section id="tournament-manager" class="col-xs-12">
                    <h1 class="title col-xs-12 col-sm-6">Управление туринирами</h1>
                    <div class="col-xs-12 col-sm-6 countdown">
                        <p class="hidden-xs">Осталось: </p>
                        <div class="countdown-wrap"></div>
                    </div>
                    <div class="active-tournament clearfix">
                        <h2>Текущий турнир</h2>
                        @foreach($activeGames as $game)
                            <div class="col-xs-12 col-md-2 item">
                                <img src="/uploads/poster/{{$game->type}}/{{$game->thumbnail_360x250}}">
                                <h3>{{$game->title}}</h3>
                            </div>
                        @endforeach
                    </div>
                    {{\Form::open(['action' => '\Back\AdminController@postStopTournaments', 'class' => 'clearfix'])}}
                    {{\Form::hidden('the_end', 1)}}
                    {{\Form::button('Завершить турнир', ['type' => 'submit', 'class' => 'btn btn-primary'])}}
                    {{\Form::close()}}
                    <div class="active-tournament clearfix">
                        <h2>Следующие турниры</h2>
                        @foreach($nextGames as $game)
                            <div class="col-xs-12 col-md-2 item">
                                <img src="/uploads/poster/{{$game->type}}/{{$game->thumbnail_360x250}}">
                                <h3>{{$game->title}}</h3>
                            </div>
                        @endforeach
                    </div>
                </section>
            </div><!-- /.row -->

            <div class="hr hr32 hr-dotted"></div>


            <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@stop

