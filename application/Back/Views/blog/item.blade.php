@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li>
            <a href="/admin/news">Блог</a>
        </li>
        <li class="active">
            {{$info['bread']}}
        </li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Блог
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{$info['head']}}
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            @if( Session::has('success') )
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <p>
                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Заебись!!!
                        </strong>
                        {{Session::get('success')}}
                    </p>

                    <p>
                        <a href="/admin/blog" class="btn btn-sm btn-success">Просмотреть блог?</a>
                        <a href="/admin/blog/create" class="btn btn-sm">Добавить еще?</a>
                    </p>
                </div>
            @endif
            @foreach ($errors->all(':message') as $message)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Всему пиздец!
                    </strong>
                    {{$message}}
                    <br>
                </div>
            @endforeach

        </div>
        <div class="col-xs-12">
            {{\Form::model($post, ['action' => [$action['route'], $action['parameter']], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true])}}
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Изображение<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        @if(isset($post->thumbnail))
                            <div class='col-xs-10 col-sm-12'>
                                <img src="/uploads/poster/blog/{{$post->thumbnail}}" width="300" style="margin-bottom: 30px" />
                            </div>
                        @endif
                            {{\Form::file('thumbnail', ['id' => 'thumb-file', 'class' => 'col-xs-10 col-sm-5'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Заголовок<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('title', null, ['placeholder' => "Введите заголовок", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageTitle', 'required' => 'required'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Алиас<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('alias', null, ['placeholder' => "Введите алиас(путь) для вашей страницы", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageAlias', 'required' => 'required'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Ключевые слова</label>

                    <div class="col-sm-9">
                        {{\Form::text('keywords', null, ['placeholder' => "Введите ключевые слова", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageAlias'])}}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-8 col-sm-offset-2"> Анонс<small class="red">*</small> </label>

                    <div class="col-sm-8 col-sm-offset-2">
                        {{\Form::textarea('anons', null, ['placeholder' => "Содержание вашей страницы", 'class' => "col-xs-10 col-sm-5", 'id' => 'news-anons', 'required' => 'required'])}}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-8 col-sm-offset-2"> Содержание </label>

                    <div class="col-sm-8 col-sm-offset-2">
                        {{\Form::textarea('content', null, ['placeholder' => "Содержание вашей страницы", 'class' => "col-xs-10 col-sm-5", 'id' => 'pages-contents'])}}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-8 col-sm-offset-2"> SEO-описание </label>

                    <div class="col-sm-8 col-sm-offset-2">
                        {{\Form::textarea('description', null, ['placeholder' => "SEO-описание для вашей страницы", 'class' => "col-xs-10 col-sm-12"])}}
                    </div>
                </div>

                <div class="form-group">

                <div class="col-xs-12">
                    {{\Form::submit('Готово', ['class' => "btn btn-primary", "style" => "float:right;"])}}
                </div>

            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop