@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li class="active">Бесплатные игры</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Бесплатные игры
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Создание, просмотр, редактирование, удаление(CRUD)
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            {{\Form::open(['action' => '\Back\FreeGamesController@postMultiplyDestroy', 'role' => 'form'])}}
            <div class="button-group">
                <a href="/admin/free-games/create" class="btn btn-primary"><i class="ace-icon fa fa-plus align-top bigger-125"></i> Добавить</a>
                <button type="submit" class="btn btn-danger"><i class="ace-icon fa fa-trash align-top bigger-125"></i> Удалить</button>
            </div>
            <table id="dymamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th class="center">

                    </th>
                    <th class="center">id</th>
                    <th class="center">Превью</th>
                    <th>Заголовок</th>
                    <th>Ссылка на страницу</th>
                    <th class="center">Ссылка на игру</th>
                    <th class="center">Тип счетчика</th>
                    <th class="center">Активная</th>
                    <th class="center">Коэффицент</th>
                    <th class="center">Лимит набора баллов</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($games as $game)
                    <tr>
                        <td class="center">
                            <label class="pos-rel">
                                {{\Form::checkbox('id[]', $game->id, false, ['class' => 'ace'])}}
                                <span class="lbl"></span>
                            </label>
                        </td>
                        <td class="center">{{$game->id}}</td>
                        <td class="center"><img src="/uploads/poster/free/{{$game->type}}/{{$game->thumbnail_360x250}}" width="50"></td>
                        <td>{{$game->title}}</td>
                        <td><a target="_blank" href="/free-game/{{$game->alias}}">{{$game->alias}}</a></td>
                        <td class="center"><a target="_blank" href="/public/games/{{$game->game_file}}">{{$game->game_file}}</a></td>
                        <td class="center">{{$game->counter_type}}</td>
                        <td class="center">{{$game->isActive == 1 ? "Да" : "Нет"}}</td>
                        <td class="center">{{$game->coefficient}}</td>
                        <td class="center">{{$game->count_stop_limit}}</td>
                        <td>
                            <div class="btn-group">
                                <a href="/admin/free-games/edit/{{$game->id}}" class="btn btn-xs btn-info">
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                </a>

                                <a href="/admin/free-games/destroy/{{$game->id}}" class="btn btn-xs btn-danger">
                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                </a>

                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->
    <!-- Modal -->
    <div class="modal fade" id="prizesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Призы</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop