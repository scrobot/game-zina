@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li>
            <a href="/admin/free-games">Бесплатные игры</a>
        </li>
        <li class="active">
            {{$info['bread']}}
        </li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Бесплатные игры
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{$info['head']}}
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            @if( Session::has('success') )
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <p>
                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Заебись!!!
                        </strong>
                        {{Session::get('success')}}
                    </p>

                    <p>
                        <a href="/admin/free-games" class="btn btn-sm btn-success">Просмотреть список?</a>
                    </p>
                </div>
            @endif
            @foreach ($errors->all(':message') as $message)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Всему пиздец!
                    </strong>
                    {{$message}}
                    <br>
                </div>
            @endforeach

        </div>
        <div class="col-xs-12">
            {{\Form::model($game, ['action' => [$action['route'], $action['parameter']], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true])}}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Название игры<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('title', null, ['placeholder' => "Введите заголовок", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageTitle'])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Алиас<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('alias', null, ['placeholder' => "Введите алиас(путь) для страницы с турниром", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageAlias'])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Ключевые слова</label>

                <div class="col-sm-9">
                    {{\Form::text('keywords', null, ['placeholder' => "Введите ключевые слова", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageAlias'])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> SEO-описание </label>

                <div class="col-sm-9">
                    {{\Form::textarea('description', null, ['placeholder' => "SEO-описание для вашей страницы", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Текст для репоста </label>

                <div class="col-sm-9">
                    {{\Form::textarea('repost_title', null, ['placeholder' => "SEO-описание для вашей страницы", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>
            <div id="speedSettings">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Коэффицент расчета балов<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('coefficient', null,['class' => 'col-xs-10 col-sm-5', 'placeholder' => '0.0001'])}}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Старт для множителя при наборе x баллов<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::number('count_stop_limit', null,['class' => 'col-xs-10 col-sm-5', 'step' => 10000, 'placeholder' => '10000'])}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Тип счетчика<small class="red">*</small> </label>

                <div class="col-sm-9">
                    <div class="col-sm-12">
                        <label>
                            {{ \Form::radio('counter_type', 'straight', true, ['class' => 'ace']) }}
                            <span class="lbl"> Прямой</span>
                        </label>
                    </div>
                    <div class="col-sm-12">
                        <label>
                            {{ \Form::radio('counter_type', 'reverse', false, ['class' => 'ace']) }}
                            <span class="lbl"> Обратный</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Активная?<small class="red">*</small> </label>

                <div class="col-sm-9">
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label>
                                {{\Form::checkbox('isActive', true, false, ['class' => 'ace ace-switch ace-switch-4'])}}
                                <span class="lbl"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Превью(изображение)<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::file('thumbnail', ['id' => 'thumb-file', 'class' => 'col-xs-10 col-sm-5'])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Игра<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::file('game_file', ['id' => 'game-full-file', 'class' => 'col-xs-10 col-sm-5'])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-8 col-sm-offset-2"> Правила </label>

                <div class="col-sm-8 col-sm-offset-2">
                    {{\Form::textarea('rules', null, ['placeholder' => "Правила участия в турнире", 'class' => "col-xs-10 col-sm-5", 'id' => 'pages-contents'])}}
                </div>
            </div>

            <div class="form-group">

                <div class="col-xs-12">
                    {{\Form::submit('Готово', ['class' => "btn btn-primary", "style" => "float:right;"])}}
                </div>

                {{\Form::close()}}
            </div><!-- /.span -->
        </div><!-- /.row -->

@stop