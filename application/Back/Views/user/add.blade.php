@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li>
            <a href="/admin/users">Пользователи</a>
        </li>
        <li class="active">
            {{$info['bread']}}
        </li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Пользователи
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{$info['head']}}
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            @if( Session::has('success') )
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <p>
                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Заебись!!!
                        </strong>
                        {{Session::get('success')}}
                    </p>

                    <p>
                        <a href="/admin/users" class="btn btn-sm btn-success">Просмотреть пользователей?</a>
                        <a href="/admin/users/create" class="btn btn-sm">Добавить еще?</a>
                    </p>
                </div>
            @endif
            @foreach ($errors->all(':message') as $message)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Всему пиздец!
                    </strong>
                    {{$message}}
                    <br>
                </div>
            @endforeach

        </div>
        <div class="col-xs-12">
            {{\Form::model($user, ['action' => [$action['route'], $action['parameter']], 'class' => 'form-horizontal', 'role' => 'form'])}}
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Имя </label>

                    <div class="col-sm-9">
                        {{\Form::text('first_name', null, ['placeholder' => "Введите ваше имя", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Фамилия </label>

                    <div class="col-sm-9">
                        {{\Form::text('last_name', null, ['placeholder' => "Введите вашу фамилию", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Дата рождения </label>

                    <div class="col-sm-9">
                        {{\Form::text('bdate', null, ['class' => "date-picker col-xs-10 col-sm-5", 'id' => 'id-date-picker-1', 'data-date-format' => 'yyyy-mm-dd'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> E-mail<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::email('email', null, ['placeholder' => "например: mamba@mail.ru", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Логин<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('nickname', null, ['placeholder' => "например: mamba01011990", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Пароль<small class="red">*</small> </label>
                    <div class="col-sm-9">
                        {{\Form::password('password', ['placeholder' => "не менее 6-ти символов", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Поавторите пароль<small class="red">*</small> </label>
                    <div class="col-sm-9">
                        {{\Form::password('password_confirmation', ['placeholder' => "Повторите пароль", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Баланс </label>

                    <div class="col-sm-9">
                        {{\Form::text('balance', null, ['placeholder' => "Установите начальный баланс пользователя", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>


                <div class="control-group sex-group col-xs-6 col-xs-offset-3">
                    <label class="control-label bolder blue">Пол</label>

                    <div class="radio">
                        <label>
                            {{\Form::radio('sex', 2, true, ['class' => 'ace'])}}
                            <span class="lbl"> Мужской </span>
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            {{\Form::radio('sex', 1, false, ['class' => 'ace'])}}
                            <span class="lbl"> Женский </span>
                        </label>
                    </div>
                </div>

                <div class="control-group col-xs-6 col-xs-offset-3">
                    <label class="control-label bolder blue">Администратор</label>
                    <div class="checkbox">
                        <label>
                            {{\Form::checkbox('isAdmin', false, false, ['class' => 'ace ace-switch ace-switch-4'])}}
                            <span class="lbl"></span>
                        </label>
                    </div>

                </div>

                <div class="control-group col-xs-6 col-xs-offset-3">
                    <label class="control-label bolder blue">Активировать?</label>
                    <div class="checkbox">
                        <label>
                            {{\Form::checkbox('verified_email', false, false, ['class' => 'ace ace-switch ace-switch-4'])}}
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>

                <div class="col-xs-12">
                    {{\Form::submit('СОЗДАТЬ', ['class' => "btn btn-primary", "style" => "float:right;"])}}
                </div>

            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop