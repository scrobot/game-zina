@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li class="active">Пользователи</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Пользователи
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Создание, просмотр, редактирование, удаление(CRUD)
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            {{\Form::open(['action' => '\Back\UserController@postMultiplyDestroy', 'role' => 'form'])}}
            <div class="button-group">
                <a href="/admin/users/create" class="btn btn-primary"><i class="ace-icon fa fa-plus align-top bigger-125"></i> Добавить</a>
                <button type="submit" class="btn btn-danger"> Обновить</button>
            </div>
            <table id="dymamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th class="center">

                    </th>
                    <th class="center">id</th>
                    <th class="center">Avatar</th>
                    <th>Login</th>
                    <th>E-mail</th>
                    <th>Ссылка на профиль</th>
                    <th class="center">Админ</th>
                    <th class="center">Активирован</th>
                    <th class="center">Забанен</th>
                    <th class="center">Баланс, руб.</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                    <td class="center">
                        <label class="pos-rel">
                            {{\Form::checkbox('uid[]', $user->id, false, ['class' => 'ace'])}}
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td class="center">{{$user->id}}</td>
                    <td class="center"><img src="{{$user->photo == "empty-profile.png" ? "/images/empty-profile.png" : $user->photo}}" width="50"></td>
                    <td>{{$user->nickname}}</td>
                    <td><a href="mailto:{{$user->email}}">{{$user->email}}</a></td>
                    <td><a href="{{$user->profile}}" target="_blank">{{$user->profile}}</a></td>
                    <td class="center">
                        <label class="pos-rel">
                            <input type="checkbox" {{$user->isAdmin ? "checked" : ""}} />
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td class="center">
                        <label class="pos-rel">
                            <input type="checkbox" {{$user->verified_email ? "checked" : ""}} />
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td class="center">
                        <label class="pos-rel">
                            <input type="checkbox" name="banned[{{$user->id}}][]" {{$user->banned ? "checked" : ""}} />
                            @if($user->banned)
                                <a href="/admin/users/remove-ban/{{$user->id}}">Убрать бан?</a>
                            @endif
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td class="center">{{$user->balance}}</td>
                    <td>
                        <div class="btn-group">
                            <a href="/admin/users/edit/{{$user->id}}" class="btn btn-xs btn-info">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </a>

                            <a href="/admin/users/destroy/{{$user->id}}" class="btn btn-xs btn-danger">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </a>

                        </div>

                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{\Form::close()}}
            {{$users->links();}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop