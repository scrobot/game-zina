@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li class="active">Вопросы-ответы</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Вопросы-ответы
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Создание, просмотр, редактирование, удаление(CRUD)
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            {{\Form::open(['action' => '\Back\FaqController@postMultiplyDestroy', 'role' => 'form'])}}
            <div class="button-group">
                <a href="/admin/faq/create" class="btn btn-primary"><i class="ace-icon fa fa-plus align-top bigger-125"></i> Добавить</a>
                <button type="submit" class="btn btn-danger"><i class="ace-icon fa fa-trash align-top bigger-125"></i> Удалить</button>
            </div>

            <table id="dymamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th class="center"></th>
                    <th class="center">id</th>
                    <th>Заголовок</th>
                    <th>Алиас</th>
                    <th>Родительская страница</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($faqs as $faq)
                    <tr>
                        <td class="center">
                            <label class="pos-rel">
                                {{\Form::checkbox('uid[]', $faq->id, false, ['class' => 'ace'])}}
                                <span class="lbl"></span>
                            </label>
                        </td>
                        <td class="center">{{$faq->id}}</td>
                        <td>{{$faq->title}}</td>
                        <td>{{$faq->alias}}</td>
                        <td>
                            {{is_null($faq->getParent($faq->parent_id)) ? "нет": $faq->getParent($faq->parent_id)->title}}
                        </td>
                        <td>
                            <div class="btn-group">
                                <a href="/admin/faq/edit/{{$faq->id}}" class="btn btn-xs btn-info">
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                </a>

                                <a href="/admin/faq/destroy/{{$faq->id}}" class="btn btn-xs btn-danger">
                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                </a>

                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop