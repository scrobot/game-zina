@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li>
            <a href="/admin/faq">Воппросы-ответы</a>
        </li>
        <li class="active">
            {{$info['bread']}}
        </li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Воппросы-ответы
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{$info['head']}}
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            @if( Session::has('success') )
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <p>
                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Заебись!!!
                        </strong>
                        {{Session::get('success')}}
                    </p>

                    <p>
                        <a href="/admin/faq" class="btn btn-sm btn-success">Просмотреть страницы?</a>
                    </p>
                </div>
            @endif
            @foreach ($errors->all(':message') as $message)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Всему пиздец!
                    </strong>
                    {{$message}}
                    <br>
                </div>
            @endforeach

        </div>
        <div class="col-xs-12">
            {{\Form::model($faq, ['action' => [$action['route'], $action['parameter']], 'class' => 'form-horizontal', 'role' => 'form'])}}
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Заголовок<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('title', null, ['placeholder' => "Введите заголовок", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageTitle'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Алиас<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('alias', null, ['placeholder' => "Введите алиас(путь) для вашей страницы", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageAlias'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Ключевые слова</label>

                    <div class="col-sm-9">
                        {{\Form::text('keywords', null, ['placeholder' => "Введите ключевые слова", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageAlias'])}}
                    </div>
                </div>

                @if($info['isCreate'])
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Родительская страница</label>
                    <div class="col-sm-9">
                        {{ \Form::select('parent_id', $faq->parents, 0, ['class' => 'col-xs-10 col-sm-5']); }}
                    </div>
                </div>
                @else
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Родительская страница</label>
                    <div class="col-sm-9">
                        {{ \Form::select('parent_id', $parents, $faq->parent_id, ['class' => 'col-xs-10 col-sm-5']); }}
                    </div>
                </div>
                @endif

                <div class="form-group">
                    <label class="col-sm-8 col-sm-offset-2"> Содержание </label>

                    <div class="col-sm-8 col-sm-offset-2">
                        {{\Form::textarea('content', null, ['placeholder' => "Содержание вашей страницы", 'class' => "col-xs-10 col-sm-5", 'id' => 'pages-contents'])}}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-8 col-sm-offset-2"> SEO-описание </label>

                    <div class="col-sm-8 col-sm-offset-2">
                        {{\Form::textarea('description', null, ['placeholder' => "SEO-описание для вашей страницы", 'class' => "col-xs-10 col-sm-12"])}}
                    </div>
                </div>


                <div class="col-xs-12">
                    {{\Form::submit('СОЗДАТЬ', ['class' => "btn btn-primary", "style" => "float:right;"])}}
                </div>

            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop