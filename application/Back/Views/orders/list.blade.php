@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/comments">Главная</a>
        </li>
        <li class="active">Заказы</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Заказы
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
               просмотр
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            <table id="dymamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>Пользователь</th>
                    <th>Сумма</th>
                    <th>Статус</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->user->nickname}}</td>
                        <td>{{$order->sum}} рублей.</td>
                        <td>{{$order->status->status}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop