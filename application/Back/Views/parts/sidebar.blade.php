<div id="sidebar" class="sidebar responsive">
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <ul class="nav nav-list">
        <li class="active">
            <a href="/admin/admin-page">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Консоль </span>
            </a>

            <b class="arrow"></b>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-gamepad"></i>
                <span class="menu-text">
                    Игры
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="/admin/games/create">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Добавить игру
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="/admin/games/">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Посмотреть игры
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-ticket"></i>
                <span class="menu-text">
                    Бесплатные Игры
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="/admin/free-games/create">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Добавить игру
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="/admin/free-games/">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Посмотреть игры
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-usd"></i>
                <span class="menu-text">
                    Аукцион(лоты)
                </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="/admin/lots/create">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Добавить лот
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="/admin/lots/">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Посмотреть лоты
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-user"></i>
                <span class="menu-text">
                    Пользователи
                </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="">
                    <a href="/admin/users/create">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Добавить
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="/admin/users/">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Просмотреть
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-file"></i>
                <span class="menu-text">
                    Страницы
                </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="">
                    <a href="/admin/pages/create">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Добавить
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="/admin/pages">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Просмотреть
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-question"></i>
                <span class="menu-text">
                    Вопросы-ответы
                </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">

                <li class="">
                    <a href="/admin/faq/create">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Добавить
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="/admin/faq">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Просмотреть
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-picture-o"></i>
                <span class="menu-text">
                    Баннеры
                </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="/admin/banners">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Просмотреть
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="/admin/banners/create">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Добавить
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-newspaper-o"></i>
                <span class="menu-text">
                    Новости
                </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="/admin/news">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Просмотреть
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="/admin/news/create">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Добавить
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-file-text"></i>
                <span class="menu-text">
                    Блог
                </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="/admin/blog">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Просмотреть
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="">
                    <a href="/admin/blog/create">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Добавить
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="/admin/orders">
                <i class="menu-icon fa fa-bars"></i>
                <span class="menu-text">
                    Заказы
                </span>
            </a>
        </li>

        <li class="">
            <a href="/admin/settings">
                <i class="menu-icon fa fa-cog"></i>
                <span class="menu-text">
                    Общая информация
                </span>
            </a>
        </li>

        <li class="">
            <a href="/admin/menu">
                <i class="menu-icon fa fa-sitemap"></i>
                <span class="menu-text">
                    Меню
                </span>
            </a>
        </li>

        <li class="">
            <a href="/admin/comments">
                <i class="menu-icon fa fa-comments"></i>
                <span class="menu-text">
                    Комментарии
                </span>
            </a>
        </li>

        <li class="">
            <a href="/admin/email-newsletter">
                <i class="menu-icon fa fa-envelope-o"></i>
                <span class="menu-text">
                    E-mail Рассылка
                </span>
            </a>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-cogs"></i>
                <span class="menu-text">
                    API
                </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="/admin/api/doc#mains">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Основные объекты и методы
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/admin/api/doc#integration">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Интеграция приложения
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/admin/api/doc#methods">
                        <i class="menu-icon fa fa-eye pink"></i>
                        Методы запросов
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/admin/api/doc#permission">
                        <i class="menu-icon fa fa-eye pink"></i>
                        PERMISSION
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/admin/api/doc#speed">
                        <i class="menu-icon fa fa-eye pink"></i>
                        SPEED
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/admin/api/doc#super">
                        <i class="menu-icon fa fa-eye pink"></i>
                        SUPER
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/admin/api/doc#score">
                        <i class="menu-icon fa fa-eye pink"></i>
                        SCORE
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/admin/api/doc#user-state">
                        <i class="menu-icon fa fa-eye pink"></i>
                        USER-STATE
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/admin/api/doc#game-state">
                        <i class="menu-icon fa fa-eye pink"></i>
                        GAME-STATE
                    </a>

                    <b class="arrow"></b>
                </li>

            </ul>
        </li>

    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>