@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li>
            Игры
        </li>
        <li>
            <a href="/admin/games">Games</a>
        </li>
        <li class="active">
            {{$info['bread']}}
        </li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Турниры
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{$info['head']}}
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            @if( Session::has('success') )
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <p>
                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Заебись!!!
                        </strong>
                        {{Session::get('success')}}
                    </p>

                    <p>
                        <a href="/admin/games/" class="btn btn-sm btn-success">Просмотреть страницы?</a>
                        <a href="/admin/games/create" class="btn btn-sm">Добавить еще?</a>
                    </p>
                </div>
            @endif
            @foreach ($errors->all(':message') as $message)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Всему пиздец!
                    </strong>
                    {{$message}}
                    <br>
                </div>
            @endforeach

        </div>

        <div class="col-xs-12">
            {{\Form::open(['action' => [$action['route'], $action['parameter']], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true])}}
            <div class="widget-box transparent" id="recent-box">
                <div class="widget-header">
                    <h4 class="widget-title lighter smaller">
                        <i class="ace-icon fa fa-rss orange"></i>Игра
                    </h4>

                    <div class="widget-toolbar no-border">
                        <ul class="nav nav-tabs" id="recent-tab">
                            <li class="active">
                                <a data-toggle="tab" href="#settings-tab">Основные сведения</a>
                            </li>

                            <li>
                                <a data-toggle="tab" href="#prizes-tab">Призы</a>
                            </li>

                            <li>
                                <a data-toggle="tab" href="#api-tab">API</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main padding-4">
                        <div class="tab-content padding-8">
                            <div id="settings-tab" class="tab-pane active">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Название игры<small class="red">*</small> </label>

                                    <div class="col-sm-9">
                                        {{\Form::text('title', null, ['placeholder' => "Введите заголовок", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageTitle'])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Алиас<small class="red">*</small> </label>

                                    <div class="col-sm-9">
                                        {{\Form::text('alias', null, ['placeholder' => "Введите алиас(путь) для страницы с турниром", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageAlias'])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Ключевые слова</label>

                                    <div class="col-sm-9">
                                        {{\Form::text('keywords', null, ['placeholder' => "Введите ключевые слова", 'class' => "col-xs-10 col-sm-5", 'id' => 'pageAlias'])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-8 col-sm-offset-2"> SEO-описание </label>

                                    <div class="col-sm-8 col-sm-offset-2">
                                        {{\Form::textarea('description', null, ['placeholder' => "SEO-описание для вашей страницы", 'class' => "col-xs-10 col-sm-12"])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Тип игры<small class="red">*</small> </label>

                                    <div class="col-sm-9">
                                        <div class="col-sm-12">
                                            <label>
                                                {{ \Form::radio('type', 'super', true, ['class' => 'ace']) }}
                                                <span class="lbl super-game"> Супер игра</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-12">
                                            <label>
                                                {{ \Form::radio('type', 'speed', false, ['class' => 'ace']) }}
                                                <span class="lbl speed-game"> Speed Game</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div id="attempts">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right"> Количество попыток<small class="red">*</small> </label>

                                        <div class="col-sm-9">
                                            {{\Form::number('attempts_count', null,['class' => 'col-xs-10 col-sm-5', 'placeholder' => '5'])}}
                                        </div>
                                    </div>
                                </div>
                                <div id="speedSettings">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right"> Коэффицент расчета балов<small class="red">*</small> </label>

                                        <div class="col-sm-9">
                                            {{\Form::text('coefficient', null,['class' => 'col-xs-10 col-sm-5', 'placeholder' => '0.0001'])}}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right"> Старт для множителя при наборе x баллов<small class="red">*</small> </label>

                                        <div class="col-sm-9">
                                            {{\Form::number('count_stop_limit', null,['class' => 'col-xs-10 col-sm-5', 'step' => 10000, 'placeholder' => '10000'])}}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Тип счетчика<small class="red">*</small> </label>

                                    <div class="col-sm-9">
                                        <div class="col-sm-12">
                                            <label>
                                                {{ \Form::radio('counter_type', 'straight', true, ['class' => 'ace']) }}
                                                <span class="lbl"> Прямой</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-12">
                                            <label>
                                                {{ \Form::radio('counter_type', 'reverse', false, ['class' => 'ace']) }}
                                                <span class="lbl"> Обратный</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Турнир текущей недели?<small class="red">*</small> </label>

                                    <div class="col-sm-9">
                                        <div class="col-sm-12">
                                            <div class="checkbox">
                                                <label>
                                                    {{\Form::checkbox('isActive', true, false, ['class' => 'ace ace-switch ace-switch-4'])}}
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Турнир следуюшей недели?<small class="red">*</small> </label>

                                    <div class="col-sm-9">
                                        <div class="col-sm-12">
                                            <div class="checkbox">
                                                <label>
                                                    {{\Form::checkbox('isNext', true, false, ['class' => 'ace ace-switch ace-switch-4'])}}
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Превью(изображение)<small class="red">*</small> </label>

                                    <div class="col-sm-9">
                                        {{\Form::file('thumbnail', ['id' => 'thumb-file', 'class' => 'col-xs-10 col-sm-5'])}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Игра(полная версия)<small class="red">*</small> </label>

                                    <div class="col-sm-9">
                                        {{\Form::file('game_file', ['id' => 'game-full-file', 'class' => 'col-xs-10 col-sm-5'])}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Дата начала и окончания турнира<small class="red">*</small> </label>

                                    <div class="col-sm-9">
                                        <div class="input-daterange input-group">
                                            {{\Form::text('start_date', null, ['class' => 'input-sm form-control'])}}
                                            <span class="input-group-addon">
                            <i class="fa fa-exchange"></i>
                        </span>
                                            {{\Form::text('end_date', null, ['class' => 'input-sm form-control'])}}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-8 col-sm-offset-2"> Правила </label>

                                    <div class="col-sm-8 col-sm-offset-2">
                                        {{\Form::textarea('rules', null, ['placeholder' => "Правила участия в турнире", 'class' => "col-xs-10 col-sm-5", 'id' => 'pages-contents'])}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-8 col-sm-offset-2"> Текст для репоста </label>

                                    <div class="col-sm-8 col-sm-offset-2">
                                        {{\Form::textarea('repost_title', null, ['class' => "col-xs-10 col-sm-5"])}}
                                    </div>
                                </div>

                            </div>

                            <div id="prizes-tab" class="tab-pane">
                                {{\Form::hidden('all_bank', null, ['id' => 'all_bank'])}}

                                <div id="calculator" class="clearfix">
                                    <div class="calc-header clearfix">
                                        <h2>Калькулятор расчета призов</h2>
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="lastPlace">
                                                        Приз за последнее место, руб.
                                                    </label>
                                                    <input type="number" class="form-control" id="lastPlace" placeholder="50" step="5">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="firstStep">
                                                        Первый шаг, руб.
                                                    </label>
                                                    <input type="number" class="form-control" id="firstStep" placeholder="10" step="5">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="secondStep">
                                                        Второй шаг, руб.
                                                    </label>
                                                    <input type="number" class="form-control" id="secondStep" placeholder="50" step="5">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="finalStep">
                                                        Шаг 10ки победителей, руб.
                                                    </label>
                                                    <input type="number" class="form-control" id="finalStep" placeholder="1000" step="100">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="firstPlace">
                                                        Первое место
                                                    </label>
                                                    <input type="text" class="form-control" id="firstPlace" placeholder="Nissan Qashqai">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="secondPlace">
                                                        Второе место
                                                    </label>
                                                    <input type="number" class="form-control" id="secondPlace" placeholder="50000" step="1000">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="thirdPlace">
                                                        Третье место
                                                    </label>
                                                    <input type="number" class="form-control" id="thirdPlace" placeholder="25000" step="500">
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-wide btn-red" id="caclSubmit">
                                                Расчитать
                                            </button>
                                        </div>
                                    </div>
                                    <div class="calc-table" style="margin-top: 30px">
                                        <h2 id="allBank" style="display: none">Общий банк <strong class="red"> рублей.</strong></h2>
                                        <table id="results-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th class="center">Место</th>
                                                <th>Приз</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div id="api-tab" class="tab-pane">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Api ключ</label>

                                    <div class="col-sm-9">
                                        {{ \Form::text('api_key', null, ['placeholder' => "Введите ключ API, или сгенерируйте его", 'class' => "col-xs-10 col-sm-5"])}}
                                        <a href="/admin/games/generate-token" class="generate-secret" data-apiKeyLength="8">Сгенерировать</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"> Секретный токен</label>

                                    <div class="col-sm-9">
                                        {{ \Form::text('api_secret_token', null, ['placeholder' => "сгенерируйте секретный токен", 'class' => "col-xs-10 col-sm-5"])}}
                                        <a href="/admin/games/generate-token" class="generate-secret" data-apiKeyLength="24">Сгенерировать</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.widget-main -->
                </div><!-- /.widget-body -->
            </div><!-- /.widget-box -->


            <div class="col-xs-12">
                {{\Form::submit('СОЗДАТЬ', ['class' => "btn btn-primary", "style" => "float:right;"])}}
            </div>

            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

    <style>
        #attempts {
            display: block;
        }
        #speedSettings {
            display: none;
        }
    </style>
@stop