<table id="static-table" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th class="center">id</th>
        <th class="center">Приз</th>
    </tr>
    </thead>
    <tbody>
        @foreach($prizes as $prize)
            <tr>
                <td class="center">{{$prize->place}}</td>
                <td class="center">{{$prize->prize}}</td>
            </tr>
        @endforeach
    </tbody>
</table>