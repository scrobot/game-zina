@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li>
            <a href="/admin/banners">Баннеры</a>
        </li>
        <li class="active">
            {{$info['bread']}}
        </li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Баннеры
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{$info['head']}}
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            @if( Session::has('success') )
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <p>
                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Заебись!!!
                        </strong>
                        {{Session::get('success')}}
                    </p>

                    <p>
                        <a href="/admin/banners" class="btn btn-sm btn-success">Просмотреть баннеры?</a>
                        <a href="/admin/banners/create" class="btn btn-sm">Добавить еще?</a>
                    </p>
                </div>
            @endif
            @foreach ($errors->all(':message') as $message)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Всему пиздец!
                    </strong>
                    {{$message}}
                    <br>
                </div>
            @endforeach

        </div>
        <div class="col-xs-12">
            {{\Form::model($banner, ['action' => [$action['route'], $action['parameter']], 'class' => 'form-horizontal', 'role' => 'form', 'files' => true])}}
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Изображение<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        @if(isset($banner->image))
                            <img src="/uploads/banners/{{$banner->image}}" width="300" />
                        @endif
                        {{\Form::file('image', ['id' => 'thumb-file', 'class' => 'col-xs-10 col-sm-5'])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Ссылка<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::text('link', null, ['placeholder' => "Введите ссылку страницы, на которую должен вести текущий баннер", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right"> Вес<small class="red">*</small> </label>

                    <div class="col-sm-9">
                        {{\Form::number('weight', null, ['placeholder' => "Введите очередность баннера", 'class' => "col-xs-10 col-sm-5"])}}
                    </div>
                </div>

                <div class="col-xs-12">
                    {{\Form::submit('Готово', ['class' => "btn btn-primary", "style" => "float:right;"])}}
                </div>

            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop