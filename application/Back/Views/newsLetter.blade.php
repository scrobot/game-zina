@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/admin-page">Главная</a>
        </li>
        <li class="active">
            {{$info['bread']}}
        </li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Email рассылка
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                {{$info['head']}}
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            @if( Session::has('success') )
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <p>
                        <strong>
                            <i class="ace-icon fa fa-check"></i>
                            Заебись!!!
                        </strong>
                        {{Session::get('success')}}
                    </p>
                </div>
            @endif
            @foreach ($errors->all(':message') as $message)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>

                    <strong>
                        <i class="ace-icon fa fa-times"></i>
                        Всему пиздец!
                    </strong>
                    {{$message}}
                    <br>
                </div>
            @endforeach

        </div>
        <div class="col-xs-12">
            {{\Form::open(['action' => [$action['route'], $action['parameter']], 'class' => 'form-horizontal', 'role' => 'form'])}}

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Тема письма<small class="red">*</small> </label>

                <div class="col-sm-9">
                    {{\Form::text('theme', null, ['placeholder' => "Например: Новости на GameZina.ru", 'class' => "col-xs-10 col-sm-5"])}}
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-8 col-sm-offset-2"> Содержание </label>

                <div class="col-sm-8 col-sm-offset-2">
                    {{\Form::textarea('content', null, ['placeholder' => "Содержание письма", 'class' => "col-xs-10 col-sm-5", 'id' => 'pages-contents'])}}
                </div>
            </div>

            <div class="col-xs-12">
                {{\Form::submit('Отправить', ['class' => "btn btn-primary", "style" => "float:right;"])}}
            </div>

            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop