@extends('Back::layout')

@section('breadcrumb')

    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/admin/blog">Главная</a>
        </li>
        <li class="active">Меню</li>
    </ul><!-- /.breadcrumb -->

@stop

@section('page-header')
    <div class="page-header">
        <h1>
            Меню
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Создание, просмотр, редактирование, удаление(CRUD)
            </small>
        </h1>
    </div><!-- /.page-header -->
@stop

@section('main-content')
    <div class="row">
        <div class="col-xs-12">
            {{\Form::model($menus,['action' => '\Back\MenuController@postChange', 'role' => 'form'])}}

            <table id="dymamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th class="center">Удалить?</th>
                    <th>Заголовок</th>
                    <th>Ссылка</th>
                    <th>Вес</th>
                    <th>Показывать в хэдере</th>
                    <th>Показывать в футере</th>
                </tr>
                </thead>
                <tbody>
                @foreach($menus as $menu)
                <tr>
                    <td class="center">
                        <label class="pos-rel">
                            {{\Form::checkbox('uid[]', $menu->id, false, ['class' => 'ace'])}}
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td>
                        {{\Form::hidden('menuEdit[id][]', $menu->id)}}
                        {{\Form::text('menuEdit[title][]', $menu->title, ['placeholder' => "Введите заголовок пункта меню", 'class' => "col-xs-10 col-sm-12"])}}
                    </td>
                    <td>{{\Form::text('menuEdit[link][]', $menu->link, ['placeholder' => "Введите ссылку пункта меню", 'class' => "col-xs-10 col-sm-12"])}}</td>
                    <td>{{\Form::number('menuEdit[weight][]', $menu->weight, ['placeholder' => "Введите очередность пункта меню", 'class' => "col-xs-10 col-sm-12"])}}</td>
                    <td style="text-align: center">{{ Form::checkbox("menuEdit[header][$menu->id]", 1, $menu->header) }}</td>
                    <td style="text-align: center">{{ Form::checkbox("menuEdit[footer][$menu->id]", 1, $menu->footer) }}</td>
                </tr>
                @endforeach
                <tr>
                    <td class="center"></td>
                    <td>{{\Form::text('menuCreate[title]', null, ['placeholder' => "Введите заголовок пункта меню", 'class' => "col-xs-10 col-sm-12"])}}</td>
                    <td>{{\Form::text('menuCreate[link]', null, ['placeholder' => "Введите ссылку пункта меню", 'class' => "col-xs-10 col-sm-12"])}}</td>
                    <td>{{\Form::number('menuCreate[weight]', null, ['placeholder' => "Введите очередность пункта меню", 'class' => "col-xs-10 col-sm-12"])}}</td>
                    <td style="text-align: center">{{ Form::checkbox('menuCreate[header]', 1, 0) }}</td>
                    <td style="text-align: center">{{ Form::checkbox('menuCreate[footer]', 1, 0) }}</td>
                </tr>
                </tbody>
            </table>
            <div class="button-group">
                <button type="submit" class="btn btn-default"><i class="ace-icon fa fa-thumbs-up align-top bigger-125"></i> Изменить</button>
            </div>
            {{\Form::close()}}
        </div><!-- /.span -->
    </div><!-- /.row -->

@stop