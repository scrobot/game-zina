<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Game-zina.ru</title>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/materialize.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <header id="header">
        <section class="header-content cols clearfix">
            <div class="col col-40">
                <a href="/" id="logo">
                    <img src="images/logo.png" alt="logo">
                    <img src="images/zaplatka_game_zina_logo.png" alt="logo2" class="zaplatka">
                </a>
                <ul class="menu left-menu clearfix">
                    <li class="menu-item"><a class="front" href="index.php">Главная</a></li>
                    <li class="menu-item"><a href="/html-layout/rules.php">Правила</a></li>
                    <li class="menu-item"><a href="about.php">О нас</a></li>
                </ul>
            </div>
            <div class="col col-20">
                <div class="video-box">
                    <div class="video-box-mask"></div>
                    <div class="video-box-content">
                        <img src="uploads/video/thumbs/thumb1.jpg" alt="Трейлер">
                    </div>
                    <i class="play-button" data-video="//www.youtube.com/embed/sCwrvUDiCQc">Play</i>
                </div>
            </div>
            <div class="col col-40">
                <ul class="menu buttons-menu clearfix">
                    <li class="menu-item"><a class="user-action" href="/html-layout/registration.php">Регистрация</a></li>
                    <li class="menu-item"><a class="user-action" href="/html-layout/login.php">Вход</a></li>
                </ul>
                <nav class="socials-list clearfix">
                    <a href="#" class="vk"></a>
                    <a href="#" class="ok"></a>
                    <a href="#" class="fb"></a>
                    <a href="#" class="yd"></a>
                    <a href="#" class="dog"></a>
                    <a href="#" class="gg"></a>
                    <a href="#" class="tw"></a>
                </nav>
                <ul class="menu right-menu clearfix">
                    <li class="menu-item small-item"><a href="/html-layout/payment.php">Платежные<br/>системы</a></li>
                    <li class="menu-item"><a href="/html-layout/partners.php">Партнеры</a></li>
                    <li class="menu-item"><a href="/html-layout/contacts.php">Контакты</a></li>
                </ul>
            </div>
        </section>
    </header>
    <div id="banner">
        <img src="uploads/banner.png" alt="banner">
    </div>
    <div id="page-wrapper" class="clearfix">
        <div class="page-content">
            <div id="games-block">
                <div class="col col-592 super-game">
                    <h2 class="title">super game</h2>
                    <div class="content-offset">
                        <div class="content-inset">
                            <div class="content-block">
                                <div class="row-game">
                                    <div class="game-screen col">
                                        <div class="in-screen">
                                            <h3 class="game-title">Лабиринт</h3>
                                            <p>начало турнира<br/>20.10.2014<br/> 20:00<br/> по москве</p>
                                            <a href="#" class="trial-button">Тренировка</a>
                                        </div>
                                        <div class="out-screen">
                                            <h3>До старта</h3>
                                            <span class="time">143:26:15</span>
                                        </div>
                                    </div>
                                    <div class="game-prize col">
                                        <a class="waves-effect waves-brown" href="javascript:void(0);"><span class="round">1</span> nissan qashqai</a>
                                        <a class="waves-effect waves-brown" href="javascript:void(0);"><span class="round">2</span> 150 000 rub</a>
                                        <a class="waves-effect waves-brown" href="javascript:void(0);"><span class="round">3</span> 100 000 rub</a>
                                        <a class="waves-effect waves-brown all" href="javascript:void(0);">Посмотреть все призовые места</a>
                                    </div>
                                </div>
                                <div class="row-game">
                                    <div class="game-screen col">
                                        <div class="in-screen">
                                            <h3 class="game-title">Пробки</h3>
                                            <p>начало турнира<br/>20.10.2014<br/> 20:00<br/> по москве</p>
                                            <a href="#" class="trial-button">Тренировка</a>
                                        </div>
                                        <div class="out-screen">
                                            <h3>До старта</h3>
                                            <span class="time">143:26:15</span>
                                        </div>
                                    </div>
                                    <div class="game-prize col">
                                        <a class="waves-effect waves-brown" href="javascript:void(0);"><span class="round">1</span> nissan qashqai</a>
                                        <a class="waves-effect waves-brown" href="javascript:void(0);"><span class="round">2</span> 150 000 rub</a>
                                        <a class="waves-effect waves-brown" href="javascript:void(0);"><span class="round">3</span> 100 000 rub</a>
                                        <a class="waves-effect waves-brown all" href="javascript:void(0);">Посмотреть все призовые места</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-592 game-speed">
                    <h2 class="title">game speed</h2>
                    <div class="content-offset">
                        <div class="content-inset">
                            <div class="content-block">
                                <div class="game-screen">
                                    <div class="game-thumb"><img src="uploads/games/snake.png"></div>
                                    <div class="game-screen-mask">
                                        <h4 class="game-title"><a href="/html-layout/snake.phput/snake.php">Snake</a></h4>
                                        <a href="#" class="prize-places">Призовые места</a>
                                    </div>
                                </div>
                                <div class="game-screen">
                                    <div class="game-thumb"><img src="uploads/games/race.png"></div>
                                    <div class="game-screen-mask">
                                        <h4 class="game-title"><a href="/html-layout/race.phpout/race.php">Race</a></h4>
                                        <a href="#" class="prize-places">Призовые места</a>
                                    </div>
                                </div>
                                <div class="game-screen">
                                    <div class="game-thumb"><img src="uploads/games/bird.png"></div>
                                    <div class="game-screen-mask">
                                        <h4 class="game-title"><a href="/html-layout/bird.phpout/bird.php">Bird</a></h4>
                                        <a href="#" class="prize-places">Призовые места</a>
                                    </div>
                                </div>
                                <div class="game-screen">
                                    <div class="game-thumb"><img src="uploads/games/balls.png"></div>
                                    <div class="game-screen-mask">
                                        <h4 class="game-title"><a href="/html-layout/balls.phput/balls.php">Balls</a></h4>
                                        <a href="#" class="prize-places">Призовые места</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-push"></div> <!-- заготовка под подвал -->
        </div>
    </div>
    <div id="footer">
        <div class="footer-wrapper clearfix">
            <div class="copyrights">&copy; 2015, Все права защищены</div>
            <ul class="menu footer-menu clearfix">
                <li class="menu-item"><a class="front" href="index.php">Главная</a></li>
                <li class="menu-item"><a href="/html-layout/rules.php">Правила</a></li>
                <li class="menu-item"><a href="/html-layout/about.php">О нас</a></li>
                <li class="menu-item small-item"><a href="/html-layout/payment.php">Платежные<br/>системы</a></li>
                <li class="menu-item"><a href="/html-layout/partners.php">Партнеры</a></li>
                <li class="menu-item"><a href="/html-layout/contacts.php">Контакты</a></li>
            </ul>
        </div>
    </div>

    <div id="left-side">
        <a href="#" class="soc-ytb"></a>
        <a href="#" class="soc-vk"></a>
        <a href="#" class="soc-ok"></a>
        <a href="#" class="soc-fb"></a>
    </div>
    <div id="right-side"><a href="#">Помощь онлайн</a></div>

    <div id="dark"></div>
    <div id="video-popup">
        <i class="fa fa-times close"></i>
        <iframe width="560" height="315" src="" frameborder="0" allowfullscreen></iframe>
    </div>

    <div id="popup-window">
        <i class="fa fa-times close"></i>
        <div class="title"></div>
        <div class="content"></div>
    </div>

        <script src="/html-layout/js/libs/jquery.v1.11.1.min.js"></script>
        <script src="/html-layout/js/libs/bootstrap.min.js"></script>
        <script src="/html-layout/js/libs/materialize.min.js"></script>
        <script type="text/javascript" src="/html-layout/js/effects/app.js"></script>
        <script type="text/javascript" src="/html-layout/js/ajax/response.js"></script>
        <script type="text/javascript" src="/html-layout/js/nativeJS/customFunctions/functions.js"></script>
    </body>
</html>