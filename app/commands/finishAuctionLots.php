<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Models\Lot;
use Mix\DateHelper;

class finishAuctionLots extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'auctions:finish';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'finish expired lots';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $lots = Lot::where('status', 1)->get();

        if(count($lots)){
            foreach ($lots as $lotID) {
                $now = date("Y-m-d H:i:s");
                $expired = $lotID->expiration_date;
                if($now > $expired) {
                    $lot = Lot::find($lotID->id);
                    $lot->status = 0;
                    $lot->user_id = $lot->getOwnerId();
                    if($lot->save())
                        \Log::info("Истек срок лота #{$lot->SKU}", ['Владелец' => $lot->owner->nickname, 'название лота' => $lot->title]);
                    else
                        \Lot::error('ошибка');
                }
            }
        }
        
	}

}
