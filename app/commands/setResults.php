<?php

use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Models\Game;
use Models\User;

class setResults extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'games:set_result';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $games = Game::where('isActive', true)->get();

        foreach($games as $game) {
            $gameData = json_decode(file_get_contents("/home/s/scroboau/gamezina.five-dots.ru/public_html/application/Assets/".$game->alias.".json"));
            foreach ($gameData->results as $data) {
                $user = \DB::table('users')->where('nickname', $data->user_nickname)->first();
                $usArr = (array) $user;
                sort($data->user_score);
                if($game->type != 'super') {
                    $score = max($data->user_score);
                } else {
                    $score = isset($data->user_score[1]) ? $data->user_score[1] : $data->user_score[0];
                }
                /**
                 * TODO: РАЗРБОРАТЬСЯ С ЭТОЙ БЛЯДСКОЙ МАГИЕЙ!!!
                 */
                @\DB::table('game_user')
                    ->where('game_id', $game->id)
                    ->where('user_id', $usArr['id'])
                    ->update(['game_count' => $score]);
            }
        }

        \Log::info('Данные в таблице game_user успешно обновлены');
    }

}
