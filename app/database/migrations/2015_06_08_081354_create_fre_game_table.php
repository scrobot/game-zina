<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreGameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('free_games', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('title');
            $table->string('alias');
            $table->string('thumbnail');
            $table->string('thumbnail_360x250');
            $table->string('game_file');
            $table->text('rules');
            $table->string('repost_title');
            $table->string('counter_type');
            $table->boolean('isActive');
            $table->float('coefficient', 8, 6);
            $table->text('description');
            $table->string('keywords');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('free_games');
	}

}
