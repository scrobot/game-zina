<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('general_information', function(Blueprint $table)
		{
			$table->string('title_patterm')->after('id');
			$table->string('home_title')->after('title_patterm');
			$table->string('subscriptions_title')->after('copyrights');
			$table->string('statistics_title')->after('subscriptions_description');
			$table->string('blog_title')->after('statistics_description');
			$table->string('news_title')->after('blog_acrhive_description');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('general_information', function(Blueprint $table)
		{
			$table->dropColumn('title_patterm','home_title','subscriptions_title','statistics_title','blog_title','news_title');
		});
	}

}
