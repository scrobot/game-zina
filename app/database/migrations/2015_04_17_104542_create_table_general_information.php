<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGeneralInformation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('general_information', function(Blueprint $table)
		{
			$table->string('home_keywords')->nullable();
			$table->text('home_description')->nullable();
			$table->text('phone_of_call_center')->nullable();
            $table->string('logo')->nullable();
            $table->string('slogan')->nullable();
            $table->string('vk_adress')->nullable();
            $table->string('ok_adress')->nullable();
            $table->string('fb_adress')->nullable();
            $table->string('mailru_adress')->nullable();
            $table->string('tw_adress')->nullable();
            $table->string('gg_adress')->nullable();
            $table->string('ya_adress')->nullable();
            $table->string('copyrights')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('general_information');
	}

}
