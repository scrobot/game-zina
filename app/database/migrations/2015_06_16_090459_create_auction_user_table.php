<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lot_user', function(Blueprint $table)
		{
            $table->increments('id');
			$table->unsignedInteger('lot_id');
			$table->unsignedInteger('user_id');
            $table->integer('bet');
            $table->timestamp('bet_time');
            $table->index('lot_id', 'user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('lot_id')->references('id')->on('lots');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('lot_user', function(Blueprint $table)
        {
            $table->dropForeign('lot_user_lot_id_foreign');
            $table->dropForeign('lot_user_user_id_foreign');
        });
		Schema::drop('lot_user');
	}

}
