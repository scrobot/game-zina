<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTableGeneralInformation2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('general_information', function(Blueprint $table)
		{
			$table->string('subscriptions_keywords')->nullable();
            $table->string('subscriptions_description')->nullable();
            $table->string('statistics_keywords')->nullable();
            $table->string('statistics_description')->nullable();
            $table->string('blog_acrhive_keywords')->nullable();
            $table->string('blog_acrhive_description')->nullable();
            $table->string('news_archive_keywords')->nullable();
            $table->string('news_archive_description')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('general_information', function(Blueprint $table)
		{
			$table->dropColumn('subscriptions_keywords','subscriptions_description','statistics_keywords','statistics_description','blog_acrhive_keywords','blog_acrhive_description','news_archive_keywords','news_archive_description');
		});
	}

}
