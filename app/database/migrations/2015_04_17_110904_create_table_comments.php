<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableComments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
            $table->text('comment');
            $table->boolean('status')->default(1);
            $table->smallInteger('post_type');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('game_id')->index()->nullable();
            $table->unsignedInteger('post_id')->index()->nullable();
            $table->unsignedInteger('new_id')->index()->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('game_id')->references('id')->on('games');
            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('new_id')->references('id')->on('news');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('comments', function(Blueprint $table)
        {
            $table->dropForeign('comments_user_id_foreign');
            $table->dropForeign('comments_game_id_foreign');
            $table->dropForeign('comments_post_id_foreign');
            $table->dropForeign('comments_new_id_foreign');
        });

		Schema::drop('comments');
	}

}
