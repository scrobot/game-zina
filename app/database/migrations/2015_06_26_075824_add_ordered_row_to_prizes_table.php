<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderedRowToPrizesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('prizes', function(Blueprint $table)
		{
            $table->boolean('ordered')->after('game_count');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('prizes', function(Blueprint $table)
		{
            $table->dropColumn('ordered');
		});
	}

}
