<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->boolean('isAdmin');
			$table->boolean('isActive')->index();
			$table->string('activationCode');
			$table->string('remember_token', 100)->nullable()->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('isAdmin');
			$table->dropColumn('isActive');
			$table->dropColumn('activationCode');
			$table->dropColumn('remember_token');
		});
	}

}
