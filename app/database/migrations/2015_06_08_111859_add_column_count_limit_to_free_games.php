<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCountLimitToFreeGames extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('free_games', function(Blueprint $table)
        {
            $table->bigInteger('count_stop_limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('free_games', function(Blueprint $table)
        {
            $table->dropColumn('count_stop_limit');
        });
    }

}
