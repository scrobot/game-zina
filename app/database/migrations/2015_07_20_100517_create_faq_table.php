<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('faqs', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title');
            $table->string('alias');
            $table->unsignedInteger('parent_id')->index()->nullable();
            $table->text('content');
            $table->foreign('parent_id')->references('id')->on('faqs')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('faqs', function(Blueprint $table)
        {
            $table->dropForeign('faqs_parent_id_foreign');
        });
		Schema::drop('faqs');
	}

}
