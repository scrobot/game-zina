<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPaysTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('pays', function(Blueprint $table)
        {
            $table->unsignedInteger('status_id');

            $table->index('status_id');
            $table->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pays', function(Blueprint $table)
        {
            $table->dropForeign('statuses_user_id_foreign');
            $table->dropColumn('status_id');
        });
    }

}
