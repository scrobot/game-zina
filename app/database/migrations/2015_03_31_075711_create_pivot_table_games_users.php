<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotTableGamesUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('game_user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('game_id');
			$table->unsignedInteger('user_id');
			$table->integer('game_count');

            $table->index('game_id', 'user_id');
            $table->foreign('game_id')->references('id')->on('games');
            $table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('game_user', function(Blueprint $table)
		{
			$table->dropForeign('game_user_game_id_foreign');
			$table->dropForeign('game_user_user_id_foreign');
		});
        Schema::drop('game_user');
	}

}
