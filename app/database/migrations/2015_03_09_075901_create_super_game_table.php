<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperGameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('super_games', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title');
            $table->string('alias');
            $table->string('thumbnail');
            $table->string('game_demo');
            $table->string('game_full');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->text('rules');
            $table->integer('all_bank');
            $table->index('title');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('super_games');
	}

}
