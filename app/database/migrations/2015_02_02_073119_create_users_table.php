<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('sex')->nullable();
			$table->date('birthday')->nullable();
			$table->string('avatar')->nullable();
			$table->string('login')->unique();
			$table->string('password', 60);
			$table->string('email')->nullable()->unique();
			$table->unsignedBigInteger('social_uid')->nullable();
			$table->unsignedInteger('role_id');
			$table->index('role_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
