<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPaysTable3 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pays', function(Blueprint $table)
		{
			$table->string('xml_encode');
			$table->string('sign_encode');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pays', function(Blueprint $table)
		{
			$table->dropColumn('sign_encode', 'xml_encode');
		});
	}

}
