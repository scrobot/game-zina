<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrizesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prizes', function(Blueprint $table)
		{
			$table->increments('id');
            $table->unsignedInteger('game_id');
            $table->integer('place');
            $table->string('prize');
            $table->index('game_id');
            $table->foreign('game_id')
                ->references('id')->on('super_games')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('prizes');
	}

}
