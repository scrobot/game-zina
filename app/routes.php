<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//API

Route::group(array('prefix' => 'api/gamezina/v1/request'), function () {
    Route::controller('get', '\Front\Api\ApiGetController');
    Route::controller('post', '\Front\Api\ApiPostController');
});

// Back
Route::group(array('before' => 'auth', 'prefix' => 'admin'), function () {
    Route::controller('/api/doc', '\Back\ApiDocumentationController');
    Route::controller('/faq', '\Back\FaqController');
    Route::controller('/free-games', '\Back\FreeGamesController');
    Route::controller('/lots', '\Back\AuctionController');
    Route::controller('/orders', '\Back\OrderController');
    Route::controller('/comments', '\Back\CommentController');
    Route::controller('/menu', '\Back\MenuController');
    Route::controller('/blog', '\Back\BlogController');
    Route::controller('/news', '\Back\NewsController');
    Route::controller('/banners', '\Back\BannerController');
    Route::controller('/games', '\Back\GameController');
    Route::controller('/pages', '\Back\PageController');
    Route::controller('/users', '\Back\UserController');
    Route::controller('/', '\Back\AdminController');
});

Route::group(['before' => 'isAdmin'], function(){
    Route::get('/sign/in', '\Back\IndexController@getIn');
});
Route::controller('sign', '\Back\IndexController');

Route::get('403', function(){
    return \View::make('403');
});

// Front
Route::group(array('before' => 'isBanned'), function () {
    Route::post('/tournament/reposted/repost', '\Front\GamesController@postReposted');
    Route::get('/binder/{alias}', '\Front\GamesController@getBinder');
    Route::get('/free-binder/', '\Front\FreeGamesController@getBinder');
    Route::post('/score', '\Front\GamesController@postScore');
    Route::post('/free-games-score', '\Front\FreeGamesController@postScore');
    Route::get('/subscriptions', '\Front\GamesController@getSubscriptions');
    Route::get('/ajax-news', '\Front\NewsController@getAjaxNews');
    Route::get('/hall-of-glory', '\Front\PageController@getHallOfGlory');
    Route::get('blog/{alias}', '\Front\BlogController@getPost');
    Route::get('news/{alias}', '\Front\NewsController@getNews');
    Route::get('/tournament', '\Front\GamesController@getAllTournaments');
    Route::get('tournament/{alias}', '\Front\GamesController@getGame');
    Route::get('free-games/{alias}', '\Front\FreeGamesController@getGame');
    Route::get('handbook/{alias}', '\Front\FaqController@getHandbook');
    Route::controller('checkout', '\Front\CheckoutController');
    Route::controller('free-games', '\Front\FreeGamesController');
    Route::controller('auctions', '\Front\AuctionController');
    Route::controller('comments', '\Front\CommentsController');
    Route::controller('news', '\Front\NewsController');
    Route::controller('blog', '\Front\BlogController');
    Route::controller('tournament', '\Front\GamesController');
    Route::controller('pay', '\Front\PayController');
    Route::controller('password', '\Front\RemindersController');
    Route::get('/', '\Front\HomeController@getIndex');
});

Route::get('page/{alias}', '\Front\PageController@getPage');
Route::controller('page', '\Front\PageController');
Route::controller('user', '\Front\UserController');
Route::get('/banhammer', '\Front\HomeController@getBanhammer');

