$(document).ready(function(){

    var body = $('body');

    $("#dymamic-table").on('click', '.prizeCall', function(e) {


            e.preventDefault();
            var link = $(this).attr('href');

            $.ajax(
                {
                    type : 'get',
                    url : link,
                    success : function(response)
                    {

                        $('#prizesModal .modal-body').append(response);

                    }
                });

    });

    $('.generate-secret').click(function(e){
        e.preventDefault()
        var length = $(this).data('apikeylength');
        var link = $(this).attr('href');
        var that = $(this);

        $.ajax(
            {
                type : 'post',
                url : link,
                data: {length: length},
                success : function(response)
                {
                    that.parent().find('input').attr('value', response)
                }
            });

    });

});