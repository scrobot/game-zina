jQuery(function($) {
    $('#dymamic-table').dataTable(
        {
            "language": {
                "lengthMenu": "Отображать _MENU_ записей на страницу",
                "zeroRecords": "Ничего не найдено",
                "info": "Страница _PAGE_ из _PAGES_",
                "infoEmpty": "Нет доступных записей",
                "infoFiltered": "(искал среди _MAX_ записей)",
                "paginate" : {
                    "first" : "Первая",
                    "last" : "Последняя",
                    "next" : "Следующая",
                    "previous" : "Предыдущая"
                },
                "search" : "Поиск"
            }
        }
    );

});

	