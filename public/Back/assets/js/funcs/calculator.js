$(document).ready(function(){

    $("#caclSubmit").click(function(){

        var i;
        var j;
        var k;
        var step = 0;
        var bank = parseInt($('#lastPlace').val());
        var places = [];
        var scope = {
            allBank : 0,
            thirdPlace:  parseInt($('#thirdPlace').val()),
            secondPlace: parseInt($('#secondPlace').val()),
            firstPlace: $('#firstPlace').val(),
            results: []
        };

        step = parseInt($('#firstStep').val());

        for(i = 4; i >= 1; i--) {

            for(j = 40; j >= 1; j--) {

                k = (i*40 + (j - 40))+40;

                places[k] = bank;
                bank += step;

                if(j == 1) {
                    step += 10;
                }

                scope.allBank += bank;

            }

        }

        step = parseInt($('#secondStep').val());

        for(i = 40; i > 10; i--) {

            places[i] = bank;
            bank += step;
            scope.allBank += bank;

        }

        step = parseInt($('#finalStep').val());

        for(i = 10; i > 0; i--) {

            switch(i) {
                case 3 :
                    scope.allBank += scope.thirdPlace;
                    places[i] = scope.thirdPlace;
                    break;
                case 2 :
                    scope.allBank += scope.secondPlace;
                    places[i] = scope.secondPlace;
                    break;
                case 1 :
                    if(isInt(scope.firstPlace))
                        scope.allBank += scope.firstPlace*1;
                    places[i] = scope.firstPlace;
                    break;
                default :
                    places[i] = bank;
                    bank += step;
                    scope.allBank += bank;
            }
        }

        scope.results = places;

        drawTable(scope.results, scope.allBank)

    });

    function drawTable(rowArray, bank) {

        var i = 0;
        $('#allBank').show();
        $('#allBank .red').text(bank);
        $('#all_bank').attr('value', bank);
        $('#results-table tbody tr').remove();
        for(i; i < rowArray.length; i++) {

            if(i != 0) {

                $('#results-table tbody').append(
                    '<tr>' +
                    '<td>' + i + '</td>' +
                    '<td><input type="text" name="prize[' + i + '][]" value="' + rowArray[i] + '"/></td>' +
                    '<td>' + '</td>' +
                    '</tr>'
                );
            }

        }


    }
});

function isInt(n) {
    return n % 1 === 0;
}
