
var alias = $('#lot-title').data('title');
var getLot = showGetLotsBinder("/auctions/single-lot-binder/" + alias)

var data = {
    lot: getLot,
    userBalance: $('#user-balance').data('balance'),
    setBet: function(ev, scope) {

        var bet = parseInt($(ev.toElement).closest('form').find('input[name="bet"]').val());
        var minBet = parseInt($(ev.toElement).closest('form').find('input[name="bet"]').attr('min'));
        var firstBet = parseInt($(ev.toElement).closest('form').find('input[name="bet"]').attr('firstBet'));
        var maxBet = minBet + firstBet;
        
        if(bet < minBet) {
            alert('Введите ставку равную или более ' + minBet + " баллов")
        } else if(bet > maxBet) {
            alert('Введите ставку равную или менее ' + maxBet + " баллов")
        } else if(isNaN(bet)) {
            alert("Введите свою ставку")
        } else {

            var formData = $(ev.toElement).closest('form').serialize();
            var formAlias = $(ev.toElement).closest('form').attr('action');
            //$('.loading').fadeIn('fast').delay(4000).fadeOut('fast');
            //$('#lot-view .lot').find('section').animate({opacity: 0},500).delay(4000).animate({opacity: 1},500);

            var results = showPostLotsBinder(formData, formAlias)
            console.log(results);
            if(results.status > 0) {
                scope.data.userBalance = results.userBalance
                scope.data.lot = results.lot
                scope.data.lot.ownerIsLook = $('#cid').data('clientid') == scope.data.lot.ownerClientId;
            } else {
                scope.data.userBalance = results.userBalance
                alert(results.error)
            }

        }



    }
}

data.lot.ownerIsLook = $('#cid').data('clientid') == data.lot.ownerClientId;

console.log(data);

lot = document.getElementById('lot');
balance = document.getElementById('user-balance')

rivets.binders.href = function(el, value) {
    var href = "/uploads/auction_images/" + value
    el.setAttribute("href", href);
}

rivets.binders.src = function(el, value) {
    var src = "/uploads/auction_images/" + value
    el.setAttribute("src", src);
}

rivets.binders.min = function(el, value) {
    var min = value
    el.setAttribute("min", min);
}

rivets.binders.placeholder = function(el, value) {
    var placeholder = "Ставка " + value
    el.setAttribute("placeholder", placeholder);
}

rivets.binders.formValue = function(el, value) {
    var formValue = value
    el.setAttribute("value", formValue);
}

rivets.binders.id = function(el, value) {
    var id = "lot-" + value
    el.setAttribute("id", id);
}

rivets.binders.title = function(el, value) {
    var title = value
    el.setAttribute("title", title);
}

rivets.formatters.title = function(value) {
    var reviewLength = 20;

    if( value.length > reviewLength )
    {
        value = value.substring(0, reviewLength) + "...";
        return value;
    }

    return value;
}

rivets.formatters.date = function(value) {

    if(value > 86400) {
        value = parseInt(value / 86400);

    } else if(value <= 86400 && value > 3600) {
        value = parseInt(value / 3600);

    } else if(value <= 3600 && value > 60) {
        value = parseInt(value / 60);

    } else if(value <= 60 && value > 1) {
        value = '< 1'
    } else if(value <= 0) {
        value = "Закончен"
    }

    return value;

}

rivets.formatters.literal = function(value){

    if(value > 86400) {
        value = parseInt(value / 86400);

        switch(value) {
            case 1: value = " День"; break;
            case 2: value = " Дня"; break;
            case 3: value = " Дня"; break;
            case 4: value = " Дня"; break;
            default: value = " Дней";
        }

    } else if(value <= 86400 && value > 3600) {

        value = parseInt(value / 3600);

        switch(value) {
            case 1: value = " Час"; break;
            case 2: value = " Часа"; break;
            case 3: value = " Часа"; break;
            case 4: value = " Часа"; break;
            case 21: value = " Час"; break;
            default: value = " Часов";
        }
    } else if(value <= 3600 && value > 60) {

        value = parseInt(value / 60);

        switch(value) {
            case 1: value = " Минута"; break;
            case 2: value = " Минуты"; break;
            case 3: value = " Минуты"; break;
            case 4: value = " Минуты"; break;
            case 21: value = " Минута"; break;
            case 31: value = " Минута"; break;
            case 41: value = " Минута"; break;
            case 51: value = " Минута"; break;
            default: value = " Минут";
        }
    } else if(value <= 60 && value > 1) {
        value = ' минуты'
    } else if(value <= 0) {
        value = ''
    }

    return value;

}


rivets.bind(lot, {data: data})
rivets.bind(balance, {user: data})

