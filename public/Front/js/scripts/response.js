$(document).ready(function(){

    var body = $('body');

    body.on('click','.user-action', function(e){
        e.preventDefault();
        var link = $(this).attr('href');

        $.ajax(
            {
                type : 'get',
                url : link,
                beforeSend: function() {
                    $('#preloader').fadeIn('fast');
                },
                success : function(response)
                {

                    $('#modal-window .modal-content').html(response);

                },
                complete : function(data)
                {
                    $('#preloader').fadeOut('fast');
                }
            });
    });

    body.on('click','.reg-send', function(){

        var data = $(this).closest('form').serialize();
        var link_action = $(this).closest('form').attr('action');
        $('#preloader').fadeIn('fast');
        $.post(link_action, data, function(response){
            $('#modal-window .modal-content').html(response);
            $('#preloader').fadeOut('fast');
        });

    });

    body.on('click','.auth-send', function(){

        var data = $(this).closest('form').serialize();
        var link_action = $(this).closest('form').attr('action');
        $('#preloader').fadeIn('fast');
        $.post(link_action, data, function(response){
            $('#preloader').fadeOut('fast');
            if(response == 1) {
                location.href = '/';
            } else {
                $('#modal-window .modal-content').find('.error-message').text(response)
            }
        });

    });

    $('.activation-box').on('click','button', function(){

        var data = $(this).closest('form').serialize();
        var link_action = $(this).closest('form').attr('action');
        $('#preloader').fadeIn('fast');
        $.post(link_action, data, function(response){
            $('#preloader').fadeOut('fast');
            $('.activation-box').html(response);
            $('.activation-box').find('p').addClass('classik');
        });

    });

    $('.user-edit-call').click(function(e){
        e.preventDefault();
        var link = $(this).attr('href');

        $.ajax
        (
            {
                type : 'get',
                url : link,
                beforeSend: function() {
                    $('#preloader').fadeIn('fast');
                },
                success : function(response)
                {

                    $('#modal-window .modal-content').html(response);

                },
                complete : function(data)
                {
                    $('#preloader').fadeOut('fast');
                }
            }
        );

    });

    body.on('click', '.edit-send', function(){
        var data = $(this).closest('form').serialize();
        var link_action = $(this).closest('form').attr('action');
        $('#preloader').fadeIn('fast');
        $.post(link_action, data, function(response){
            if(response.successId === 1) {
                $('#modal-window .modal-content #user-edit').find('.success-message').text(response.msg);
            } else {
                $('#modal-window .modal-content #user-edit').find('.error-message').text(response.msg);
            }
            $('#preloader').fadeOut('fast');
        });
    });

    body.on('click', '.btn-reminder', function(){
        var data = $(this).closest('form').serialize();
        var link_action = $(this).closest('form').attr('action');
        $('#preloader').fadeIn('fast');
        $.post(link_action, data, function(response){
            if(response.status) {
                body.find('#reminder .alert-success').html(response.status).fadeIn('fast');
            } else if(response.error) {
                body.find('#reminder .alert-danger').html(response.error).fadeIn('fast');
            }
            $('#preloader').fadeOut('fast');
        });
    });

    body.on('click', '.reminder', function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        $('#preloader').fadeIn('fast');
        $('#modal-window').modal();
        $.get(link, null ,function(response){
            $('#modal-window .modal-content').html(response);
            $('#preloader').fadeOut('fast');
        });

    });

    body.on('click', '.pay-submit', function(){
        var data = $(this).closest('form').serialize();
        var link_action = $(this).closest('form').attr('action');
        $('#preloader').fadeIn('fast');
        $.post(link_action, data, function(response){
            $('#pays-window .modal-content').html(response);
            $('#preloader').fadeOut('fast');
        });
    });


});