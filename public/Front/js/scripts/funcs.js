function showGetLotBinder(alias)
{
    var result = null;
    var scriptUrl = alias;
    $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
    });

    return result;
}

function showPostLotsBinder(formData, alias)
{
    var result = null;
    var scriptUrl = alias;
    $.ajax({
        url: scriptUrl,
        type: 'post',
        data: formData,
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
    });

    return result;
}

function showGetLotsBinder(alias)
{
    var result = null;
    var scriptUrl = alias;
    $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
    });

    return result;
}

function showGetResult( alias )
{
    var result = null;
    var scriptUrl = "/binder/"+ alias;
    $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
    });

    return result;
}

function showGetResultFree()
{
    var result = null;
    var scriptUrl = "/free-binder/";
    $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
    });

    return result;
}

function inputMaskInit()
{

    $('.checkout-form form input.fio').inputmask({
        mask: "*{1,20}[ *{1,20}][ *{1,20}]",
        greedy: false,
        definitions: {
            '*': {
                validator: "[А-Яа-я!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1
            }
        },
        oncomplete: function(){
            $(this).addClass('ok').removeClass('error')
            $('.fio-error-message').fadeOut('fast');
        },
        onincomplete: function(){$(this).addClass('error').removeClass('ok')}
    });

    $('.checkout-form form input').focusout(function(){
       $(this).valid();
    })

    $('.checkout-form form textarea').focusout(function(){
        $(this).valid();
    })

    $('.checkout-form form input.phone').inputmask("mask", {
        mask: "+7(999) 999-9999",
        oncomplete: function(){$(this).addClass('ok').removeClass('error')},
        onincomplete: function(){$(this).addClass('error').removeClass('ok')}
    });

    $('.checkout-form form input.email').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        },
        oncomplete: function(){$(this).addClass('ok').removeClass('error')},
        onincomplete: function(){$(this).addClass('error').removeClass('ok')}
    });

    $('.checkout-form form input[name="division_code"]').inputmask("mask", {
        "mask": "999-999",
    });
    $('.checkout-form form input[name="date_of_issue"]').inputmask("mask", {
        "mask": "99.99.9999",
    });
    $('.checkout-form form input[name="serial_number"]').inputmask("mask", {
        "mask": "9999 999999",
    });
    $('.checkout-form form input[name="birthday"]').inputmask("mask", {
        "mask": "99.99.9999",
    });
    $('.checkout-form form input[name="registration_postcode"]').inputmask("mask", {
        "mask": "9{6}",
    });
    $('.checkout-form form input[name="registration_housing"]').inputmask("mask", {
        "mask": "9{1,6}",
    });
    $('.checkout-form form input[name="registration_building"]').inputmask("mask", {
        "mask": "9{1,6}",
    });
    $('.checkout-form form input[name="registration_flat"]').inputmask("mask", {
        "mask": "9{1,6}",
    });
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function validate_form(formName) {
    return false
}