$(document).ready(function() {

    var body = $('body');

    $('.carousel').carousel({
        interval: 5000
    }).carousel('pause');
    $("#advertisement-slider").responsiveSlides({
        auto: true,
        pager: true,
        nav: false,
        speed: 1500
    });
    if ($('body').width() < 1024 && $('body').width() >= 768) {
        $('#flashContent').find('object').attr('width', 720).attr('height', 542);
        $('#flashContent').find('object object').attr('width', 720).attr('height', 542);
    }
    if ($('body').width() >= 1024 && $('body').width() < 1200) {
        $('#flashContent').find('object').attr('width', 705).attr('height', 542);
        $('#flashContent').find('object object').attr('width', 705).attr('height', 542);
    }
    if ($('body').width() >= 1200) {
        $('#flashContent').find('object').attr('width', 748).attr('height', 540);
        $('#flashContent').find('object object').attr('width', 748).attr('height', 540);
    }
    $('.countdown .flip-clock-divider.hours .flip-clock-label').text('Часов');
    $('.countdown .flip-clock-divider.minutes .flip-clock-label').text('Минут');
    $('.countdown .flip-clock-divider.seconds .flip-clock-label').text('Секунд');

    $('.comment-reply').click(function(){
        var commentId = $(this).data('commentid');
        var replier = $(this).data('replier');

        $('#reply_field').attr('value', 1);
        $('#reply_comment_id_field').attr('value', commentId);
        $('#replier').attr('value', replier);

        // Запоминаем textarea, т.к. далее будем использовать его более 1 раза:
        var poleVvoda = $("#comment-form textarea"),

        // Сохраняем старое значение прежде чем очистить:
            val = poleVvoda.val();

        // Если значение не оканчивается пробелом, то добавляем его:
        if (val.charAt(val.length-1) != " ") {
            val += " ";
        }

        // Ставим фокус на textarea, очищаем значение, применяем повторно:
        poleVvoda.focus().val("").val(val);
    });

    $('nav.sort a').click(function(){
        $('nav.sort a').each(function(){$(this).removeClass('active')});
        $(this).addClass('active');

        var full = $(this).data('full');
        var type = $(this).data('type');

        $.get('/ajax-news', {full: full, type: type}, function(response) {
           $('.news-wrap').html(response);
        });

    });

    $('.user-info').on('click', '.change-email-add', function(){
        $('.einfo').hide();
        $('.user-profile .change-form').show();
        $(this).addClass('change-email-remove').removeClass('change-email-add').text('отмена')
    })

    $('.user-info').on('click', '.change-email-remove', function(){
        $('.einfo').show();
        $('.user-profile .change-form').hide();
        $(this).addClass('change-email-add').removeClass('change-email-remove').text('изменить')
    })

    /*AJAX*/

    $('.form-validate input').focusout(function(){
        var val = $(this).val();
        var name = $(this).attr('name');
        var that = $(this);
        var change = $(this).data('change')

        $.post('/user/validation-field', {name: name, val: val}, function(response){
            if(response.status === 1) {
                that.closest('.form-group').addClass('has-success').removeClass('has-error');
                that.closest('.form-group').find('p.error').text('');
                if(change == 1) {
                    $('.change-form button').show()
                }
            } else {
                that.closest('.form-group').addClass('has-error').removeClass('has-success');
                that.closest('.form-group').find('p.error').text(response.message);
                $('.change-form button').hide()
            }
        })
    });

    $('.form-validate button').click(function(e){
        e.preventDefault()
        var action = $(this).closest('form').attr('action');
        var formData = $(this).closest('form').serialize();

        $.ajax(
            {
                type : 'post',
                url : action,
                data : formData,
                beforeSend: function() {
                    //$('#preloader').fadeIn('fast');
                },
                success : function(response)
                {
                    if(response.status == 200) {
                        window.location.href = response.link
                    } else {

                    }
                 },
                complete : function(data)
                {
                    //$('#preloader').fadeOut('fast');
                }
            });
    })

    $('.auth-form button').click(function(e) {
        e.preventDefault();
        var action = $(this).closest('form').attr('action');
        var formData = $(this).closest('form').serialize();

        $.ajax(
            {
                type : 'post',
                url : action,
                data : formData,
                beforeSend: function() {
                    //$('#preloader').fadeIn('fast');
                },
                success : function(response)
                {
                    if(response.status == 200) {
                        window.location.href = response.link
                    } else {
                        $('#sign-in .error-message').text(response.error)
                    }
                },
                complete : function(data)
                {
                    //$('#preloader').fadeOut('fast');
                }
            });

    })

    $('#comment-form').on('click', 'button', function(){

        var action = $(this).closest('form').attr('action');
        var formData = $(this).closest('form').serialize();
        $.ajax(
            {
                type : 'post',
                url : action,
                data : formData,
                beforeSend: function() {
                    //$('#preloader').fadeIn('fast');
                },
                success : function(response)
                {
                    $('.comments-content').find('.comment-empty');
                    $('.comments-content').append(response.view);
                },
                complete : function(data)
                {
                    //$('#preloader').fadeOut('fast');
                }
            });

    });

    var checkout = $('#checkout');

    checkout.on('click', 'button', function (e) {

        if(!$(this).hasClass('fuck')) {
            e.preventDefault()

            var form = $(this).closest('form')
            var action = form.attr('action')
            var data = form.serialize()
            var loading = $('.loading')

            $.extend(jQuery.validator.messages, {
                required: "Это поле обязательно для заполнения."
            });

            form.validate({
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass(errorClass).addClass(validClass);
                }
            });

            if(form.valid()) {
                loading.fadeIn('fast')
                $.post(action, data, function(response){
                    loading.fadeOut('fast')
                    $('#checkout').html(response);
                    inputMaskInit()
                })
            }
        }
    })

    checkout.on('click', '#matches', function(){
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('#actual_address').load('/checkout/actual-address-form', function(){
                $('#actual_address input').each(function(){
                    $(this).attr('required', 'required')
                    $('.checkout-form form input[name="actual_postcode"]').inputmask("mask", {"mask": "9{6}"}); //specifying fn & options
                    $('.checkout-form form input[name="actual_housing"]').inputmask("mask", {"mask": "9{1,6}"}); //specifying fn & options
                    $('.checkout-form form input[name="actual_building"]').inputmask("mask", {"mask": "9{1,6}"}); //specifying fn & options
                    $('.checkout-form form input[name="actual_flat"]').inputmask("mask", {"mask": "9{1,6}"}); //specifying fn & options
                })
            });
        } else {
            $(this).addClass('active');
            $('#actual_address input').each(function(){
                $(this).remove();
            });
        }
    });

    checkout.on('click', 'a', function(e){
        e.preventDefault();
        var step = $(this).data('step');
        var step_url = '/checkout/step/' + step;
        checkout.load(step_url, function(){
            inputMaskInit()
        })
    })

    $('.more-prizes').click(function(e){
        e.preventDefault()
        var thisText = $(this).text();
        if(thisText == "Посмотреть все") {
            $(this).text('Свернуть')
        } else if(thisText == "Свернуть") {
            $(this).text('Посмотреть все')
        }
        $('#prizes').find('.hidden-place').slideToggle('fast')
    })

    $('.user-part-info #auctions').on('click', 'a', function(e){
        e.preventDefault()
        $(this).fadeOut('fast');
        $.get('/user/show-lots', {start: 0}, function(response) {
            $('.user-part-info #auctions').html(response);
        })
    })

    $('.user-part-info #pays').on('click', 'a', function(e){
        e.preventDefault()
        $(this).fadeOut('fast');
        $.get('/user/show-pays', {start: 0}, function(response) {
            $('.user-part-info #pays').html(response);
        })
    })

});

function validate_form(formName) {
    return false
}