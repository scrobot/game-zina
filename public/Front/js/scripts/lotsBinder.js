var getData = showGetLotsBinder("/auctions/lots-binder/")

var nativeLength = getData.expired_lots.length
var expiredLots = getData.expired_lots.slice(0, 4);

var data = {
    lots: getData.lots,
    expiredLots: {
        hasExpired: false,
        lotsData: expiredLots,
        arrayLenght: expiredLots.length,
        moreExists: expiredLots.length < nativeLength
    },
    userBalance: $('#user-balance').data('balance'),
    setBet: function(ev, scope) {

        var bet = parseInt($(ev.toElement).closest('form').find('input[name="bet"]').val());
        var minBet = parseInt($(ev.toElement).closest('form').find('input[name="bet"]').attr('min'));
        var firstBet = parseInt($(ev.toElement).closest('form').find('input[name="bet"]').attr('firstBet'));
        var maxBet = minBet + firstBet;

        if(bet < minBet) {
            alert('Введите ставку равную или более ' + minBet + " баллов")
        } else if(bet > maxBet) {
            alert('Введите ставку равную или менее ' + maxBet + " баллов")
        } else if(isNaN(bet)) {
            alert("Введите свою ставку")
        } else {

            var formData = $(ev.toElement).closest('form').serialize();
            var formAlias = $(ev.toElement).closest('form').attr('action');
            var results = showPostLotsBinder(formData, formAlias)
            // console.log(results);
            if(results.status > 0) {
                scope.data.userBalance = results.userBalance
                scope.data.lots = results.lots
                var cid = $('#cid').data('clientid');

                for(var i = 0; i < data.lots.length; i++) {
                    scope.data.lots[i].ownerIsLook = cid == scope.data.lots[i].ownerClientId
                }
            } else {
                scope.data.userBalance = results.userBalance
                alert(results.error)

            }

        }
    },
    loadMore: function(ev, scope) {
        var length = $(ev.toElement).data('length')
        length += 4
        $(ev.toElement).attr('data-length', length)
        scope.data.expiredLots.lotsData = getData.expired_lots.slice(0, length)
        scope.data.expiredLots.arrayLenght = length
        scope.data.expiredLots.moreExists = length < getData.expired_lots.length
    }
}

console.log(data);

var cid = $('#cid').data('clientid');

for(var i = 0; i < data.lots.length; i++) {
    data.lots[i].ownerIsLook = cid == data.lots[i].ownerClientId
}

console.log(data);

if(getData.expired_lots.length > 0) {
    data.expiredLots.hasExpired = true
}

lots = document.getElementById('lots');
expiredLots = document.getElementById('expiredLots');
balance = document.getElementById('user-balance')

rivets.binders.href = function(el, value) {
    var href = "/auctions/lot/" + value
    el.setAttribute("href", href);
}

rivets.binders.src = function(el, value) {
    var src = "/uploads/auction_images/" + value
    el.setAttribute("src", src);
}

rivets.binders.min = function(el, value) {
    var min = value
    el.setAttribute("min", min);
}

rivets.binders.placeholder = function(el, value) {
    var placeholder = "Ставка " + value
    el.setAttribute("placeholder", placeholder);
}

rivets.binders.formValue = function(el, value) {
    var formValue = value
    el.setAttribute("value", formValue);
}

rivets.binders.id = function(el, value) {
    var id = "lot-" + value
    el.setAttribute("id", id);
}

rivets.binders.title = function(el, value) {
    var title = value
    el.setAttribute("title", title);
}

rivets.binders.datalength = function(el, value) {
    var datalength = value
    el.setAttribute("data-length", datalength);
}

rivets.formatters.title = function(value) {
    var reviewLength = 20;

    if( value.length > reviewLength )
    {
        value = value.substring(0, reviewLength) + "...";
        return value;
    }

    return value;
}

rivets.formatters.formatDate = function(value) {

    var date = new Date(value)
    var formatedDate = $.formatDateTime('dd.mm.yy в h:ii', date)

    return formatedDate

}

rivets.formatters.date = function(value) {

    if(value > 86400) {
        value = parseInt(value / 86400);

    } else if(value <= 86400 && value > 3600) {
        value = parseInt(value / 3600);

    } else if(value <= 3600 && value > 60) {
        value = parseInt(value / 60);

    } else if(value <= 60 && value > 1) {
        value = '< 1'
    } else if(value <= 0) {
        value = "Закончен"
    }

    return value;

}

rivets.formatters.literal = function(value){

    if(value > 86400) {
        value = parseInt(value / 86400);

        switch(value) {
            case 1: value = " День"; break;
            case 2: value = " Дня"; break;
            case 3: value = " Дня"; break;
            case 4: value = " Дня"; break;
            default: value = " Дней";
        }

    } else if(value <= 86400 && value > 3600) {

        value = parseInt(value / 3600);

        switch(value) {
            case 1: value = " Час"; break;
            case 2: value = " Часа"; break;
            case 3: value = " Часа"; break;
            case 4: value = " Часа"; break;
            case 21: value = " Час"; break;
            default: value = " Часов";
        }
    } else if(value <= 3600 && value > 60) {

        value = parseInt(value / 60);

        switch(value) {
            case 1: value = " Минута"; break;
            case 2: value = " Минуты"; break;
            case 3: value = " Минуты"; break;
            case 4: value = " Минуты"; break;
            case 21: value = " Минута"; break;
            case 31: value = " Минута"; break;
            case 41: value = " Минута"; break;
            case 51: value = " Минута"; break;
            default: value = " Минут";
        }
    } else if(value <= 60 && value > 1) {
        value = ' минуты'
    } else if(value <= 0) {
        value = ''
    }

    return value;

}


rivets.bind(lots, {data: data})
rivets.bind(expiredLots, {data: data})
rivets.bind(balance, {user: data})
