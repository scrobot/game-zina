
   var alias = $('.game-page .title').data('alias');
   var results = showGetResult(alias)

    data = {
        all: results.allResults,
        top: results.top,
        top10: results.top10,
        current: results.currentUser,
        userBalance: $('#user-balance').data('balance'),
        refresh: function(ev, scope) {
            temp = showGetResult(alias)
            scope.data.all = temp.allResults
            scope.data.top = temp.top
            scope.data.top10 = temp.top10
            scope.data.current = temp.currentUser
        }
    }

    top10 = document.getElementById('top10Results')
    top200 = document.getElementById('top200Results')
    balance = document.getElementById('user-balance')

    rivets.formatters.place = function(value, username, count){

        var arr;

        if(count == 1)
            arr = data.top10
        else if(count == 2)
            arr = data.top
        else if(count == 3)
            arr = data.all

        for(var i = 0; i < arr.length; i++) {
            if(username == arr[i]['username']) {
                if(count == 3) {
                    data.current.place = i+1
                    return data.current.place
                } else {
                    value = i+1;
                }
                break;
            }
        }

        return value

    }

    rivets.bind(top10, {data: data})
    rivets.bind(top200, {data: data})
    rivets.bind(balance, {user: data})

